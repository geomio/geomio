%
% Example code to compute a Bouguer anomaly of an infinite horizontal cylinder
% and to compare with its analytical solution
%
%
% T.S. Baumann, Mainz, 2018
% =========================================================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
clc, clear, close all

% settings
opt                 = geomIO_Options();
opt.inputDir        = ['./Input'];
opt.outputDir       = ['./Output'];
opt.inputFileName   = ['cylinder.svg'];

% run geomIO
PathCoord           = run_geomIO(opt,'2D');


% selection of shapes and density asignment
paths = {
    'cylinder',  500
     };
opt.pathNames    = {paths{:,1}}; 
opt.gravity.drho = [paths{:,2}]; 


% Profile m
x0 = -400:400; 
z0 = zeros(size(x0));

% Analytical solution
gobsa = gravCylinder(x0,z0,0,-130,opt.gravity.drho(1),50);


%??Show geometries and gravity signal
plotgravSVG(PathCoord,x0,z0,x0,gobsa, opt);
