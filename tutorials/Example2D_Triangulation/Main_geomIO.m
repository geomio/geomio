% ==========================================
% How to mesh a rabbit?
% ==========================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================


clc, clear, close all
%% Initialize geomIO and extract coordinates
opt                 = geomIO_Options(); 
opt.inputFileName   = ['./Input/Lapinou.svg'];
[PathCoord]  = readSVG(opt); 


%% Define a set of markers
xmin = 0; xmax = opt.svg.width; 
ymin = 0; ymax = opt.svg.height;
[Xp,Yp]      = ndgrid(xmin:15:xmax,ymin:15:ymax);
Phase        = zeros(size(Xp));

%% Plot your initial drawing
figure(1)
plotSVG(PathCoord,opt)

% Get drawing coordinate of the rabbit
myPath       = fieldnames(PathCoord.LapinouLayer);
LapinouCoord = PathCoord.LapinouLayer.myDrawing.DrawCoord;

%% Body fitted finite element meshing routine using mtriangle 
% mtriangle is a matlab  interface from Triangle (Shewchuk, 1996), part of
% the mutils package (https://sourceforge.net/projects/milamin/files/)


% Extract rabbit's subpaths (in this case, there is just one)
SubPathsLapinou = PathCoord.LapinouLayer.myDrawing.StartSubPath(2:end);

% Define a model box
Box  = [xmin xmin xmax xmax;
        ymin ymax ymax ymin];

% Create a 2*M matrix containing all contour coordinates
Contours        = [Box LapinouCoord];
% Pos contains the beginning index of inidividual contours
Pos             = cumsum([1 size(Box,2) SubPathsLapinou-1]);

% Create segments
for i = 1:length(Pos)-1
    Is = Pos(i);   Ie = Pos(i+1)-1;  % Starting and Ending index
    segments(:,Is:Ie) = [Is:Ie ; Is+1:Ie Is];
end

% Generate mesh using M. Dabrowski's matlab interface to JR Shewchuk's Triangle
tri_opts = struct('element_type','tri3', 'min_angle', 30);
tristr   = struct('points', Contours, 'segments', uint32(segments), 'regions', [ 400 600 1 -1 ]');
[Mesh]   = mtriangle(tri_opts, tristr);

% Plot
figure(2)
X = Mesh.NODES(1,:);    Z = Mesh.NODES(2,:);
patch(X(Mesh.ELEMS(1:3,:)), Z(Mesh.ELEMS(1:3,:)),Mesh.elem_markers, 'LineWidth',.1)
[CMAP, PhaseMax] = getColormapFromPathCoord(PathCoord, opt); 
% colormap(CMAP)
colorbar
axis equal; drawnow
