% ==========================================
% Example of a geodynamic model including
% topography and several types of vertical
% temperature profiles
% ==========================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

clc, clear, close all
opt = geomIO_Options();
%% User input
% Files
BaseDir = ['./'];

opt.inputDir = [BaseDir 'Input/'];
opt.outputDir = [BaseDir 'Output/'];

opt.inputFileName  = ['TemperatureExample.HZ.svg'];

opt.PolygonsFileName = [opt.inputDir '/InputLaMEM/Polygon.bin'];
opt.LaMEMinputFileName = ['/InputLaMEM/TemperatureExample.dat'];

% Related to input file
opt.orient              = 'HZ';
opt.closedContours      = true;

opt.setup.units     = 'geo';
opt.setup.W         = 4000;
opt.setup.L         = 5000;
opt.setup.H         = 770;

opt.setup.x_left = 0;
opt.setup.y_front = -5000;
opt.setup.z_bot = -760;

opt.zi = [-760,-30,-15:4];

opt.shiftPVobj = [1 1 1];

Myrs = 3600*24*365.25*1e6;

%% Define temperature profile types
%% CC Europe
TempModel{1}.TprofType   = 'linear';
TempModel{1}.noLayers    = 1;
TempModel{1}.Tgradient   = [0.025 0.01]; % Tgradient  for n+1 layers (the final gradient runs until the bottom)
TempModel{1}.Depth       = [-30 -95]*1e3; % Depth until which the given Tgradient is applied

%% CC Adria
TempModel{2}.TprofType   = 'linear';
TempModel{2}.noLayers    = 1;
TempModel{2}.Tgradient   = [0.02 0.003 ]; % Tgradient  for each layer
TempModel{2}.Depth       = [-45 -250]*1e3; % Depth until which the given Tgradient is applied


%% Young Ocean
TempModel{3}.TprofType   = 'HSC'; % half space cooling model
TempModel{3}.Age         = 5 * Myrs;
TempModel{3}.AgeMap      = [];

%% Old Ocean
TempModel{4}.TprofType   = 'HSC'; % half space cooling model
TempModel{4}.Age         = 200 * Myrs;
TempModel{4}.AgeMap      = [];


%% Read file and create volumes
[PathCoord,Volumes] = run_geomIO(opt);
plotVolumes(Volumes)

%% Write temperature file
PathLabels  = { 'OuterOcean_oc', 'OceanTransition_oc', 'Continent01_cc', 'Continent02_cc', 'InnerOcean_oc'}; % in drawing order from bottom to top
TempType    = [     4                      4                1                    1               3       ]; % Type of temperature profile

nx = 201; ny = 201; nz = 201;

PathLabels_positive = PathLabels; % Positive topography
PathLabels_negative = {'Air'};    % Negative topography. Those volumes are assigned after to dig the topography assigned to the positive volumes
[Topo, X2D, Y2D] = initTopo(opt,Volumes, PathLabels_positive, PathLabels_negative, nx, ny);
[TypeMap, AgeMap, X2D, Y2D, X3D, Y3D, Z3D, T3D] = initTemp(opt, PathCoord, TempModel, PathLabels, TempType,  nx, ny, nz, Topo);

figure(2)
title('Topography')
surf(X2D,Y2D,Topo); shading interp; camlight
view(95,54)


