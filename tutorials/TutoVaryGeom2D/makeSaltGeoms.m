% =========================================================================
% Example code to create geometry variations of salt diapir
% 
% A. Spang, Mainz, 03/2022
% =========================================================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
%        Arne Spang       [arspang@uni-mainz.de]
% =========================================================================


clear
% set rng seed for reproducibility
rng(200877)

% basic geomIO settings
inputFile              = './Input/Diapir.EW.svg';
opt                    = geomIO_Options();
opt.outputDir          = './Output_Salt';
opt.inputFileName      = inputFile;
opt.LaMEMinputFileName = './Input/Salt.dat';
opt.readLaMEM          = true;
opt.writeParaview      = true;
opt.writePolygons      = true;
opt.interp             = true;
opt.zi                 = [-9:1:9];
opt.getPolygons        = true;

% volumes
paths = {
    'Air', 0, 0
    'Crust', 1, 0
    'Diapir', 2, 0
    'SaltBed', 2, 0
     };
opt.pathNames       = {paths{:,1}}; 
opt.phase           = [paths{:,2}];
opt.type            = [paths{:,3}];

% vary geom settings
opt.varyGeom.do             = true;                  % activate geometry vatriation
opt.varyGeom.volume         = 'Diapir';              % name of volume in Inkscape
opt.varyGeom.dim            = '2D_X';                % mode
opt.varyGeom.genType        = 'grid';                % how to generate scaling parameters
opt.varyGeom.Grid.lb        = [0.5; 0.5; 0.5; 0.5];  % lower bound for scaling parameter generation
opt.varyGeom.Grid.ub        = [2.0; 3.0; 2.0; 1.5];  % upper bound for scaling parameter generation
opt.varyGeom.Grid.num       = [2; 2; 2; 2];          % number of samples for each scaling parameter
opt.varyGeom.Grid.Sz        = [0.8; 1.1; 2];         % options for vertical scaling parameter generation
opt.varyGeom.Grid.noise     = 0.3;                   % noise to be added to scaling parameters
% opt.varyGeom.genType        = 'normal';              % how to generate scaling parameters
% opt.varyGeom.n              = 32;                    % number of variations
% opt.varyGeom.mu             = 1;                     % mean of scaling parameters
% opt.varyGeom.sigma          = 0.3;                   % standard deviation of scaling parameters
% opt.varyGeom.Sz             = [1, 0.08];             % mean and standard deviation of vertical scaling
opt.varyGeom.stretchType    = 'factor';              % scale by factor or absolute value
opt.varyGeom.SzType         = 'bot';                 % reference level for vertical stretching
opt.varyGeom.CtrlPoly       = [153, 253, 385, 482];  % indices of control polygons
opt.varyGeom.CP_ref         = 'global';              % reference system for control polygons
opt.varyGeom.CP_track       = true;                  % control polygons cahnge depth with vertical scaling
opt.varyGeom.outName        = './SaltExample';       % output name
opt.varyGeom.outNameOffset  = 0;                     % offset numbering of output
opt.varyGeom.writeParaview  = false;                 % write .vtk files
opt.varyGeom.writePolygons  = true;                  % write LaMEM polygons
opt.varyGeom.drawOutlines   = true;                  % draw outline along profile
opt.varyGeom.outlineProf    = [0, 0];                % coordinates of outline profile

% run geomIO
[PathCoord,Volumes,opt] = run_geomIO(opt,'default');




% visualize diapir
polygons = Volumes.Diapir.Polygons;
figure()
hold on; box on
for i = 1 : length(polygons)
    plot(polygons{i}(:,1),polygons{i}(:,3),'k')
end

% visualize control polygons
CtrlPoly = [153, 253, 385, 482];
for i = 1 : 4
    plot(polygons{CtrlPoly(i)}(:,1),polygons{CtrlPoly(i)}(:,3),'r','LineWidth',2)
end

% visualize produced variations
load Output_Salt/Outlines.mat
figure()
hold on, box on
for i = 1 : size(Outline,2)-1
    plot(Outline{1,i}(:,1),Outline{1,i}(:,2),'k')
end
plot(Outline{1,33}(:,1),Outline{1,33}(:,2),'r','LineWidth',2)
axis([-10 10 -16.5 0])