% =========================================================================
% 3D Example code to vary a subduction setting
% 
% A. Spang, Mainz, 03/2022
% =========================================================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
%        Arne Spang       [arspang@uni-mainz.de]
% =========================================================================

clear

% basic geomIO settings
inputFile           = ['CurvedSlab.EW.svg'];
opt                 = geomIO_Options();
opt.outputDir       = ['./Output_Subduction'];
opt.inputFileName   = inputFile;
opt.LaMEMinputFileName ='CurvedSlab.dat';
opt.readLaMEM       = true;
opt.writeParaview   = true;
opt.writePolygons   = true;
opt.interp          = true;
opt.zi              = [7000:10:8000];
opt.getPolygons     = true;
opt.shiftPVobj      = [2e-1 2e-2 2e-1];

% varySub settings
opt.varySub.do      = true;         % activate subduction variation
opt.varySub.vols    = {'Slab2'};    % name of volume in Inkscape
opt.varySub.ref     = 'trench';     % reference system of rotation center coordinates
opt.varySub.xRot    = [0];          % rotation center coordinates
opt.varySub.theta   = [-20];        % angles of rotation
opt.varySub.tolZ    = [1];          % tolerance when identifying the plate

opt.varySub.addWZ   = true;         % add a weak zone
opt.varySub.d_WZ    = 20;           % thickness of weak zone
opt.varySub.ID_WZ   = 1;            % LaMEM ID
opt.varySub.type_WZ = 0;            % LaMEM marker type
opt.varySub.d_Lith  = 100;          % thickness of overriding plate

opt.varySub.addCrust= true;         % add crust
opt.varySub.d_OC    = 10;           % thickness of crust
opt.varySub.ID_OC   = 2;            % LaMEM ID
opt.varySub.type_OC = 0;            % LaMEM marker type


% volumes
paths = {
    'Slab2', 5, 0
    };
opt.pathNames       = {paths{:,1}}; 
opt.phase           = [paths{:,2}];
opt.type            = [paths{:,3}];

% Run geomIO
[PathCoord,Volumes,opt] = run_geomIO(opt,'default');