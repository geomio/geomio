clear

if exist('Output_Magma','dir')
    deleteDir('Output_Magma')
end
if exist('Output_Subduction','dir')
    deleteDir('Output_Subduction')
end
if isfile('PetscVec.mat')
    delete PetscVec.mat
end




%% supporting functions
function deleteDir(dirName)
list = dir(dirName);
for i = 1 :length(list)
    if ~list(i).isdir
        delete([dirName, '/', list(i).name]);
    elseif list(i).isdir && ~(strcmp(list(i).name,'.') || strcmp(list(i).name,'..'))
        subList = dir([dirName, '/', list(i).name]);
        for j = 1 : length(subList)
            if ~subList(j).isdir
                delete([dirName, '/', list(i).name, '/', subList(j).name])
            end
        end
        rmdir([dirName, '/', list(i).name])
    end
end
rmdir(dirName)
end