% =========================================================================
% 3D Example code to vary a magma body
% 
% A. Spang, Mainz, 03/2022
% =========================================================================

% =========================================================================
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   Main_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
%        Arne Spang       [arspang@uni-mainz.de]
% =========================================================================
clear

% basic geomIO settings
inputFile                   = 'Magma.HZ.svg';
opt                         = geomIO_Options();
opt.outputDir               = ['./Output_Magma'];
opt.inputFileName           = inputFile;
opt.LaMEMinputFileName      ='Magma.dat';
opt.readLaMEM               = true;
opt.writeParaview           = true;
opt.writePolygons           = true;
opt.interp                  = true;
opt.zi                      = [-80:1:15];
opt.getPolygons             = true;

% volumes
paths = {
    'APMB_HZ', 4, 1
     };
opt.pathNames               = {paths{:,1}}; 
opt.phase                   = [paths{:,2}];
opt.type                    = [paths{:,3}];

% vary geom settings
opt.varyGeom.do             = true;                  % activate geometry vatriation
opt.varyGeom.volume         = 'APMB_HZ';             % name of volume in Inkscape
opt.varyGeom.dim            = '3D';                  % mode
opt.varyGeom.stretchType    = 'factor';              % scale by factor or absolute value
opt.varyGeom.SzType         = 'com';                 % reference level for vertical stretching
opt.varyGeom.CtrlPoly       = [50 60];               % indices of control polygons
opt.varyGeom.CP_ref         = 'volume';              % reference system for control polygons
opt.varyGeom.CP_track       = true;                  % control polygons change depth with vertical scaling
opt.varyGeom.outName        = 'Magma';               % output name
opt.varyGeom.writeParaview  = true;                  % write .vtk files
opt.varyGeom.writePolygons  = true;                  % write LaMEM polygons
opt.varyGeom.drawOutlines   = true;                  % draw outline along profile
opt.varyGeom.outlineProf    = [7536 -12.5];          % coordinates of outline profile
opt.varyGeom.loadStretch    = 'ExampleParams.mat';   % file that contains scaling parameters

% run geomIO
[PathCoord,Volumes,opt] = run_geomIO(opt,'default');