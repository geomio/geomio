![](../doc/geomIO_logo.png)

### Tutorials 

This folder contains tutorials, examples and benchmarks. 

Tutorials are accompanied by a step by step description in the paper and/or in the wiki.

- Tuto2D_Basic - Produces Fig. 1B of the paper; [wiki description](https://bitbucket.org/geomio/geomio/wiki/Basic2Dtutorial)
- Tuto3D_Basic - Produces Fig. 2    of the paper; [wiki description](https://bitbucket.org/geomio/geomio/wiki/Basic3Dtutorial)
- Tuto3D_Geodynamics - Produces Fig. S2 of the paper's supplement

Examples are more advanced applications. The files are well commented but a step by step description is not provided.

- Example2D_Gravity - A basic gravity example
- Example2D_Triangulation - Produces Fig. 1C of the paper
- Example3D_Slab_temperature - Produces the temperature model of the Japanese subduction zone shown in Fig. 6 of the paper

Benchmarks demonstrate the numerical accuracy of the gravity model

- Benchmark2D_Gravity
- Benchmark3D_Gravity

Individual example folders contain:

- Input - folder containing svg files
- Output - empty folder (filled up with VTK files of Volumes if this option is activated)
- Main_geomIO.m - The single script that user must run to call geomIO's functions
- xxx.m - additional functions specific to the application



### Licence

    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
    All rights reserved.

    geomIO is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, version 3 of the License.

    geomIO is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with geomIO. If not, see <http://www.gnu.org/licenses/>.

[Full license is available here](./LICENSE.txt)


### How to use

[Documentation is available here](https://bitbucket.org/geomio/geomio/wiki/Home)


### Acknowledgements 

geomIO's development has been supported by the European Research Council under the European Community's Seventh Framework program (FP7/2007/2013) with ERC starting grant agreement no.258830.

![](/doc/docPics/ERC_logo.jpg)

![](/doc/docPics/2000px-Johannes_Gutenberg-Universitat_Mainz_logo.svg.png)