function outline = getOutline(Polygons,orient,Coord)
    numPoly = length(Polygons);
    outline = zeros(numPoly*2+1,2);
    switch orient
        case 'EW'
            dimX = 1;
            dimN = 2;
            
        case 'NS'
            dimX = 2;
            dimN = 1;
        otherwise
            error('Wrong orientaion in getOutline!')
    end
    
    % warning flag
    warn = 0;
    
    for iPoly = 1 : numPoly
        % access Polygon
        Pol = Polygons{iPoly};
        
        % find center of gravity
        [geom,~,~]  = polygeom(Pol(:,1),Pol(:,2));
        center      = [geom(2) geom(3)];
        
        % divide polygon in two parts
        indsPart{1} = find(Pol(:,dimX) > center(dimX));
        indsPart{2} = find(Pol(:,dimX) < center(dimX));
        
        for iPart = 1 : 2
            indPart = indsPart{iPart};
            
            % reorder part in case of a jump in coordinates
            if max(diff(indPart)) > 1
                indJump     = find(diff(indPart) > 1);
                indPart     = [indPart(indJump+1:end); indPart(1:indJump)];
            end
            
            % distance to profile of every node
            dist            = Pol(indPart,dimN) - Coord;
            
            % find closest pair
            if dist(1) > 0
                ind_2       = find(dist < 0, 1);
                ind_1       = ind_2 - 1;
            else
                ind_2       = find(dist > 0, 1);
                ind_1       = ind_2 - 1;
            end
            
            % check if a pair is found
            if isempty(ind_1) || isempty(ind_2)
                inter       = NaN;
                warn        = 1;
            else
                points(1,:)     = Pol(indPart(ind_1),:);
                points(2,:)     = Pol(indPart(ind_2),:);
            
                % calculate intersecion between profile and polygon part
                m               = (points(2,dimX) - points(1,dimX))/(points(2,dimN) - points(1,dimN));
                inter           = points(1,dimX) + m * (Coord - points(1,dimN));
            end
            
            % enter into outline matrix
            if iPart == 1
                outline(iPoly,1)                = inter;
                outline(iPoly,2)                = Polygons{iPoly}(1,3);
            elseif iPart == 2
                outline(numPoly*2+1-iPoly,1)    = inter;
                outline(numPoly*2+1-iPoly,2)    = Polygons{iPoly}(1,3);
            end
        end
    end
    
    % issue warning
    if warn
        fprintf('Warning: Some slices are not be present on %s-profile \n',orient);
    end
    
    outline(end,:)                      = outline(1,:);  
end