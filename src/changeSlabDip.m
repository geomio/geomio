function [Volumes, Lat,Lon,Depth] = changeSlabDip(Volumes, Lat, Lon, Depth, Vol_drawDir, Vol_sliceDir, opt)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville, Tobias S. Baumann &
%    Arne Spang
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   changeSlabDip.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
%        Arne Spang       [arspang@uni-mainz.de]
% =========================================================================

% Variables:
% phi       = angle between average dip direction of slab and x-direction
% alpha     = average dip angle of the entire slab
% alpha_app = apparant average dip angle of the entire slab in the profiles

% access options
% reference system
ref     = opt.varySub.ref;
% rotation CEnters
xRot    = opt.varySub.xRot;
% change in slab angle
Theta   = opt.varySub.theta;
% tolerance of plate top
tolZ    = opt.varySub.tolZ;
% tolerance for angles
tolDip  = 1;

% get relavant coordinates
switch opt.orient
    case 'EW'
        Plate.X = Lon;
        Plate.Y = Lat;
    case 'NS'
        Plate.X = Lat;
        Plate.Y = Lon;
    otherwise
        error('Orientation must be EW or NS')          
end
Plate.Z = Depth;

numNodes    = size(Plate.X,1);
numSlices   = size(Plate.X,2);

All.LatTop  = zeros(numSlices,1); 
All.LonTop  = zeros(numSlices,1);
All.DepTop  = zeros(numSlices,1);
All.LatBot  = zeros(numSlices,1); 
All.LonBot  = zeros(numSlices,1);
All.DepBot  = zeros(numSlices,1);
All.LatLow  = zeros(numSlices,1); 
All.LonLow  = zeros(numSlices,1);
All.DepLow  = zeros(numSlices,1);
All.dipDir  = cell(numSlices,1);
All.indTop  = zeros(numSlices,1);
All.indBot  = zeros(numSlices,1);
All.indLow  = zeros(numSlices,1);
All.drawDir = cell(numSlices,2);
All.dirVec  = zeros(numNodes,2,numSlices);
All.Top     = zeros(numSlices,1);
All.Bot     = zeros(numSlices,1);

if opt.varySub.addWZ
    WeakZone.XZ = cell(numSlices,1);
    WeakZone.Y  = zeros(numSlices,1);
end

if opt.varySub.addCrust
    Crust.XZ = cell(numSlices,1);
    Crust.Y  = zeros(numSlices,1);
end


for iSlice = 1 : numSlices
    X = Plate.X(:,iSlice);
    Z = Plate.Z(:,iSlice);

    % check polygon direction
    edges = (X(2:end)-X(1:end-1)) .* (Z(2:end)+Z(1:end-1));
    if sum(edges) > 0
        drawDir = 'clockwise';
    else
        drawDir = 'anticlockwise';
    end
    
    % get angles at every node
    [dirVec, dirDeg, dipDeg] = findAngles(X,Z);
   
    % find lowest 
    indLow  = find(Z == min(Z));
    Low     = [X(indLow) Z(indLow)];
    
    % find top nodes
    Top         = max(Z);
    indTop      = find(Z >= Top - tolZ & dipDeg > -tolDip & dipDeg < tolDip);

    % find nodes at the edges of the top
    indLeftTop  = find(X == min(X(indTop)));
    indRightTop = find(X == max(X(indTop)));
    % starting and end node might overlap
    indLeftTop  = indLeftTop(1);
    indRightTop = indRightTop(1);
    
    % take a node in the middle of the top and find a node on the oppposite
    % side (the bottom of the plate)
    xMiddle     = (X(indLeftTop) + X(indRightTop))/2;
    distMiddle  = abs(X(indTop) - xMiddle);
    
    ind         = indTop(distMiddle == min(distMiddle));
    indPartner  = findPartner(X,Z,ind,dirVec(ind,:),drawDir);

    Bot         = Z(indPartner);
    indBot      = find(Z >= (Bot-tolZ) & Z <= (Bot+tolZ) &...
                       dipDeg > -tolDip & dipDeg < tolDip &...
                       X >= X(indLeftTop) & X <= X(indRightTop));

    % find nodes at the edges of the bottom
    indLeftBot  = find(X == min(X(indBot)));
    indRightBot = find(X == max(X(indBot)));
    % starting and end node might overlap
    indLeftBot  = indLeftBot(1);
    indRightBot = indRightBot(1); 
    
    % find dip direction and nodes were slab starts and ends
    if Low(1) > X(indRightTop)
        dipDir      = 'right';
        indTrench   = indRightTop;
        indBot      = indRightBot;
    elseif Low(1) < X(indLeftTop)
        dipDir      = 'left';
        indTrench   = indLeftTop;
        indBot      = indLeftBot;
    else
        error('Slab dip is apparantly > 90 degrees')
    end
    
    % save relevant info
    switch opt.orient
        case 'EW'
            All.LatTop(iSlice) = unique(Lat(:,iSlice));
            All.LonTop(iSlice) = X(indTrench);
            All.LatBot(iSlice) = unique(Lat(:,iSlice));
            All.LonBot(iSlice) = X(indBot);
            All.LatLow(iSlice) = unique(Lat(:,iSlice));
            All.LonLow(iSlice) = Low(1);
        case 'NS'
            All.LatTop(iSlice) = X(indTrench);
            All.LonTop(iSlice) = unique(Lon(:,iSlice));
            All.LatBot(iSlice) = X(indBot);
            All.LonBot(iSlice) = unique(Lon(:,iSlice));
            All.LatLow(iSlice) = Low(1);
            All.LonLow(iSlice) = unique(Lon(:,iSlice));           
    end
    
    All.DepTop(iSlice)      = Z(indTrench);
    All.DepBot(iSlice)      = Z(indBot);
    All.DepLow(iSlice)      = Low(2);  
    All.dipDir{iSlice}      = dipDir;
    All.indTop(iSlice)      = indTrench;
    All.indBot(iSlice)      = indBot;
    All.indLow(iSlice)      = indLow;
    All.drawDir{iSlice}     = drawDir;
    All.dirVec(:,:,iSlice)  = dirVec;
    All.Top(iSlice)         = Top;
    All.Bot(iSlice)         = Bot;
end

% check that dip direction is the same everywhere
if length(unique(All.dipDir)) > 1
    error('Slabs seem to dip into different directions')
end

% compute vectors along SlabTop, SlabBot and SlabLow to get a mean
% orientation
VecTop = [All.LonTop(end)-All.LonTop(1) All.LatTop(end)-All.LatTop(1) All.DepTop(end) - All.DepTop(1)];
VecTop = VecTop / norm(VecTop);
VecBot = [All.LonBot(end)-All.LonBot(1) All.LatBot(end)-All.LatBot(1) All.DepBot(end) - All.DepBot(1)];
VecBot = VecBot / norm(VecBot);
VecLow = [All.LonLow(end)-All.LonLow(1) All.LatLow(end)-All.LatLow(1) All.DepLow(end) - All.DepLow(1)];
VecLow = VecLow / norm(VecLow);
VecMean = (VecTop + VecBot + VecLow)/3;

% compute real dip direction
if strcmp(dipDir,'right')
    RealDipDir = cross(VecMean,[0 0 1]); 
else
    RealDipDir = cross(VecMean,[0 0 -1]);
end

% real dip direction as angle anticlockwise from EAST
phi = atand(RealDipDir(2)/RealDipDir(1));

% go through slices
for iSlice = 1 : numSlices
    % get coordinates of slice
    X       = Plate.X(:,iSlice);
    Z       = Plate.Z(:,iSlice);
    
    % extract info
    dipDir      = All.dipDir{iSlice};
    indTrench   = All.indTop(iSlice);
    indLow      = All.indLow(iSlice);
    drawDir     = All.drawDir{iSlice};
    dirVec      = All.dirVec(:,:,iSlice);
    Top         = All.Top(iSlice);
    Bot         = All.Bot(iSlice);
    
    % check reference system and recompute xRot of that slice
    if strcmp(ref,'trench')
        xRotSlice = X(indTrench) + xRot;
    end
    
    % number of rotation centers
    numCenters  = length(xRotSlice);
    indRotTop   = zeros(numCenters,1);
    indRotBot   = zeros(numCenters,1);
    
    % loop over rotation centers and find bordering indices
    for iCenter = 1 : numCenters
        if strcmp(ref,'trench') && xRot(iCenter) == 0
            indRotTop(iCenter) = indTrench;
            indRotBot(iCenter) = All.indBot(iSlice);
        else
            % find rotation node on top
            indRotTop(iCenter)  = findClosestTopNodeX(X,Z,xRotSlice(iCenter));
            % find partner on bottom side
            indRotBot(iCenter)  = findPartner(X,Z,indRotTop(iCenter),...
                                              dirVec(indRotTop(iCenter),:),drawDir);
        end
    end
    
    % loop over rotation centers and actually rotate
    for iCenter = 1 : numCenters
        if strcmp(ref,'trench') && xRot(iCenter) == 0
            checkTrench = true;
        else
            checkTrench = false;
        end
        indTop         = indRotTop(iCenter);
        indBot         = indRotBot(iCenter);
        rotCen         = [(X(indTop) + X(indBot))/2 (Z(indTop) + Z(indBot))/2];
        theta          = Theta(iCenter);
        [~,indSlabBot] = min(Z);
        
        % devide nodes in 2 groups
        % slab: will be rotated, ordered from top to bottom
        % stable: wil not move, ordered from bottom to top
        if (strcmp(dipDir,'right') && strcmp(drawDir,'clockwise')) ||...
           (strcmp(dipDir,'left')  && strcmp(drawDir,'anticlockwise'))
            if indBot > indTop
                    indSlab     = (indTop:1:indBot);
                    indStable   = [indBot+1:1:numNodes 1:1:indTop-1];
                    indSlabTop  = (indTop:1:indSlabBot);
                else
                    indSlab     = [indTop:1:numNodes 1:1:indBot];
                    indStable   = (indBot+1:indTop-1);
                    if indSlabBot > indTop
                        indSlabTop = (indTop:1:indSlabBot);
                    else
                        indSlabTop = [indTop:1:numNodes 1:1:indSlabBot];
                    end
                    
            end
        else
            if indTop > indBot
                    indSlab     = (indTop:-1:indBot);
                    indStable   = [indBot-1:-1:1 numNodes:-1:indTop+1];
                    indSlabTop  = (indTop:-1:indSlabBot);
                else
                    indSlab     = [indTop:-1:1 numNodes:-1:indBot];
                    indStable   = (indBot-1:-1:indTop+1);
                    if indTop > indSlabBot
                        indSlabTop = (indTop:-1:indSlabBot);
                    else
                        indSlabTop = [indTop:-1:1 numNodes:-1:indSlabBot];
                    end
            end
        end
        
        % get apparant dip of slab
        if strcmp(dipDir,'right')
            alpha_app = -atand((Z(indLow)-Z(indBot)) / (X(indLow)-X(indBot)));
        else
            alpha_app =  atand((Z(indLow)-Z(indBot)) / (X(indLow)-X(indBot)));
        end
        
        % calculate real dip of slab
        alpha           = atand(tand(alpha_app) / cosd(phi));
        % change real dip of the slab
        alphaNew        = alpha + theta;
        % change back into apparant dip
        alpha_appNew    = atand(tand(alphaNew) * cosd(phi));
        % calculate change in apparant angle
        theta_app       = alpha_appNew - alpha_app;
        
        % get slab coordinates
        Slab        = [X(indSlab),Z(indSlab)];
        % transform coordinates relative to rotation center (start of slab)
        Slab        = Slab - rotCen;
        
        % build rotation matrix
        if strcmp(dipDir,'right')
            R = [cosd(-theta_app) -sind(-theta_app);...
                 sind(-theta_app)  cosd(-theta_app)];
        else
            R = [cosd(theta_app) -sind(theta_app);...
                 sind(theta_app)  cosd(theta_app)];
        end
        
        % rotate SLab
        Slab    = R * Slab';
        % transform back to absolute coordinates
        Slab    = Slab' + rotCen;
        % put slab coordinates back into coordinate vector
        Xnew            = X;
        Znew            = Z;
        Xnew(indSlab)   = Slab(:,1);
        Znew(indSlab)   = Slab(:,2);
        
        % check overlap
        [Xnew, Znew] = checkResolveOverlap(X,Z,Xnew,Znew,indSlab,indStable,dipDir,theta,checkTrench,Top,Bot);
        
        X = Xnew;
        Z = Znew;       
    end
    
    % find entire slab top (from trench to the bottom)
    indTop         = indTrench;
    [~,indSlabBot] = min(Z);
    if (strcmp(dipDir,'right') && strcmp(drawDir,'clockwise')) ||...
       (strcmp(dipDir,'left')  && strcmp(drawDir,'anticlockwise'))
        if indSlabBot > indTop
            indSlabTop  = (indTop:1:indSlabBot);
        else
            indSlabTop  = [indTop:1:numNodes 1:1:indSlabBot];
            
        end
    else
        if indTop > indSlabBot
            indSlabTop  = (indTop:-1:indSlabBot);
        else
            indSlabTop = [indTop:-1:1 numNodes:-1:indSlabBot];
        end
    end    
    
    % make weak zone
    if opt.varySub.addWZ
        [~, dirDeg, ~]      = findAngles(X,Z);
        WeakZone.XZ{iSlice} = makeWeakZone(X, Z, indSlabTop, dirDeg, dipDir, opt);
        WeakZone.Y(iSlice)  = Plate.Y(1,iSlice);
    end
    
    % make oceanic crust
    if opt.varySub.addCrust
        Crust.XZ{iSlice}    = makeOceanCrust(X, Z, indSlabTop, dipDir, opt);
        Crust.Y(iSlice)     = Plate.Y(1,iSlice);
    end
    
    % return coordinates of slice
    Plate.X(:,iSlice) = X;
    Plate.Z(:,iSlice) = Z;
end

% return coordinates
switch opt.orient
    case 'EW'
        Lon = Plate.X;
    case 'NS'
        Lat = Plate.X;       
end
Depth = Plate.Z;

% make weak zone volume
if opt.varySub.addWZ
    % check length of polygons
    lens = zeros(numSlices,1);
    for iSlice = 1 : numSlices
        lens(iSlice) = size(WeakZone.XZ{iSlice},1);
    end
    
    % copy them over if all polygons have the same number of nodes
    % interpolate new ones if they are different
    if length(unique(lens)) == 1
        WZ_numNodes = size(WeakZone.XZ{1},1);
        WZ_X        = zeros(WZ_numNodes,numSlices);
        WZ_Y        = ones(WZ_numNodes,numSlices);
        WZ_Z        = zeros(WZ_numNodes,numSlices);
        for iSlice = 1 : numSlices
            WZ_X(:,iSlice) = WeakZone.XZ{iSlice}(:,1);
            WZ_Y(:,iSlice) = WZ_Y(:,iSlice) * WeakZone.Y(iSlice);
            WZ_Z(:,iSlice) = WeakZone.XZ{iSlice}(:,2);
        end
    else
        warning('Created weak zone has different number polygon nodes on different profiles. Interpolation is necessary. \nPlease check the results.')
        WZ_numNodes = 2*numNodes;
        WZ_X        = zeros(WZ_numNodes,numSlices);
        WZ_Y        = ones(WZ_numNodes,numSlices);
        WZ_Z        = zeros(WZ_numNodes,numSlices);
        for iSlice = 1 : numSlices
            newPoly        = interparc(WZ_numNodes,WeakZone.XZ{iSlice}(:,1),WeakZone.XZ{iSlice}(:,2),'linear');
            WZ_X(:,iSlice) = newPoly(:,1);
            WZ_Y(:,iSlice) = WZ_Y(:,iSlice) * WZ.Y(iSlice);
            WZ_Z(:,iSlice) = newPoly(:,2);
        end
    end
    
    switch opt.orient
        case 'EW'
            WZ_Lon = WZ_X;
            WZ_Lat = WZ_Y;
        case 'NS'
            WZ_Lat = WZ_X;
            WZ_Lon = WZ_Y;
    end
    WZ_Dep = WZ_Z;
    
    Volumes = makeVolWZ(Volumes, WZ_Lon, WZ_Lat, WZ_Dep, Vol_drawDir, Vol_sliceDir, opt);
end

% make crust volume
if opt.varySub.addCrust
    % check length of polygons
    lens = zeros(numSlices,1);
    for iSlice = 1 : numSlices
        lens(iSlice) = size(Crust.XZ{iSlice},1);
    end
    
    % copy them over if all polygons have the same number of nodes
    % interpolate new ones if they are different
    if length(unique(lens)) == 1
        OC_numNodes = lens(1);
        OC_X        = zeros(OC_numNodes,numSlices);
        OC_Y        = ones(OC_numNodes,numSlices);
        OC_Z        = zeros(OC_numNodes,numSlices);
        for iSlice = 1 : numSlices
            OC_X(:,iSlice) = Crust.XZ{iSlice}(:,1);
            OC_Y(:,iSlice) = OC_Y(:,iSlice) * Crust.Y(iSlice);
            OC_Z(:,iSlice) = Crust.XZ{iSlice}(:,2);
        end
    else
        fprintf('Warning: Created oceanic crust has different number of polygon nodes on different profiles. Interpolation was necessary. You may want to check the results.')
        OC_numNodes = 2*numNodes;
        OC_X        = zeros(OC_numNodes,numSlices);
        OC_Y        = ones(OC_numNodes,numSlices);
        OC_Z        = zeros(OC_numNodes,numSlices);
        for iSlice = 1 : numSlices
            newPoly        = interparc(OC_numNodes,Crust.XZ{iSlice}(:,1),Crust.XZ{iSlice}(:,2),'linear');
            OC_X(:,iSlice) = newPoly(:,1);
            OC_Y(:,iSlice) = OC_Y(:,iSlice) * Crust.Y(iSlice);
            OC_Z(:,iSlice) = newPoly(:,2);
        end
    end
    
    switch opt.orient
        case 'EW'
            OC_Lon = OC_X;
            OC_Lat = OC_Y;
        case 'NS'
            OC_Lat = OC_X;
            OC_Lon = OC_Y;
    end
    OC_Dep = OC_Z;
    
    Volumes = makeVolOC(Volumes, OC_Lon, OC_Lat, OC_Dep, Vol_drawDir, Vol_sliceDir, opt); 
end

end



%% supporting functions
% check if nodes of the rotated slab now overlap with the stationary part
% and resolve those issues
function [Xnew, Ynew] = checkResolveOverlap(X,Y,Xnew,Ynew,indSlab,indStable,dipDir,theta,trench,Top,Bot)

numSlab     = length(indSlab);
numStable   = length(indStable);

% do we need to check the top or the bottom for overlap?
if theta > 0
    % steepening: check botttom
    step        = -1;
    iterSlab    = (numSlab  :  step : 1);
    iterStable  = (1        : -step : numStable);
elseif theta < 0
    % flattening: check top
    step        = 1;
    iterSlab    = (1        :  step : numSlab);
    iterStable  = (numStable: -step : 1);
else
    return;
end

% count backwards from the lower end of the new and find overlapping nodes
% also find the first two that are not overlapping
% overlapping nodes will be moved between first 2 non-overlapping nodes
In      = [];
for iNode = iterSlab
    x = Xnew(indSlab(iNode));
    y = Ynew(indSlab(iNode));
    if inpolygon(x,y,X,Y)
        In    = [In indSlab(iNode)];
    else
        Out    = indSlab(iNode);
        OutOut = indSlab(iNode+step);
        break;
    end
end
% move overlapping nodes (first non overlapping node is also moved)
if ~isempty(In)
    numNodes = length(In) + 2;
    x = linspace(Xnew(Out),Xnew(OutOut),numNodes);
    y = linspace(Ynew(Out),Ynew(OutOut),numNodes);
    
    Xnew([In Out OutOut]) = x;
    Ynew([In Out OutOut]) = y;
end

% do the same with nodes of old body that overlap with new one
In      = [];
for iNode = iterStable
    x = X(indStable(iNode));
 %   y = Y(indStable(iNode));
    if (strcmp(dipDir,'right') && x > Xnew(indSlab(iterSlab(1)))) ||...
       (strcmp(dipDir,'left')  && x < Xnew(indSlab(iterSlab(1)))) %||...
%       (strcmp(dipDir,'right') && y > Ynew(indSlab(iterSlab(1)))) ||...
%       (strcmp(dipDir,'left')  && y < Ynew(indSlab(iterSlab(1))))
        In    = [In indStable(iNode)];
    else
        Out    = indStable(iNode);
        OutOut = indStable(iNode-step);
        break;
    end
end
% move overlapping nodes (first non overlapping node is also moved)
if ~isempty(In)
    numNodes = length(In) + 2;
    x = linspace(X(Out),X(OutOut),numNodes);
    y = linspace(Y(Out),Y(OutOut),numNodes);
    
    Xnew([In Out OutOut]) = x;
    Ynew([In Out OutOut]) = y;
end

% special case if rotating at the trench
if trench
    % no nodes should be higher than top of the plate
    Ynew(Ynew > Top) = Top;
    % check nodes from the end for being higher than the bottom of
    % the plate
    [~,indBot] = min(Ynew(indSlab)); 
    Bot = Ynew(indStable(1));
    ind = (indBot:1:numSlab);
    for iNode = indBot : numSlab
        if Ynew(indSlab(iNode)) > Bot
            Ynew(indSlab(iNode)) = Bot;
        end
    end
    % find highest point among slab end and check if anyhting is lower
    % afterwards and fix it to remove kinks
    [~,maxInd]         = max(Ynew(indSlab(ind)));
    ind                = ind(maxInd:end);
    Ynew(indSlab(ind)) = interp1([Xnew(indSlab(ind(1))) Xnew(indStable(1))],[Ynew(indSlab(ind(1))) Bot],Xnew(indSlab(ind)));
end

end

% compute the angle in each slab node
function [dirVec, dirDeg, dipDeg] = findAngles(X,Y)
% X and Y must be column vectors of same length
if length(size(X)) > 2 || length(size(Y)) > 2 ||...
   size(X,2) ~= 1 || size(Y,2) ~= 1 ||...
   length(X) ~= length(Y)
    error('X and Y must be column vectors of the same length')
end

% polygons are expected to be closed (X_1 = X_end, Y_1 = Y_end)
tol = 1e-9;
if abs(X(1) -X(end)) > tol || abs(Y(1) - Y(end)) > tol
    error('Polygon has to be closed (X_1 = X_end and Y_1 = Y_end)')
end

diffX       = diff(X);
diffY       = diff(Y);

vecTo       = [diffX diffY];
vecTo       = [X(1) - X(end-1) Y(1) - Y(end-1); vecTo];

vecFrom     = [diffX diffY];
vecFrom     = [vecFrom; X(2) - X(end) Y(2) - Y(end)];

vecAt       = (vecTo + vecFrom) / 2;
dirVec      = vecAt ./ vecnorm(vecAt,2,2);

[dirDeg, dipDeg]    = getDirection(dirVec(:,1),dirVec(:,2));  
end

% find top of the slab for rotation center
function ind = findClosestTopNodeX(X,Y,x)
% distance in x for every node
dist = abs(X - x);

[~,order] = sort(dist);

% loop until we find the first node that is on top off the polygon
for iNode = 1 : length(X)
    xy = [X(order(iNode)) Y(order(iNode))];
    % move slightly down
    xy(2) = xy(2) - 1e-3;
    % if we are still in the polygon, then we must be on top
    if inpolygon(xy(1),xy(2),X,Y)
        break;
    end
    % if we do not find any, give an error
    if iNode == length(X)
        error('Failed to find top of the polygon')

    end
end

ind = order(iNode);
end

% for a single node, this finds the node on the opposite side of the
% polygon (perpendicular to the wall)
function indPartner = findPartner(X,Y,ind,dir,drawDir)
% number of bisection iterations
iter = 10;
% coordinate of node
xy  = [X(ind) Y(ind)];

% find direction to search in
switch drawDir
    case 'clockwise'
        dir2 = [dir(2) -dir(1)];
    case 'anticlockwise'
        dir2 = [-dir(2) dir(1)];
    otherwise
        error('Unknown draw direction')
end

% use bisection to find other side of polygon
a   = 0;
b   = max([max(X)-min(X), max(Y) - min(Y)]);
c   = (a+b)/2;
xy_ = xy + dir2 * c;
for i = 1 : iter
    if inpolygon(xy_(1),xy_(2),X,Y)
        a = c;
    else
        b = c;
    end
    c   = (a+b)/2;
    xy_ = xy + dir2 * c;
end

% distance of every node to target
dist = sqrt((X-xy_(1)).^2 + (Y-xy_(2)).^2);

% partner is node with minimum distance to target
indPartner = find(dist == min(dist));
end

% takes vectors and returns their direction in 360 degrees°
% (anticlockwise from +X)
function [Dir, Dip] = getDirection(X,Y)
% X and Y must be column vectors of same length
if length(size(X)) > 2 || length(size(Y)) > 2 ||...
   size(X,2) ~= 1 || size(Y,2) ~= 1 ||...
   length(X) ~= length(Y)
    error('X and Y must be column vectors of the same length')
end

Dir = zeros(length(X),1);
Dip = zeros(length(X),1);

for i = 1 : length(X)
    x = X(i);
    y = Y(i);

    if x == 0
        if y == 0
            dir = 0;
            dip = 0;
        elseif y > 0
            dir = 90;
            dip = 90;
        else
            dir = 270;
            dip = -90;
        end
    else
        alpha = atand(y/x);
        if x > 0 && y >= 0
            dir = alpha;
            dip = alpha;
        elseif x > 0 && y < 0
            dir = alpha + 360;
            dip = alpha;
        else
            dir = alpha + 180;
            dip = alpha;
        end
    end
    Dir(i) = dir;
    Dip(i) = dip;
end

end

% adds oceanic crust at the top of the slab
function Crust = makeOceanCrust(X, Z, indSlabTop, dipDir, opt)
% X,Y        Coordinates of nodes
% indSlabTop indices of top of slab

% read input
d              = opt.varySub.d_OC;
tol            = 1;

% identify nodes along the top of the slab
if strcmp(dipDir,'right')
    [~,indTip] = max(X(indSlabTop));
else
    [~,indTip] = min(X(indSlabTop));
end
indSlabTop     = indSlabTop(1:indTip);

% identify nodes along the top of the plate
indPlateTop    = find(Z > max(Z)-tol);
[~,ia]         = uniquetol(X(indPlateTop),1e-3);
if strcmp(dipDir,'right')
    indPlateTop = indPlateTop(ia);
else
    indPlateTop = flipud(indPlateTop(ia));
end

% combine nodes to find top of plate
ind            = unique([indPlateTop; indSlabTop'],'stable');
Top.X          = X(ind);
Top.Z          = Z(ind);

% create coat around top
buffer         = polybuffer([Top.X Top.Z],'lines',d);
Coat.X         = buffer.Vertices(:,1);
Coat.Z         = buffer.Vertices(:,2);

% reorder Coat so that starts at a consistent location
[~,indMin]     = min(Coat.Z);
Coat.X         = Coat.X([indMin:end, 1:indMin-1]);
Coat.Z         = Coat.Z([indMin:end, 1:indMin-1]);

% remove everything outside the slab
In             = inpolygon(Coat.X, Coat.Z, X, Z);
indFirst       = find(In == true,1,'first');
indLast        = find(In == true,1,'last');

% make new nodes at the ends that are on the slab polygon
newFirst       = findEdge([Coat.X(indFirst),Coat.Z(indFirst)],[Coat.X(indFirst-1),Coat.Z(indFirst-1)],X,Z);
newLast        = findEdge([Coat.X(indLast),Coat.Z(indLast)],[Coat.X(indLast+1),Coat.Z(indLast+1)],X,Z);

% put together
Crust          = [[Top.X; newFirst(1); Coat.X(In); newLast(1); Top.X(1)],[Top.Z; newFirst(2); Coat.Z(In); newLast(2); Top.Z(1)]];

% check for big gaps between nodes
maxGap         = 50;
Dist           = ((Crust(2:end,1) - Crust(1:end-1,1)).^2 + (Crust(2:end,2) - Crust(1:end-1,2)).^2).^(1/2);
bigGaps        = find(Dist > maxGap);
Dist           = Dist(bigGaps);
for i = 1 : length(bigGaps)
    j          = bigGaps(i);
    xz1        = Crust(j,:);
    xz2        = Crust(j+1,:);
    numNew     = floor(Dist(i)/maxGap);
    xNew       = linspace(xz1(1),xz2(1),numNew+2);
    xNew       = xNew(2:end-1);
    
    zNew       = interp1([xz1(1) xz2(1)],[xz1(2) xz2(2)],xNew);
    Crust      = [Crust(1:j,:); [xNew' zNew']; Crust(j+1:end,:)];
    bigGaps    = bigGaps + numNew;
end
end

% find the edge of a polygon between a pair of coordinates
function xy = findEdge(In, Out, PolyX, PolyY)
% try to narrow it down by bisection
% number of bisection iterations
iter = 20;

Test = [(In(1) + Out(1))/2, (In(2) + Out(2))/2];
for i = 1 : iter
    if inpolygon(Test(1),Test(2),PolyX,PolyY)
        In = Test;
    else
        Out = Test;
    end
    Test = [(In(1) + Out(1))/2, (In(2) + Out(2))/2];
end
xy = Test;
end

% adds a weak zone at the top of the slab
function WZ = makeWeakZone(X, Z, ind, dirDeg, dipDir, opt)
% X,Y        Coordinates of nodes
% ind        indices of top of slab
% dipDeg     dipping vector at every node
% topPlate   Z-coordinate of top of plate

% read options
dWeak        = opt.varySub.d_WZ;
botLith      = -opt.varySub.d_Lith;
theta        = opt.varySub.theta(1);

% extend slab by one node
if theta > 0
    ind          = [ind(1)-(ind(2)-ind(1)) ind];
end

% reduce to slab nodes
X            = X(ind);
Z            = Z(ind);
dirDeg       = dirDeg(ind);

% create new nodes for weak zone
if strcmp(dipDir, 'right')
    normals                = dirDeg + 90;
    normals(normals > 360) = normals(normals > 360) - 360;
else
    normals                = dirDeg - 90;
    normals(normals < 0)   = normals(normals < 0) + 360;
end

% Create top of weak zone
Weak.X       = X + cosd(normals) * dWeak;
Weak.Z       = Z + sind(normals) * dWeak;

% connect to slab
Weak.X       = [Weak.X; flipud(X); Weak.X(1)];
Weak.Z       = [Weak.Z; flipud(Z); Weak.Z(1)];

% cut everything that is ablove the plate top
topPlate     = max(Z) - opt.varySub.tolZ/20;
ind1         = find(Weak.Z < topPlate,1,'first')-1;
ind2 = find(Weak.Z < topPlate,1,'last');
z            = topPlate;
x1           = interp1(Weak.Z(ind1:ind1+1),Weak.X(ind1:ind1+1),z);
x2           = interp1(Weak.Z(ind2:ind2+1),Weak.X(ind2:ind2+1),z);
Weak.X       = Weak.X(Weak.Z <= topPlate);
Weak.Z       = Weak.Z(Weak.Z <= topPlate);
Weak.X       = [x1; Weak.X; x2; x1];
Weak.Z       = [z;  Weak.Z; z;  z];

% cut everything below lithosphere
ind1         = find(Weak.Z < botLith,1,'first') - 1;
ind2         = find(Weak.Z < botLith,1,'last');
z            = botLith;
x1           = interp1(Weak.Z(ind1:ind1+1),Weak.X(ind1:ind1+1),z);
x2           = interp1(Weak.Z(ind2:ind2+1),Weak.X(ind2:ind2+1),z);
Weak.X       = [Weak.X(1:ind1); x1; x2; Weak.X(ind2+1:end)];
Weak.Z       = [Weak.Z(1:ind1);  z;  z; Weak.Z(ind2+1:end)];

WZ           = [Weak.X, Weak.Z];
end