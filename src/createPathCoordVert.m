function [PathCoordVert, VolumesVert, opt] = createPathCoordVert(PathCoordVert,VolumesVert, PathCoord, pathLabel, opt)          
% 
% Create a new PathCoord structure for vertical slices
% 
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   createPathCoordVert.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
fprintf(' > Create new slices in %s direction\n', opt.Vert.orient);


% read initial file

% optVert = opt;
% optVert.orient        = opt.Vert.orient;
% optVert.inputFileName = opt.inputFileNameVert;
% optVert.pathNames     = opt.pathNamesVert;
% [~,~, optVert] = readSVG(optVert);


% create a volume but without transformation
transform = opt.transform2Cart;

opt.transform2Cart = false;
VolumesVert = makeVolume(PathCoord, VolumesVert, pathLabel, opt);

opt.transform2Cart = transform;

VolumesVert.(pathLabel).orient    = opt.Vert.orient;
switch opt.Vert.orient
    case 'NS'
        VolumesVert.(pathLabel).normalDir = [1 0 0];
    case 'EW'
        VolumesVert.(pathLabel).normalDir = [0 1 0];
    case 'HZ'
        VolumesVert.(pathLabel).normalDir = [0 0 1];
    otherwise
        error('Unknown orientation')  
end

% The axes of the volume are ordered the usual way: x: 1 y: 2 z: 3 
ax = [1 2 3];

% get coordinates
X = VolumesVert.(pathLabel).CartCoord{ax(1)}(:);
Y = VolumesVert.(pathLabel).CartCoord{ax(2)}(:);
Z = VolumesVert.(pathLabel).CartCoord{ax(3)}(:);

% CoordGrid contains x,y,z values of points on the surface, such that x and
% y and regularly gridded
x_min = min(X);     x_max = max(X);
y_min = min(Y);     y_max = max(Y);
z_min = min(Z);     z_max = max(Z);



% create coordinates according to user input
x = opt.Vert.xi;
y = opt.Vert.yi;
z = opt.Vert.zi;

x = x(find(x >= x_min & x <= x_max)); % xp Test, i.e. used for tor the interpolation
y = y(find(y >= y_min & y <= y_max));
z = z(find(z >= z_min & z <= z_max));

coordRed                    = {x,y,z};
VolumesVert.(pathLabel).zi  = coordRed{find(VolumesVert.(pathLabel).normalDir)};

if isfield(VolumesVert,'zi_all')
    VolumesVert.zi_all = unique([VolumesVert.zi_all VolumesVert.(pathLabel).zi]);
else
    VolumesVert.zi_all = unique([VolumesVert.(pathLabel).zi]);
end

% create slice
[VolumesVert.(pathLabel).Polygons,VolumesVert.(pathLabel).pos] = makePolygonSlices(...
                VolumesVert.(pathLabel).CartCoord,ax,...
                VolumesVert.(pathLabel).TRI,...
                VolumesVert.(pathLabel).normalDir, coordRed, opt);

% get PathStyle from original PathCoord
LayersCell    = findPathsInLayers( PathCoord, pathLabel );
pathStyle     = PathCoord.(LayersCell{1}).(pathLabel).PathStyle;


% get orientation of vertical plane
switch opt.Vert.orient
    case 'NS'
        ax = [2 3 1];
    case 'EW'
        ax = [1 3 2];
    case 'HZ'
        ax = [1 2 3];
    otherwise
        error('unknown orientation')
end

% construct new PathCoord for perpendicular slices
ix  = ax(1);
iy  = ax(2);
pos = VolumesVert.(pathLabel).pos;

for k = 1:length(pos)
   
    % Scaling lat/lon/depth to paper coordinates
    xPaper = linCoordTrans(opt.xref,opt.xrefPaper,VolumesVert.(pathLabel).Polygons{k}(:,ix)');
    yPaper = linCoordTrans(opt.yref,opt.yrefPaper,VolumesVert.(pathLabel).Polygons{k}(:,iy)');


    % identifier for negative coordinates
    if VolumesVert.(pathLabel).zi(pos(k)) >= 0
        layersign = 'p';
    else
        layersign = 'm';
    end

    % this is for the case that the body is divided into multiple slices at the same z-coordinate
    % construct pathLabel
    if k > 1
        if pos(k) == pos(k-1)
            pathLabelorg = [pathLabel '_' num2str(new)];
            new = new+1;
        else
            new=1;
            pathLabelorg = pathLabel;
        end
    else
        new=1;
        pathLabelorg = pathLabel;
    end


    PathCoordVert.([layersign sprintf('%.3d',abs(VolumesVert.(pathLabel).zi(pos(k))))]).(pathLabelorg).InputCoord = [xPaper; yPaper];
    PathCoordVert.([layersign sprintf('%.3d',abs(VolumesVert.(pathLabel).zi(pos(k))))]).(pathLabelorg).PathStyle = pathStyle;
    PathCoordVert.([layersign sprintf('%.3d',abs(VolumesVert.(pathLabel).zi(pos(k))))]).(pathLabelorg).Commands.Index  = [];
    PathCoordVert.([layersign sprintf('%.3d',abs(VolumesVert.(pathLabel).zi(pos(k))))]).(pathLabelorg).Commands.String = 'MZ';
    PathCoordVert.([layersign sprintf('%.3d',abs(VolumesVert.(pathLabel).zi(pos(k))))]).(pathLabelorg).Commands.SectionLength = [length(yPaper) 0]; 

end



end
