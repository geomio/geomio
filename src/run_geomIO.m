function [PathCoord, Volumes,opt] = run_geomIO(opt, varargin)
% run_geomIO is the main function of geomIO. Different jobs can be
% executed, depending on the users need.
%
% The job '2D' is best suited for 2D drawings. It performs the following tasks:
% - Read SVG file
% - Create PathCoord
%
%  The job 'default' is suited for most cases for 3D input. It performs the following tasks:
% - Read LaMEM (if opt.readLaMEM==true, false by default)
% - Read SVG
% - Interpolate new slices in between the drawn slices, at position opt.zi 
%   (i.e. increases resolution in the direction orthogonal to the drawing direction). 
%   By default opt.zi==opt.z
% - Write a new SVG file containing the interpolate slices (If opt.writeSVG==true, false by default)
% - Create Volumes for the paths specified in opt.pathNames (opt.pathNames read from the SVG file by default)
%   It is recommended that the user specifies opt.pathNames since the phase
%   assignment order will follow the order of opt.pathNames (by default it may be mixed)
% - Create Polygons (slices of the Volumes) (if opt.getPolygons==true,
%   false by default);
% - Save Volume in Paraview format (if opt.writeParaview==true, false by default)
% - Create PathCoord for an other orientation by slicing the Volumes (if opt.Vert.write==true, false by default)
% - Save SVG file for other orientation (if opt.Vert.write==true, false by default)
% - Save Volumes in matlab format (if opt.saveVolumes==true, true by default)
% - Save Polygons in PETSC binary format (it opt.writePolyons==true, false by default)
%
% [PathCoord, Volumes] = run_geomIO(opt), executes run_geomIO with the 'default' job
% [PathCoord, Volumes] = run_geomIO(opt,jobName), where jobName is a string
% continaing the name of a specific job. By default the 'default' job is
% executed. 
% opt is an instance of the geomIO_Options class. To create opt: 
% opt = geomIO_Options();
%
% jobs include: 'default', '2D', 'Initialize SVG file', 'Read Profiles', 'MakePolygonsFromVolumes'
% [PathCoord, Volumes] = run_geomIO(opt,'MakePolygonsFromVolumes',Volumes)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   run_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
%% Main function calls
Volumes = struct();
if nargin == 1
    job = 'default';
elseif nargin == 2
    job = varargin{1};
elseif nargin == 3
    job = varargin{1};
    Volumes = varargin{2};
    opt.useSVG = false;
    PathCoord = [];
end

switch job
    case '2D'
        opt.Mode = '2D';
end

opt.runJob = job;
%fprintf('run_geomIO is running the job: %s',job);

% check options for feasibility & initialize
opt     = checkOpts(opt);


opt.normalDir = [0 0 1];

switch job
    case 'Initialize SVG file'
        initSVG(opt.inputFileName,opt)
        
    case 'Test'
        [PathCoord,docNode, opt] = readSVG(opt);
        
    case 'Read profiles'
        [~,~, opt] = readSVG(opt);
        [Profiles] = readProfileFromSVG(opt);
        save([opt.outputDir 'Profiles.mat'],'Profiles');
        fprintf(['\nSaved file: ' opt.outputDir 'Profiles.mat\n']);

    case 'gravity3D'
        
        % Read SVG
        %=====================================
        fprintf('=== Read SVG file : %s\n',opt.inputFileName)
        [PathCoord,docNode, opt] = readSVG(opt);
        
        
        % Interpolate new slices
        %=====================================
        if opt.interp
            fprintf(' => Interpolate new slices\n')
            [PathCoord] = interpPaths2InterLayers(PathCoord,opt.pathNames,opt);
        end
        
    case 'default'
        % Read LaMEM input
        %=====================================
        fprintf('=== Read LaMEM input file: %s\n',opt.LaMEMinputFileName)
        if opt.readLaMEM
            readLaMEMinput(opt);
        end
        
        % Read SVG
        %=====================================
        fprintf('=== Read SVG file : %s\n',opt.inputFileName)
        [PathCoord,docNode, opt] = readSVG(opt);
        
        
        % Interpolate new slices
        %=====================================
        if opt.interp
            fprintf(' => Interpolate new slices\n')
            [PathCoord] = interpPaths2InterLayers(PathCoord,opt.pathNames,opt);
        end
        
        % Write new SVG file including the interpolated slices
        %=====================================
        if opt.writeSVG & opt.interp
            fprintf(' => Write SVG file \n')
            writeSVG(docNode,PathCoord,opt,opt.orient,opt.zi,opt.outputFileName,0,0);
        end
        
        % Loop through paths
        %=====================================
        fprintf(' => Create 3D geometries from paths\n');
        for iP = 1:length(opt.pathNames)
            pathLabel = opt.pathNames{iP};
            fprintf(' > Path: %s - phase %d\n',pathLabel,opt.phase(iP))
            
            % Create Volume
            %=====================================
            Volumes = makeVolume( PathCoord, Volumes, pathLabel, opt);
            
            % Create vertical slices
            %=====================================
            if opt.Vert.write
                if iP == 1
                    PathCoordVert   = struct;
                    VolumesVert     = struct;
                end
                [PathCoordVert, VolumesVert, opt] = createPathCoordVert(PathCoordVert, VolumesVert, PathCoord, pathLabel,opt);
            end
            
            % Create polygon slices
            %=====================================
            if opt.getPolygons
                [Volumes.(pathLabel).Polygons, Volumes.(pathLabel).PolyPos] = makePolygonSlices(...
                    Volumes.(pathLabel).CartCoord,Volumes.(pathLabel).ax,...
                    Volumes.(pathLabel).TRI,...
                    opt.normalDir, {opt.setup.marker_x , opt.setup.marker_y, opt.setup.marker_z}, opt);

                % Check for overlapping polygons on the same slice
                checkOverlap(Volumes.(pathLabel),pathLabel,opt);
            end

            % Save paraview
            %=====================================
            if opt.writeParaview
                writeParaview(Volumes,pathLabel,opt)
            end
            fprintf('\n');
        end
        
        % write vertical slices to SVG
        %=====================================
        if opt.Vert.write
            writeSVGVert(PathCoordVert,VolumesVert,opt);
        end
        
        % Transform DrawCoord and InputCoord to the Reference coordinate system
        % This was not done earlier because writeSVG relies on the paper
        % coordinates
        if opt.transformCoord
            Layers = fieldnames(PathCoord);
            noLayers = length(Layers);
            for iL = 1:noLayers
                Paths = fieldnames(PathCoord.(Layers{iL}));
                noPaths = length(Paths);
                for iP = 1:noPaths
                        Coord = PathCoord.(Layers{iL}).(Paths{iP}).DrawCoord;
                        Coord(1,:) = linCoordTrans(opt.xrefPaper,opt.xref,Coord(1,:));
                        Coord(2,:)   = linCoordTrans(opt.yrefPaper,opt.yref,Coord(2,:));
                        PathCoord.(Layers{iL}).(Paths{iP}).DrawCoord = Coord;
                        
                        Coord = PathCoord.(Layers{iL}).(Paths{iP}).InputCoord;
                        Coord(1,:) = linCoordTrans(opt.xrefPaper,opt.xref,Coord(1,:));
                        Coord(2,:)   = linCoordTrans(opt.yrefPaper,opt.yref,Coord(2,:));
                        PathCoord.(Layers{iL}).(Paths{iP}).InputCoord = Coord;
                end
            end
        end
                
        % Save Volume
        %=====================================
        if opt.writeVolumes
            fprintf([' => Write volumes file: ' opt.outputDir 'Volumes.mat\n'])
            save([opt.outputDir 'Volumes.mat'],'Volumes')
        end
        
        % Save LaMEM
        %=====================================
        if opt.writePolygons && opt.getPolygons
            writePolygons( Volumes, opt)
        end
        
        % make variations
        %=====================================
        if opt.varyGeom.do
            if opt.varyGeom.loadPolygons{1}
                load(opt.varyGeom.loadPolygons{2});
                label                       = opt.varyGeom.loadPolygons{3};
                Volumes.(label).Polygons    = Vol.Polygons;
                Volumes.(label).PolyPos     = Vol.PolyPos;
                clear Vol;
            end
            varyGeom(Volumes, opt)
        end
        
        % safe subduction outlines
        %=====================================
        if opt.varySub.drawOutlines
            SubOutlines.Plate.EW = getOutlineSub(Volumes.(opt.varySub.vols),'EW',opt.varySub.outlineProf(1),opt.varySub.vols);
            SubOutlines.Plate.NS = getOutlineSub(Volumes.(opt.varySub.vols),'NS',opt.varySub.outlineProf(2),opt.varySub.vols);
            if opt.varySub.addWZ
                SubOutlines.WZ.EW = getOutlineSub(Volumes.WeakZone,'EW',opt.varySub.outlineProf(1),'WeakZone');
                SubOutlines.WZ.NS = getOutlineSub(Volumes.WeakZone,'NS',opt.varySub.outlineProf(2),'WeakZone');
            end
            if opt.varySub.addCrust
                SubOutlines.OC.EW = getOutlineSub(Volumes.OceanicCrust,'EW',opt.varySub.outlineProf(1),'OceanicCrust');
                SubOutlines.OC.NS = getOutlineSub(Volumes.OceanicCrust,'NS',opt.varySub.outlineProf(2),'OceanicCrust');
            end
            outname = [opt.outputDir,'Sub_Outline.mat'];
            save(outname, 'SubOutlines')
        end
        
        
    case '2D'
        
        % Read SVG
        %=====================================
        fprintf('=== Read SVG file : %s\n',opt.inputFileName)
        [PathCoord,docNode, opt] = readSVG(opt);
        
        
        if opt.transformCoord
            Layers = fieldnames(PathCoord);
            noLayers = length(Layers);
            for iL = 1:noLayers
                Paths = fieldnames(PathCoord.(Layers{iL}));
                noPaths = length(Paths);
                for iP = 1:noPaths
                        Coord = PathCoord.(Layers{iL}).(Paths{iP}).DrawCoord;
                        Coord(1,:) = linCoordTrans(opt.xrefPaper,opt.xref,Coord(1,:));
                        Coord(2,:)   = linCoordTrans(opt.yrefPaper,opt.yref,Coord(2,:));
                        PathCoord.(Layers{iL}).(Paths{iP}).DrawCoord = Coord;
                        
                        Coord = PathCoord.(Layers{iL}).(Paths{iP}).InputCoord;
                        Coord(1,:) = linCoordTrans(opt.xrefPaper,opt.xref,Coord(1,:));
                        Coord(2,:)   = linCoordTrans(opt.yrefPaper,opt.yref,Coord(2,:));
                        PathCoord.(Layers{iL}).(Paths{iP}).InputCoord = Coord;
                end
            end
        end
        
    
    case 'MakePolygonsFromVolumes'
        
        % Read LaMEM input
        %=====================================
        fprintf('=== Read LaMEM input file: %s\n',opt.LaMEMinputFileName)
        if opt.readLaMEM
            setup = readLaMEMinput(opt);
        end
        
               
        % Loop through paths
        %=====================================
        fprintf(' => Slice volumes\n');
        for iP = 1:length(opt.pathNames)
            pathLabel = opt.pathNames{iP};
            fprintf(' > Path: %s - phase %d\n',pathLabel,opt.phase(iP))
            
            
            % Create polygon slices
            %=====================================
            if opt.getPolygons
                [Volumes.(pathLabel).Polygons, Volumes.(pathLabel).PolyPos] = makePolygonSlices(...
                    Volumes.(pathLabel).CartCoord,Volumes.(pathLabel).ax,...
                    Volumes.(pathLabel).TRI,...
                    opt.normalDir, {setup.marker_x , setup.marker_y, setup.marker_z}, opt);
            end
            
            % Save paraview
            %=====================================
            if opt.writeParaview
                writeParaview(Volumes,pathLabel,opt)
            end
            fprintf('\n');
        end
        
               
        % Save Volume
        %=====================================
        if opt.writeVolumes
            fprintf([' => Write volumes file: ' opt.outputDir 'Volumes.mat\n'])
            save([opt.outputDir 'Volumes.mat'],'Volumes')
        end
        
        % Save LaMEM
        %=====================================
        if opt.writePolygons
            writePolygons( Volumes, opt)
        end
        
    otherwise
        error('unknown job');
        
end


end