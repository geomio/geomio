function plotVolumes(Volumes,varargin)
% plotVolumes(Volumes)
% plotVolumes(Volumes,List) where List contains the list of Volumes to
% plot.
% The list of all volumes contain within the Volumes object can be obtained
% by calling fieldnames(Volumes);
% e.g. plotVolumes(Volumes,{'Vol1','Vol2'})
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   plotVolumes.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
if not(isempty(varargin))
    Paths = varargin{1};
else
    Paths = fieldnames(Volumes);
end

xmin =  1e10;
xmax = -1e10;
ymin =  1e10;
ymax = -1e10;
zmin =  1e10;
zmax = -1e10;

for iP = 1:length(Paths)
    pathLabel = Paths{iP};
    X            = Volumes.(pathLabel).CartCoord{1};
    Y            = Volumes.(pathLabel).CartCoord{2};
    Z            = Volumes.(pathLabel).CartCoord{3};

    
    
%     writeVTKcell(fileName, TRI,[X,Y,Z] , ones(size(TRI,1),1))
    
    
    
    
    TRI          = Volumes.(pathLabel).TRI;
    PathStyle    = Volumes.(pathLabel).PathStyle;
    if strcmp(PathStyle.fill,'none')
        if strcmp(PathStyle.stroke,'none')
            PathStyle.fill = [.9 .9 .9];
        else
            PathStyle.fill = PathStyle.stroke
        end
    end
    Color = PathStyle.fill;

%     h = patch(X(TRI'), Y(TRI'),Z(TRI'),Color,'linestyle','none','FaceAlpha',PathStyle.fill_opacity);
    h = patch(X(TRI'), Y(TRI'),Z(TRI'),Color,'linestyle','none','FaceAlpha',.5);
    set(h,'FaceLighting','Phong','EdgeLighting','Phong' )
    material metal
    % axis([x_min x_max y_min y_max z_min z_max])
    % view(60,10)
   xmin_temp = min(X); xmin = min([xmin,xmin_temp]);
   xmax_temp = max(X); xmax = max([xmax,xmax_temp]);
   ymin_temp = min(Y); ymin = min([ymin,ymin_temp]);
   ymax_temp = max(Y); ymax = max([ymax,ymax_temp]);
   zmin_temp = min(Z); zmin = min([zmin,zmin_temp]);
   zmax_temp = max(Z); zmax = max([zmax,zmax_temp]);
end



H = zmax-zmin;
W = xmax-xmin;
D = ymax-ymin;


light('Position',[xmin-.1*W,ymin-.1*D,zmin-.1*H])
light('Position',[xmin-.1*W,ymin-.1*D,zmax+.1*H])

light('Position',[xmin-.1*W,ymax+.1*D,zmin-.1*H])
light('Position',[xmin-.1*W,ymax+.1*D,zmax+.1*H])

light('Position',[xmax+.1*W,ymin-.1*D,zmin-.1*H])
light('Position',[xmax+.1*W,ymin-.1*D,zmax+.1*H])

light('Position',[xmax+.1*W,ymax+.1*D,zmin-.1*H])
light('Position',[xmax+.1*W,ymax+.1*D,zmax+.1*H])


% light('Position',[0 0 0])
% light
% camlight headlight
xlabel('x')
ylabel('y')
zlabel('z')
% axis equal
% camproj('perspective')

box on
end
