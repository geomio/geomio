function varyGeom(Volumes,opt)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville, Tobias S. Baumann &
%    Arne Spang
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   varyGeom.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
%        Arne Spang       [arspang@uni-mainz.de]
% =========================================================================

%% Hardcoded Input
paraviewSimplification  = 2;
makeVideo               = 0;
showOutlines            = 0;

%% check if orientation is horizontal
if (opt.normalDir(1) ~= 0 || opt.normalDir(2) ~= 0 || opt.normalDir(3) ~= 1)
    error('Varying geometry only works with horizontal slicing')
end

%% read input

% check some options and retrieve some input
[Volume, volLabel, mu, sigma, stretchType, Sz, SzType, dXY, dZ] = checkVariationOptions(Volumes, opt.varyGeom);

% load or generate Stretch
[StrInfo, n, ref, CP_tr, pr]     = loadGenerateStretch(mu, sigma, Sz, SzType, dXY, dZ, stretchType, opt.varyGeom);

%% fix some directories

% check if output directory exists
if ~exist(opt.outputDir, 'dir')
    mkdir(opt.outputDir)
end

% save output directory
outputDir       = opt.outputDir;

%% print input parameters
printInParam(pr,opt)

% preallocate Outline Structure
if opt.varyGeom.drawOutlines
    [Outline, EW_coord, NS_coord] = preallocateOutline(Volume, opt.varyGeom, n);
end

% preallocate cell array
if opt.varyGeom.saveVols
    saveVols = cell(1,n);
end

%% main loop
tic
for iVersion = 1 : n
    
    % get copy of original volumes
    newVolumes = Volumes;
    
    % overwrite output directory for this version
    dirName         = [opt.varyGeom.outName sprintf('%06d',iVersion+opt.varyGeom.outNameOffset) '/'];
    opt.outputDir   = [outputDir dirName];
    if exist(opt.outputDir, 'dir')
        delete([opt.outputDir '*']);
    else
        mkdir([outputDir dirName]);
    end

    % copy original
    oldVol      = Volumes.(volLabel);
    
    % get control polygons
    CtrlPoly    = StrInfo.CtrlPoly;

    % stretch in z-direction
    if isfield(StrInfo,'Sz')
        if opt.varyGeom.gravOnly
            oldVol = stretchVolumeZ2(oldVol,StrInfo.Sz(iVersion));
        else
            [oldVol, CtrlPoly] = stretchVolumeZ(oldVol,StrInfo.Sz(iVersion), SzType, opt.setup.marker_z, StrInfo.CtrlPoly, CP_tr, ref);
        end
    end
    
    % number of polygons in volume
    numPoly     = length(oldVol.Polygons);

    % shift in z-direction (can only shift in marker level steps)
    if isfield(StrInfo,'dZ')
        if opt.varyGeom.gravOnly
            for iPol = 1 : numPoly
                oldVol.Polygons{iPol}(:,3) = oldVol.Polygons{iPol}(:,3) + StrInfo.dZ(iVersion);
            end
        else
            % how many marker levels does shiftZ equal
            markerDist              = opt.setup.marker_z(2) - opt.setup.marker_z(1);
            numLevels               = round(StrInfo.dZ(iVersion) / markerDist);
            shiftDist               = numLevels * markerDist;
            % shift polygons to new marker level
            oldVol.PolyPos          = oldVol.PolyPos + numLevels;
            % update dZ
            StrInfo.dZ(iVersion)    = shiftDist;
            % update Z-coordinates of polygons according to their new marker level
            for iPol = 1 : numPoly
                oldVol.Polygons{iPol}(:,3) = oldVol.Polygons{iPol}(:,3) + shiftDist;
            end
        end
    end    

    % interpolate stretching parameter
    [S_All] = interpStretch(length(opt.setup.marker_z),oldVol,StrInfo.Stretch(:,:,iVersion),CtrlPoly,ref);

    % preallocate new Polygons
    NewPolygons = cell(1,numPoly);
    
    if opt.varyGeom.writeParaview
        % check if all Polygons have the same number of nodes and track maximum
        maxNodes   = size(oldVol.Polygons{1},1);
        interpFlag = false;
        for iPoly = 2 : numPoly
            if size(oldVol.Polygons{iPoly},1) < maxNodes
                interpFlag = true;
            elseif size(oldVol.Polygons{iPoly},1) > maxNodes
                interpFlag = true;
                maxNodes   = size(oldVol.Polygons{iPoly},1);
            end
        end
        ind             = (1:paraviewSimplification:maxNodes-1);
        XCart           = zeros(length(ind),numPoly);
        YCart           = zeros(size(XCart));
        ZCart           = zeros(size(XCart));
        if interpFlag; fprintf('Warning: Need to interpolate polygons for paraview output. This can lead to small mistakes in the .vtk files. \n'); end
    end

    % stretch polygons and copy nodes for paraview output
    for iPol = 1 : numPoly
        switch stretchType
            case 'factor'
                NewPolygons{iPol}   = stretchPolygon(oldVol.Polygons{iPol}, S_All(iPol,1), S_All(iPol,2), StrInfo.theta(iVersion));
            case 'absolute'
                NewPolygons{iPol}   = stretchPolygonAbs(oldVol.Polygons{iPol}, S_All(iPol,1), S_All(iPol,2), StrInfo.theta(iVersion));
        end
        
        % shift in x and y
        if isfield(StrInfo,'dX') && isfield(StrInfo,'dY')
            NewPolygons{iPol}       = shiftPolyXY(NewPolygons{iPol},StrInfo.dX(iVersion),StrInfo.dY(iVersion));
        end

        % reduce amount of nodes for paraview output
        if opt.varyGeom.writeParaview
            if min(diff(oldVol.PolyPos)) == 0
                fprintf('Warning: Multiple polygons on the same slice. This will probably lead to big mistakes in the .vtk files! \n')
            end
            if interpFlag
                ParaPoly = interparc(maxNodes,NewPolygons{iPol}(:,1),NewPolygons{iPol}(:,2),NewPolygons{iPol}(:,3),'linear');
            else
                ParaPoly = NewPolygons{iPol};                
            end
            XCart(:,iPol)   = ParaPoly(ind,1);
            YCart(:,iPol)   = ParaPoly(ind,2);
            ZCart(:,iPol)   = ParaPoly(ind,3);
        end
    end

    % build new volume
    newVol.ax               = oldVol.ax;
    newVol.type             = oldVol.type;
    newVol.phase            = oldVol.phase;
    newVol.PathStyle        = oldVol.PathStyle;
    newVol.Polygons         = NewPolygons;
    newVol.PolyPos          = oldVol.PolyPos;
    newVol.CtrlPolygons     = CtrlPoly;
    newVol.Stretch          = StrInfo.Stretch(:,:,iVersion);
    if isfield(StrInfo,'Sz')
        newVol.Sz           = StrInfo.Sz(iVersion);
    end
    if opt.varyGeom.writeParaview
        newVol.CartCoord    = {XCart, YCart, ZCart};
        newVol.TRI          = triangulateVolumes(newVol.CartCoord{1},newVol.CartCoord{2},opt);        
    end

    % replace old Volume
    newVolumes.(volLabel)   = newVol;

    % save stretchParameters
    filename                = [opt.outputDir 'StretchInfo.csv'];
    fid                     = fopen(filename, 'wt');
    if isfield(StrInfo,'Sz')
        fprintf(fid, '%s,%.4f\n','Sz',StrInfo.Sz(iVersion));
    end
    if isfield(StrInfo,'dX')
        fprintf(fid, '%s,%.4f\n','dX',StrInfo.dX(iVersion));
        fprintf(fid, '%s,%.4f\n','dY',StrInfo.dY(iVersion));
    end
    if isfield(StrInfo,'dZ')
        fprintf(fid, '%s,%.4f\n','dZ',StrInfo.dZ(iVersion));
    end
    fprintf(fid, '%s,%s,%s\n','Polygon','Sx','Sy');
    fclose(fid);
    Info                    = [CtrlPoly, squeeze(StrInfo.Stretch(:,:,iVersion))];
    dlmwrite(filename, Info, 'delimiter', ',', '-append');

    % write vtk file for paraview
    if opt.varyGeom.writeParaview
        writeParaview(newVolumes, volLabel, opt)
    end

    % find outlines
    if opt.varyGeom.drawOutlines
        Outline{1,iVersion} = getOutlineNew(newVol.Polygons,'EW',EW_coord);
        Outline{2,iVersion} = getOutlineNew(newVol.Polygons,'NS',NS_coord);
    end
    
    % plot outlines
    if showOutlines && opt.varyGeom.drawOutlines
        Frames = plotOutlines(Outline, iVersion, makeVideo, opt.setup);
    end

    % save Volumes
    if opt.varyGeom.saveVols
        if opt.varyGeom.saveVols == 1
            saveVol.Polygons    = newVol.Polygons;
            saveVol.PolyPos     = newVol.PolyPos;
        elseif opt.varyGeom.saveVols == 2
            saveVol             = newVol;
        end
        saveVols{iVersion}      = saveVol;
        clear saveVol;
    end
    
    % write LaMEM input file
    if opt.varyGeom.writePolygons
        writePolygons(newVolumes, opt)
    end
    
    if mod(iVersion,n/10) == 0
        fprintf('Creating %d geometries took %.2f seconds\n',iVersion,toc)
    end

end

% reset output directory
opt.outputDir = outputDir;

% save Outlines
if opt.varyGeom.drawOutlines
    save([opt.outputDir 'Outlines.mat'],'Outline');
end

% save StrInfo
save([opt.outputDir 'StretchInfo.mat'],'StrInfo');

if makeVideo
    % create the video writer with 1 fps
    writerObj = VideoWriter('Variation_1_2.avi');
    writerObj.FrameRate = 3;
    % set the seconds per image
    % open the video writer
    open(writerObj);
    % write the frames to the video
    for i=1:length(Frames)
        % convert the image to a frame
        frame = Frames(i) ;
        writeVideo(writerObj, frame);
    end
    % close the writer object
    close(writerObj);
end

% save Volumes
if opt.varyGeom.saveVols
    save([opt.outputDir 'saveVols.mat'],'saveVols','-v7.3');
end
fprintf('Done!\n');
end


%% supporting functions

% retrieves and checks some input
function [Volume, volLabel, mu, sigma, stretchType, Sz, SzType, dXY, dZ] = checkVariationOptions(Volumes, opt)   
    % check volume name
    if ischar(opt.volume)
        volLabel = opt.volume;
        if isfield(Volumes,volLabel)
            Volume = Volumes.(volLabel);
        else
            error([volLabel ' is not a Volume'])
        end
    else
        error('opt.varyGeom.volume needs to be a string/char array')
    end
    
    % stretch type
    switch opt.stretchType
        case 'factor'
            stretchType = 'factor';
        case 'absolute'
            stretchType = 'absolute';
        otherwise
            error('opt.varyGeom.stretchType must either be factor or absolute')
    end
    
    % stretch or transformation params
    mu          = opt.mu;
    sigma       = opt.sigma;
    
    % check for stretching in z-direction
    Sz          = opt.Sz;
    SzType      = opt.SzType;
    
    % check for horizontal shift
    dXY         = opt.shift;
    
    % check for vertical Shift
    dZ          = opt.shiftZ;
end

% loads or generates the stretch parametes
function [StrInfo, n, ref, CP_tr, pr] = loadGenerateStretch(mu, sigma, Sz, SzType, dXY, dZ, stretchType, opt)
    ref   = opt.CP_ref;
    CP_tr = opt.CP_track;
    if opt.loadStretch
        genType                 = 'load';
        dim                     = opt.dim;
        loadStruc               = load(opt.loadStretch);
        n                       = loadStruc.n;
        StrInfo.Stretch         = loadStruc.StrInfo.Stretch;
        
        if isfield(loadStruc.StrInfo,'CtrlPoly')
            StrInfo.CtrlPoly    = loadStruc.StrInfo.CtrlPoly;
        else
            StrInfo.CtrlPoly    = opt.CtrlPoly';
        end
        
        if length(StrInfo.CtrlPoly) ~= size(StrInfo.Stretch,1)
            error('Number of control polygons does not match number of stretch parameters')
        end
        
        if isfield(loadStruc.StrInfo,'Sz')
            StrInfo.Sz          = loadStruc.StrInfo.Sz;
            if strcmp(ref,'volume')
                warning('You gave control polygons in reference to volume but stretching in Sz can shift the volume');
            end
        end
        
        if isfield(loadStruc.StrInfo,'dX') && isfield(loadStruc.StrInfo,'dY')
            StrInfo.dX          = loadStruc.StrInfo.dX;
            StrInfo.dY          = loadStruc.StrInfo.dY;
        end
        
        if isfield(loadStruc.StrInfo,'dZ')
            StrInfo.dZ          = loadStruc.StrInfo.dZ;
            if strcmp(ref,'volume')
                warning('You gave control polygons in reference to volume but the volume will be shifted by dZ');
            end
        end
        
        if isfield(loadStruc.StrInfo,'theta')
            StrInfo.theta       = loadStruc.StrInfo.theta;
        else
            StrInfo.theta       = opt.theta * ones(size(StrInfo.Stretch,3),1);
        end
        
        % printing info
        mu                      = [NaN NaN];
        sigma                   = [NaN NaN];
        pr.sigma2               = [NaN NaN];
        pr.Sz                   = [NaN NaN];
        pr.SzType               = SzType;
        pr.dZ                   = [NaN NaN];
        pr.mu_shift             = [NaN NaN];
        pr.sigma_shift          = [NaN NaN];
    
    elseif strcmp(opt.genType,'grid')
        StrInfo                 = generateStretchGrid(opt);
        StrInfo.theta           = opt.theta * ones(size(StrInfo.Stretch,3),1);
        n                       = size(StrInfo.Stretch,3);
        genType                 = opt.genType;
        dim                     = opt.dim;
                
        % printing info
        mu                      = [NaN NaN];
        sigma                   = [NaN NaN];
        pr.sigma2               = [NaN NaN];
        pr.Sz                   = [NaN NaN];
        pr.SzType               = SzType;
        pr.dZ                   = [NaN NaN];
        pr.mu_shift             = [NaN NaN];
        pr.sigma_shift          = [NaN NaN];
    else
        n                       = opt.n;
        genType                 = opt.genType;
        dim                     = opt.dim;
        switch genType
            case 'acceptance'
                acceptRule      = opt.acceptRule;
                secondChance    = opt.secondChance;
            case 'hist'
                numCandidates   = opt.histNumCand;
            case 'guidedChain'
                sigma2          = opt.sigma2;
        end

        % generate stretch parameters for all variations
        switch genType
            case 'guidedChain'
                StrInfo  = generateStretchGuidedChain(n, mu, sigma, opt.CtrlPoly, dim, sigma2);
            case 'acceptance'
                StrInfo  = generateStretchAcceptance(n, mu, sigma, opt.CtrlPoly, dim, acceptRule, secondChance);
            case 'hist'
                StrInfo  = generateStretchHistogram(n, mu, sigma, opt.CtrlPoly, dim, numCandidates);
            case 'chain'
                StrInfo  = generateStretchChain(n, mu, sigma, opt.CtrlPoly, dim);
            case 'normal'
                StrInfo  = generateStretchNormal(n, mu, sigma, opt.CtrlPoly, dim);
        end

        % enforce minimum stretch
        if strcmp(stretchType,'factor')
            StrInfo.Stretch(StrInfo.Stretch < opt.Smin) = opt.Smin;
        end

        % generate Sz
        if length(Sz) == 2
            StrInfo.Sz   = randn(n,1) * Sz(2) + Sz(1);
            pr.Sz        = Sz;
            pr.SzType    = SzType;
            if strcmp(ref,'volume')
                warning('You gave control polygons in reference to volume but stretching in Sz can shift the volume');
            end
        else
            pr.Sz        = [NaN NaN];
            pr.SzType    = 'none';
        end
        
        % generate shift
        if length(dXY) == 4
            StrInfo.dX   = randn(n,1) * dXY(3) + dXY(1);
            StrInfo.dY   = randn(n,1) * dXY(4) + dXY(2);
            pr.mu_shift  = dXY(1:2);
            pr.sigma_shift = dXY(3:4);
        else
            pr.mu_shift  = [NaN NaN];
            pr.sigma_shift = [NaN NaN];
        end

        % generate dZ
        if length(dZ) == 2
            StrInfo.dZ   = randn(n,1) * dZ(2) + dZ(1);
            pr.dZ        = dZ;
            if strcmp(ref,'volume')
                warning('You gave control polygons in reference to volume but the volume will be shifted by dZ');
            end
        else
            pr.dZ        = [NaN NaN];
        end
        
        % coordiante rotation
        StrInfo.theta    = opt.theta * ones(size(StrInfo.Stretch,3),1);;
    end
    
    % more printing stuff
    pr.stretchType  = stretchType;
    pr.genType      = genType;
    pr.n            = n;
    pr.ref          = ref;
    if strcmp(dim,'3D')
        pr.mu           = mu;
        pr.sigma        = sigma;
    else
        pr.mu           = [mu, NaN];
        pr.sigma        = [sigma, NaN];
    end
    if ~strcmp(genType,'acceptance')
        pr.delta(1) = NaN;
        pr.delta(2) = NaN;
    else
        pr.delta(1) = acceptRule(1);
        pr.delta(2) = acceptRule(2);
    end

    if ~strcmp(genType,'guidedChain')
        pr.sigma2(1) = NaN;
        pr.sigma2(2) = NaN;
    else
        pr.sigma2    = sigma2;
    end
    
    pr.CtrlPoly = sprintf('%d,',StrInfo.CtrlPoly');
    pr.CtrlPoly = pr.CtrlPoly(1:end-1);
end

% prints the input parameters to a file
function printInParam(pr,opt)
    filename     = [opt.outputDir 'InputParameters.csv'];
    fid          = fopen(filename, 'wt');
    fprintf(fid, ['%s,%d\n'...
                  '%s,%s\n'...
                  '%s,%s\n'...
                  '%s,%s\n'...
                  '%s,%s\n'...
                  '%s,%.2f,%.2f\n'...
                  '%s,%s\n'...
                  '%s,%.2f,%.2f\n'...
                  ',%s,%s\n'...
                  '%s,%.2f,%.2f\n'...
                  '%s,%.2f,%.2f\n'...
                  '%s,%.2f,%.2f\n'...
                  '%s,%.2f,%.2f\n'...
                  '%s,%.2f,%.2f\n'...
                  '%s,%.2f,%.2f\n'],...
                  'n',pr.n,...
                  'stretch type',pr.stretchType,...
                  'CtrlPoly reference',pr.ref,...
                  'control polygons',pr.CtrlPoly,...
                  'generation type',pr.genType,...
                  'Sz (mu & sigma)',pr.Sz(1),pr.Sz(2),...
                  'SzType',pr.SzType,...
                  'dZ (mu & sigma)',pr.dZ(1),pr.dZ(2),...
                  'X','Y','mu',pr.mu(1),pr.mu(2),...
                  'sigma',pr.sigma(1),pr.sigma(2),...
                  'deltaS',pr.delta(1),pr.delta(2),...
                  'sigma2',pr.sigma2(1),pr.sigma2(2),...
                  'mu_shift',pr.mu_shift(1),pr.mu_shift(1),...
                  'sigma_shift',pr.sigma_shift(1),pr.sigma_shift(2));
    fclose(fid);
end

% preallocates space for Outlines
function [Outline, EW_coord, NS_coord] = preallocateOutline(Vol, opt, n)
    EW_coord                = opt.outlineProf(1);
    NS_coord                = opt.outlineProf(2);
    
    Outline                 = cell(2,n+1);

    % enter the initial outline as the last entry
    Outline{1,end}          = getOutlineNew(Vol.Polygons,'EW',EW_coord);
    Outline{2,end}          = getOutlineNew(Vol.Polygons,'NS',NS_coord);
end

% stretches by factor
function newPolygon = stretchPolygon(Polygon,Sx,Sy,theta)
    X = Polygon(:,1);
    Y = Polygon(:,2);
    
    % find center of gravity
    [geom,~,~]  = polygeom(X,Y);
    center      = [geom(2) geom(3)];
    
    % transform coordinates polfgon nodes into relative ones
    dX          = X - center(1);
    dY          = Y - center(2);
    
    % rotate coordinate system
    if theta
        Q       = [cosd(theta) sind(theta); -sind(theta) cosd(theta)];
        d       = [dX, dY] * Q;
        dX      = d(:,1);
        dY      = d(:,2);
    end 
    
    % stretch relative coordinates
    dX          = dX * Sx;
    dY          = dY * Sy;
    
    % rotate coordinate system back
    if theta
        d       = [dX, dY] * Q';
        dX      = d(:,1);
        dY      = d(:,2);
    end  
    
    % transform back
    Xnew        = dX + center(1);
    Ynew        = dY + center(2);
    
    newPolygon  = [Xnew Ynew Polygon(:,3)];
end

% stretches by absolute value
% angles within polygon stay the same
function newPolygon = stretchPolygonAbs(Polygon,dx,dy,theta)
    
    % find center of gravity
    [geom,~,~]  = polygeom(Polygon(:,1),Polygon(:,2));
    center      = [geom(2) geom(3)];
    
    % find vectors from center to nodes
    vectors     = Polygon(:,1:2) - center;
    
    % rotate coordinate system
    if theta
        Q       = [cosd(theta) sind(theta); -sind(theta) cosd(theta)];
        vectors = vectors * Q;
    end
    
    % get length of those vectors
    veclen      = (vecnorm(vectors'))';
    % calculate factors for both dimensions
    factors     = vectors ./ veclen;
    
    % error if polygon shrinks to less than 10% size
    if dx < -max(vectors(:,1))*0.9
        error('dx is shrinking polygon too much')
    end
    
    if dy < -max(vectors(:,2))*0.9
        error('dy is shrinking polygon too much')
    end
    
    % warning if dx or dy are really big
    if abs(dx) > (max(vectors(:,1)) * 2 / 3)
        fprintf('Warning: dx is more than 2/3 of polygon size. Ratio: %.2f \n', dx / max(vectors(:,1)))
    end
    
    if abs(dy) > (max(vectors(:,2)) * 2 / 3)
        fprintf('Warning: dy is more than 2/3 of polygon size. Ratio: %.2f \n', dy / max(vectors(:,2)))
    end
    
    % calculate change for each coordinate
    chX     = dx * factors(:,1);
    chY     = dy * factors(:,2);
    
    % rotate coordinate system back
    if theta
        ch  = [chX, chY] * Q';
        chX = ch(:,1);
        chY = ch(:,2);
    end
    
    % apply change        
    newPolygon(:,1) = Polygon(:,1) + chX;
    newPolygon(:,2) = Polygon(:,2) + chY;
    newPolygon(:,3) = Polygon(:,3);
end

% stretch in z-direction and reinterpolate onto marker levels
function [NewVolume, CtrlPoly] = stretchVolumeZ(Volume, Sz, type, z_vec, CtrlPoly, CP_tr, CP_ref)
    
    if Sz > 1-1e-12 && Sz < 1+1e-12
        NewVolume = Volume;
        return
    end
    
    % copy Volume
    NewVolume = Volume;
    Polygons  = Volume.Polygons;
    
    % check Sz
    if Sz < 0.1
        fprintf('Warning: Reset Sz to 0.1 \n')
        Sz = 0.1;
    end

    % number of polygons in volume
    numPoly                 = length(Polygons);
    
    % find maximum number of nodes per polygon
    maxNodes = 0;
    for i = 1 : numPoly
        if size(Polygons{i},1) > maxNodes
            maxNodes = size(Polygons{i},1);
        end
    end
    
    % check type
    switch type
        case 'com'
            center          = findMassCenter(Volume);
            z_center        = center(3);
        case 'mid'
            minZ            = Volume.Polygons{1}(1,3);
            maxZ            = Volume.Polygons{end}(1,3);
            z_center        = (minZ + maxZ)/2;
        case 'top'
            z_center        = Volume.Polygons{end}(1,3);
        case 'bot'
            z_center        = Volume.Polygons{1}(1,3);
        otherwise
            error('Unknown Sz type')
    end
    
    X = zeros(numPoly,maxNodes);
    Y = zeros(numPoly,maxNodes);
    Z = zeros(numPoly,1);
    
    % loop over polygons
    for iPoly = 1 : numPoly
        % get polygon
        Poly        = Polygons{iPoly};
        % convert z-coordiantes to relative coords
        Poly(:,3)   = Poly(:,3) - z_center;
        % stretch z-coordinates
        Poly(:,3)   = Poly(:,3) * Sz;
        % convert back to absolute coordiantes
        Poly(:,3)   = Poly(:,3) + z_center;
        % interpolate it on the same number of nodes as all others
        Pol         = interparc(maxNodes,Poly(:,1),Poly(:,2),'linear');
        Poly        = [Pol Poly(1,3)*ones(maxNodes,1)];
        % save new coordinates
        X(iPoly,:)  = Poly(:,1)';
        Y(iPoly,:)  = Poly(:,2)';
        Z(iPoly)    = Poly(1,3);
    end
    
    % find new top and bottom boundary of volume
    minZ    = min(Z);
    maxZ    = max(Z);
    
    % which of the global marker levels is within this volume
    ind        = find(z_vec >= minZ & z_vec <= maxZ);
    Vol_z_vec  = z_vec(ind);
    newNumPoly = length(Vol_z_vec);
    
    Xnew = zeros(newNumPoly,maxNodes);
    Ynew = zeros(newNumPoly,maxNodes);
    Znew = zeros(newNumPoly,maxNodes);
    
    
    % loop over the nodes
    for iNode = 1 : maxNodes
        % interpolate new polygon coords back onto LaMEM marker levels
        Xnew(:,iNode) = interp1(Z(:),X(:,iNode),Vol_z_vec);
        Ynew(:,iNode) = interp1(Z(:),Y(:,iNode),Vol_z_vec);
        Znew(:,iNode) = Vol_z_vec;
    end
    
    % collect polygons
    Polygons = cell(newNumPoly,1);
    for iPoly = 1 : newNumPoly
        Polygons{iPoly} = [Xnew(iPoly,:)', Ynew(iPoly,:)', Znew(iPoly,:)'];
    end
    
    % insert
    NewVolume.Polygons = Polygons;
    NewVolume.PolyPos  = ind;
    
    % adjust control polygons if CP_track flag is set
    if CP_tr
        if strcmp(CP_ref,'volume')
            CtrlPoly = CtrlPoly + (Volume.PolyPos(1)-1);
        end
        % depth of control polygons
        d = z_vec(CtrlPoly);
        % transform relative to center
        d = d - z_center;
        % stretch
        d = d * Sz;
        % transform back
        d = d + z_center;
        % find which polygon is closest to the adjusted depth
        for iPoly = 1 : length(CtrlPoly)
            [~,ind] = min(abs(z_vec - d(iPoly)));
            CtrlPoly(iPoly) = ind;
        end
        if strcmp(CP_ref,'volume')
            CtrlPoly = CtrlPoly - (NewVolume.PolyPos(1)-1);
        end
    end
end

% shift polygon in x and y
function newPolygon = shiftPolyXY(Polygon,dX,dY)
    newPolygon = [Polygon(:,1)+dX Polygon(:,2)+dY Polygon(:,3)];
end

% Generate first stretch parameter for chain, guided chain and histogram method
function [Sx1, Sy1] = getS1(n, mu, sigma, dim)
    S1      = randn(n,1) * sigma(1) + mu(1);
    if strcmp(dim,'3D')
        Sx1 = S1;
        Sy1 = randn(n,1) * sigma(2) + mu(2);
    elseif strcmp(dim,'3D_homo')
        Sx1 = S1;
        Sy1 = S1;
    elseif strcmp(dim,'2D_X')
        Sx1 = S1;
        Sy1 = ones(n,1);
    elseif strcmp(dim,'2D_Y')
        Sx1 = ones(n,1);
        Sy1 = S1;
    end
end

% simplest version
% can have big jumps between control polygons
function [StrInfo] = generateStretchNormal(n, mu, sigma, CtrlPoly, dim)
    
    % preallocate
    StrInfo.CtrlPoly    = CtrlPoly';
    StrInfo.Stretch     = zeros(length(CtrlPoly),2,n);
    
    % create random numbers for all control polygons
    for iVersion = 1 : n
        if strcmp(dim,'3D')
            % Sx
            StrInfo.Stretch(:,1,iVersion) = randn(length(CtrlPoly),1) * sigma(1) + mu(1);
            % Sy
            StrInfo.Stretch(:,2,iVersion) = randn(length(CtrlPoly),1) * sigma(2) + mu(2);
        elseif strcmp(dim,'3D_homo')
            StrInfo.Stretch(:,1,iVersion) = randn(length(CtrlPoly),1) * sigma + mu;
            StrInfo.Stretch(:,2,iVersion) = StrInfo.Stretch(:,1,iVersion);
        elseif strcmp(dim,'2D_X')
            StrInfo.Stretch(:,1,iVersion) = randn(length(CtrlPoly),1) * sigma + mu;
            StrInfo.Stretch(:,2,iVersion) = 1;
        elseif strcmp(dim,'2D_Y')
            StrInfo.Stretch(:,1,iVersion) = 1;
            StrInfo.Stretch(:,2,iVersion) = randn(length(CtrlPoly),1) * sigma + mu;
        end
    end
end

% 1st version: S_n becomes mu_n+1
% (S tends to grow towards the top in this case)
function [StrInfo] = generateStretchChain(n, mu, sigma, CtrlPoly, dim)
    
    % preallocate
    StrInfo.CtrlPoly    = CtrlPoly';
    StrInfo.Stretch     = zeros(length(CtrlPoly),2,n);
    
    % create random numbers for first control polygon of each version
    [Sx1, Sy1]             = getS1(n, mu, sigma, dim);
    StrInfo.Stretch(1,1,:) = Sx1;
    StrInfo.Stretch(1,2,:) = Sy1;
    
    % create stretch parameters for other control polygons
    for iPoly = 2 : length(CtrlPoly)
        for iVersion = 1 : n
            if strcmp(dim,'3D')
                StrInfo.Stretch(iPoly,1,iVersion) = randn(1,1) * sigma(1) + StrInfo.Stretch(iPoly-1,1,iVersion);
                StrInfo.Stretch(iPoly,2,iVersion) = randn(1,1) * sigma(2) + StrInfo.Stretch(iPoly-1,2,iVersion);
            elseif strcmp(dim,'3D_homo')
                StrInfo.Stretch(iPoly,1,iVersion) = randn(1,1) * sigma(1) + StrInfo.Stretch(iPoly-1,1,iVersion);
                StrInfo.Stretch(iPoly,2,iVersion) = StrInfo.Stretch(iPoly,1,iVersion);
            elseif strcmp(dim,'2D_X')
                StrInfo.Stretch(iPoly,1,iVersion) = randn(1,1) * sigma(1) + StrInfo.Stretch(iPoly-1,1,iVersion);
                StrInfo.Stretch(iPoly,2,iVersion) = 1;
            elseif strcmp(dim,'2D_Y')
                StrInfo.Stretch(iPoly,1,iVersion) = 1;
                StrInfo.Stretch(iPoly,2,iVersion) = randn(1,1) * sigma(1) + StrInfo.Stretch(iPoly-1,2,iVersion);
            end
        end
    end  
end

% 2nd version: mu constant, but acceptance rule: S_(n-1)-sigma < S_n < S_(n-1)+sigma +
% 10% chance of getting accepted anyway
% (S does not grow towrads the top)
function [StrInfo] = generateStretchAcceptance(n, mu, sigma, CtrlPoly, dim, tol, sec)
    
    % preallocate
    StrInfo.CtrlPoly    = CtrlPoly';
    StrInfo.Stretch     = zeros(length(CtrlPoly),2,n);
    
    % create random numbers for first control polygon of each version
    [Sx1, Sy1]             = getS1(n, mu, sigma, dim);
    StrInfo.Stretch(1,1,:) = Sx1;
    StrInfo.Stretch(1,2,:) = Sy1;
    
    % create stretch parameters for other control polygons
    for iPoly = 2 : length(CtrlPoly)
        for iVersion = 1 : n
            if strcmp(dim,'3D')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamAcceptance(StrInfo.Stretch(iPoly-1,1,iVersion), mu(1), sigma(1), tol(1), sec(1));           
                % find Sy
                StrInfo.Stretch(iPoly,2,iVersion) = findParamAcceptance(StrInfo.Stretch(iPoly-1,2,iVersion), mu(2), sigma(2), tol(2), sec(2));
            elseif strcmp(dim,'3D_homo')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamAcceptance(StrInfo.Stretch(iPoly-1,1,iVersion), mu, sigma, tol, sec);
                % copy to Sy
                StrInfo.Stretch(iPoly,2,iVersion) = StrInfo.Stretch(iPoly,1,iVersion);
            elseif strcmp(dim,'2D_X')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamAcceptance(StrInfo.Stretch(iPoly-1,1,iVersion), mu, sigma, tol, sec);
                % set Sy to 1
                StrInfo.Stretch(iPoly,2,iVersion) = 1;
            elseif strcmp(dim,'2D_Y')
                % set Sx to 1
                StrInfo.Stretch(iPoly,1,iVersion) = 1;
                % find Sy
                StrInfo.Stretch(iPoly,2,iVersion) = findParamAcceptance(StrInfo.Stretch(iPoly-1,2,iVersion), mu, sigma, tol, sec);

            end
        end
    end  
end

function S = findParamAcceptance(prev, mu, sigma, tol, sec)
    S = 0;
    while ~S
        prop = randn(1,1) * sigma + mu;
        if abs(prop - prev) < tol
            S = prop;
        elseif abs(prop - prev) < 2*tol
            if rand < sec
                S = prop;
            end
        end
    end
end

% 3rd version: S_n generted by combined distribution of mu1,sigma &
% S_(n-1),sigma
% (growth of S towards the top is less likely)
function [StrInfo] = generateStretchHistogram(n, mu, sigma, CtrlPoly, dim, num)
    % preallocate
    StrInfo.CtrlPoly    = CtrlPoly';
    StrInfo.Stretch     = zeros(length(CtrlPoly),2,n);
    
    % create random numbers for first control polygon of each version
    [Sx1, Sy1]             = getS1(n, mu, sigma, dim);
    StrInfo.Stretch(1,1,:) = Sx1;
    StrInfo.Stretch(1,2,:) = Sy1;
    
    % create stretch parameters for other control polygons
    for iPoly = 2 : length(CtrlPoly)
        for iVersion = 1 : n
            if strcmp(dim,'3D')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamHistogram(StrInfo.Stretch(iPoly-1,1,iVersion), mu(1), sigma(1), num);
                % find Sy
                StrInfo.Stretch(iPoly,2,iVersion) = findParamHistogram(StrInfo.Stretch(iPoly-1,2,iVersion), mu(2), sigma(2), num);
            elseif strcmp(dim,'3D_homo')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamHistogram(StrInfo.Stretch(iPoly-1,1,iVersion), mu, sigma, num);
                % copy Sy
                StrInfo.Stretch(iPoly,2,iVersion) = StrInfo.Stretch(iPoly,1,iVersion);
            elseif strcmp(dim,'2D_X')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamHistogram(StrInfo.Stretch(iPoly-1,1,iVersion), mu, sigma, num);
                % set Sy
                StrInfo.Stretch(iPoly,2,iVersion) = 1;
            elseif strcmp(dim,'2D_Y')
                % set Sx
                StrInfo.Stretch(iPoly,1,iVersion) = 1;
                % find Sy
                StrInfo.Stretch(iPoly,2,iVersion) = findParamHistogram(StrInfo.Stretch(iPoly-1,2,iVersion), mu, sigma, num);
            end
        end
    end 
end

function S = findParamHistogram(prev, mu, sigma, num)
    P    = randn(num,1) * sigma + mu;
    Q    = randn(num,1) * sigma + prev;
    J    = [P;Q];
    pick = round(rand * 2 * num);
    S    = J(pick);
end

% 4th version: S_n generated by joint (multiplicative) probability of 
% mu1,sigma1 & S_(n-1),sigma2
% similar results to generateStretchAcceptance
function [StrInfo] = generateStretchGuidedChain(n, mu, sigma, CtrlPoly, dim, sigma2)

    % preallocate
    StrInfo.CtrlPoly    = CtrlPoly';
    StrInfo.Stretch     = zeros(length(CtrlPoly),2,n);
    
    % create random numbers for first control polygon of each version
    [Sx1, Sy1]             = getS1(n, mu, sigma, dim);
    StrInfo.Stretch(1,1,:) = Sx1;
    StrInfo.Stretch(1,2,:) = Sy1;
    
    % create stretch parameters for other control polygons
    for iPoly = 2 : length(CtrlPoly)
        for iVersion = 1 : n
            if strcmp(dim,'3D')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamGuidedChain(StrInfo.Stretch(iPoly-1,1,iVersion), mu(1), sigma(1), sigma2(1));
                % find Sy
                StrInfo.Stretch(iPoly,2,iVersion) = findParamGuidedChain(StrInfo.Stretch(iPoly-1,2,iVersion), mu(2), sigma(2), sigma2(2));
            elseif strcmp(dim,'3D_homo')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamGuidedChain(StrInfo.Stretch(iPoly-1,1,iVersion), mu, sigma, sigma2);
                % copy Sy
                StrInfo.Stretch(iPoly,2,iVersion) = StrInfo.Stretch(iPoly,1,iVersion);
            elseif strcmp(dim,'2D_X')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamGuidedChain(StrInfo.Stretch(iPoly-1,1,iVersion), mu, sigma, sigma2);
                % set Sy
                StrInfo.Stretch(iPoly,2,iVersion) = 1;
            elseif strcmp(dim,'2D_Y')
                % set Sx
                StrInfo.Stretch(iPoly,1,iVersion) = 1;
                % find Sy
                StrInfo.Stretch(iPoly,2,iVersion) = findParamGuidedChain(StrInfo.Stretch(iPoly-1,2,iVersion), mu, sigma, sigma2);
            end
        end
    end
end

function [StrInfo] = generateStretchMetropolis(n, mu, sigma, CtrlPoly, dim, sigma2)

    % preallocate
    StrInfo.CtrlPoly    = CtrlPoly';
    StrInfo.Stretch     = zeros(length(CtrlPoly),2,n);
    
    % create random numbers for first control polygon of each version
    [Sx1, Sy1]             = getS1(n, mu, sigma, dim);
    StrInfo.Stretch(1,1,:) = Sx1;
    StrInfo.Stretch(1,2,:) = Sy1;
    
    % create stretch parameters for other control polygons
    for iPoly = 2 : length(CtrlPoly)
        for iVersion = 1 : n
            if strcmp(dim,'3D')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamMetropolis(StrInfo.Stretch(iPoly-1,1,iVersion), mu(1), sigma(1), sigma2(1));
                % find Sy
                StrInfo.Stretch(iPoly,2,iVersion) = findParamMetropolis(StrInfo.Stretch(iPoly-1,2,iVersion), mu(2), sigma(2), sigma2(2));
            elseif strcmp(dim,'3D_homo')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamMetropolis(StrInfo.Stretch(iPoly-1,1,iVersion), mu, sigma, sigma2);
                % copy Sy
                StrInfo.Stretch(iPoly,2,iVersion) = StrInfo.Stretch(iPoly,1,iVersion);
            elseif strcmp(dim,'2D_X')
                % find Sx
                StrInfo.Stretch(iPoly,1,iVersion) = findParamMetropolis(StrInfo.Stretch(iPoly-1,1,iVersion), mu, sigma, sigma2);
                % set Sy
                StrInfo.Stretch(iPoly,2,iVersion) = 1;
            elseif strcmp(dim,'2D_Y')
                % set Sx
                StrInfo.Stretch(iPoly,1,iVersion) = 1;
                % find Sy
                StrInfo.Stretch(iPoly,2,iVersion) = findParamMetropolis(StrInfo.Stretch(iPoly-1,2,iVersion), mu, sigma, sigma2);
            end
        end
    end
end

function S = findParamGuidedChain(prev, mu, sigma, sigma2)
    iter    = 0;
    found   = false;
    while ~found
        % get new candidate
        x   = randn(1,1) * sigma + mu;
        % get likelihood
        p   = exp(-0.5 * ((x-prev)/sigma2)^2);
        % create testnumber
        u   = rand;
        % check
        if u < p
            % accept candidate
            S = x;
            found = true;
        else
            % reject candidate
            iter = iter + 1;
        end
        % accept anyway after 10000 tries
        if iter > 10000
            fprintf('WARNING: Unable to find suitable x-candidate for version: %d, CtrlPoly #: %d. Using %.3f.\n',iVersion, iPoly, x);
            S = x;
            found = true;
        end
    end
end

function S = findParamMetropolis(prev, mu, sigma, sigma2)
    iter    = 0;
    found   = false;
    while ~found
        % get new candidate
        x   = randn(1,1) * sigma2 + prev;
        % get likelihood
        p   = exp(-0.5 * ((x-mu)/sigma)^2);
        % create testnumber
        u   = rand;
        % check
        if u < p
            % accept candidate
            S = x;
            found = true;
        else
            % reject candidate
            iter = iter + 1;
        end
        % accept anyway after 10000 tries
        if iter > 10000
            fprintf('WARNING: Unable to find suitable x-candidate for version: %d, CtrlPoly #: %d. Using %.3f.\n',iVersion, iPoly, x);
            S = x;
            found = true;
        end
    end
end

% create stretch parameters on regular grid
function [StrInfo] = generateStretchGrid(opt)
    % read
    lb                      = opt.Grid.lb;
    ub                      = opt.Grid.ub;
    num                     = opt.Grid.num;
    dim                     = opt.dim;
    noise                   = opt.Grid.noise;
    
    % check Sz
    if isfield(opt.Grid,'Sz')
        num                 = [num; opt.Grid.Sz(3)];
    end
    
    % preallocate
    n                       = prod(num);
    StrInfo.CtrlPoly        = opt.CtrlPoly';
    StrInfo.Stretch         = zeros(length(StrInfo.CtrlPoly),2,n);

    % generate stretch
    if strcmp(dim,'3D')
        StrInfo.Stretch(:,1,:) = findParamsGrid(lb(:,1), ub(:,1), num(:,1), noise);
        StrInfo.Stretch(:,2,:) = findParamsGrid(lb(:,2), ub(:,2), num(:,2), noise);
    elseif strcmp(dim,'3D_homo')
        StrInfo.Stretch(:,1,:) = findParamsGrid(lb, ub, num, noise);
        StrInfo.Stretch(:,2,:) = StrInfo.Stretch(:,1,:);
    elseif strcmp(dim,'2D_X')
        StrInfo.Stretch(:,1,:) = findParamsGrid(lb, ub, num, noise);
        StrInfo.Stretch(:,2,:) = 1;
    elseif strcmp(dim,'2D_Y')
        StrInfo.Stretch(:,1,:) = 1;
        StrInfo.Stretch(:,2,:) = findParamsGrid(lb, ub, num, noise);
    end
      
    % handle Sz
    if isfield(opt.Grid,'Sz')
        StrInfo             = findSzGrid(StrInfo,opt);
    end
end

function Parameters = findParamsGrid(lb, ub, num, noise)
    n                       = prod(num);
    nPoly                   = length(lb);
    
    len                     = ones(nPoly,1);
    for i = 2 : nPoly
        len(i)              = prod(num(1:i-1));
    end
    
    Parameters              = zeros(nPoly,1,n);
    for i = 1 : nPoly
        % parameter grid
        temp                = linspace(lb(i),ub(i),num(i)+1);
        d                   = (temp(2) - temp(1))/2;
        params              = (temp(1:end-1) + d)';

        % make block
        lp                  = num(i);
        l                   = len(i);
        block = zeros(l*lp,1);
        for j = 1 : lp
            block((j-1)*l+1:j*l) = params(j);
        end
        % repeat block
        block               = repmat(block,n/(l*lp),1);
        % add noise
        if noise > 0
            addNoise        = noise * d .* randn(length(block),1);
            block           = block + addNoise;
        end
        Parameters(i,1,:) = block;
    end
end

function [StrInfo] = findSzGrid(StrInfo, opt)
    % read options
    lb               = opt.Grid.Sz(1);
    ub               = opt.Grid.Sz(2);
    num              = opt.Grid.Sz(3);
    nPrev            = size(StrInfo.Stretch,3)/num;
    noise            = opt.Grid.noise;
    
    % make Sz params
    temp             = linspace(lb, ub, num+1);
    d                = (temp(2) - temp(1))/2;
    params           = (temp(1:end-1) + d)';
    
    % make block
    block            = zeros(nPrev*num,1);
    for i = 1 : num
        block((i-1)*nPrev+1:i*nPrev) = params(i);
    end
    
    % add noise
    if noise > 0
        addNoise        = noise * d .* randn(length(block),1);
        block           = block + addNoise;
    end
    
    % assemble stretch info
    StrInfo.Sz       = block;
end

% interpolates stretch parameters between control polygons
function [S_All] = interpStretch(numLevel, Volume, Stretch, CtrlPoly, ref)
      
    % number of control polygons
    numCtrlPoly  = length(CtrlPoly);
    
    % split stretch into x- and y-component to make things easier to read
    Sx          = Stretch(:,1);
    Sy          = Stretch(:,2);

    % preallocate stretchfactors for all polygons
    SxAll       = zeros(numLevel,1);
    SyAll       = zeros(numLevel,1);
    
    % check whether CtrlPoly is in reference to the volume or the global
    % layers
    switch ref
        case 'volume'
            % convert to global levels
            CtrlPoly = CtrlPoly + Volume.PolyPos(1)-1;
        case 'global'
            % nothing to be done
        otherwise
            error('opt.varyGeom.CP_ref has to be either volume or global')
    end
    
    % check if all control polygons are within the setup
    for i = 1 : numCtrlPoly
        if CtrlPoly(i) > numLevel
            CtrlPoly = CtrlPoly(1:i-1);
            numCtrlPoly = i-1;
            break
        end
    end

    % enter first value
    SxAll(CtrlPoly(1))  = Sx(1);
    SyAll(CtrlPoly(1))  = Sy(1);
    
    % interpolate the rest
    for i = 2 : numCtrlPoly
        newSx                                   = linspace(Sx(i-1),Sx(i),CtrlPoly(i) - CtrlPoly(i-1) + 1);
        newSy                                   = linspace(Sy(i-1),Sy(i),CtrlPoly(i) - CtrlPoly(i-1) + 1);
        SxAll((CtrlPoly(i-1)+1):CtrlPoly(i))    = newSx(2:end);
        SyAll((CtrlPoly(i-1)+1):CtrlPoly(i))    = newSy(2:end);
    end
    
    % extend stretch to first and last polygon
    if CtrlPoly(1) ~= 1
        SxAll(1:CtrlPoly(1))        = Sx(1);
        SyAll(1:CtrlPoly(1))        = Sy(1);
    end
    if CtrlPoly(end) ~= numLevel
        SxAll(CtrlPoly(end):end)    = Sx(end);
        SyAll(CtrlPoly(end):end)    = Sy(end);
    end

    % join Sx and Sy
    S_All   = [SxAll SyAll];
    
    % extract the slices that overlap with the volume
    S_All   = S_All(Volume.PolyPos,:);
end

% plots the outlines
function Frames = plotOutlines(Outline, iVersion, makeVideo, opt)
    figure(1)
    clf
    set(gcf,'position',[100,100,1500,1000])
    subplot(2,1,1) % EW
    plot(Outline{1,end}(:,1),Outline{1,end}(:,2),'Color',[0 0 0.5],'LineWidth',2)
    hold on
    plot(Outline{1,iVersion}(:,1),Outline{1,iVersion}(:,2),'r','LineWidth',2)
    title('EW','FontSize', 28)
    xlabel('Easting [km]', 'FontSize', 24)
    ylabel('Depth [km]','FontSize', 24)
    axis([opt.coord_x(1) opt.coord_x(2) opt.coord_z(1) opt.coord_z(2)])
    hold off

    subplot(2,1,2) % NS
    plot(Outline{2,end}(:,1),Outline{2,end}(:,2),'Color',[0 0 0.5],'LineWidth',2)
    hold on
    plot(Outline{2,iVersion}(:,1),Outline{2,iVersion}(:,2),'r','LineWidth',2)
    title('NS','FontSize', 28)
    xlabel('Northing [km]', 'FontSize', 24)
    ylabel('Depth [km]','FontSize', 24)
    axis([opt.coord_y(1) opt.coord_y(2) opt.coord_z(1) opt.coord_z(2)])
    hold off

    if makeVideo
        figure(1)
        Frames(iVersion) = getframe(gcf);
        drawnow
    else
        Frames(iVersion) = 0;
    end
end

%% unused but possibly useful supporting functions

% expects CtrlPoly to be in reference to volume and cant handle cases where
% CtrlPoly may be outside the volume
function [S_All] = interpStretchOld(Stretch, CtrlPoly, numPoly)
      
    % number of control polygons
    numCtrlPoly = length(CtrlPoly);
    
    % split stretch into x- and y-component to make things easier to read
    Sx          = Stretch(:,1);
    Sy          = Stretch(:,2);

    % preallocate stretchfactors for all polygons
    SxAll       = zeros(numPoly,1);
    SyAll       = zeros(numPoly,1);

    % enter first value
    SxAll(CtrlPoly(1),:)  = Sx(1);
    SyAll(CtrlPoly(1),:)  = Sy(1);
    
    % interpolate the rest
    for i = 2 : numCtrlPoly
        newSx                                   = linspace(Sx(i-1),Sx(i),CtrlPoly(i) - CtrlPoly(i-1) + 1);
        newSy                                   = linspace(Sy(i-1),Sy(i),CtrlPoly(i) - CtrlPoly(i-1) + 1);
        SxAll((CtrlPoly(i-1)+1):CtrlPoly(i))    = newSx(2:end);
        SyAll((CtrlPoly(i-1)+1):CtrlPoly(i))    = newSy(2:end);
    end
    
    % extend stretch to first and last polygon
    if CtrlPoly(1) ~= 1
        SxAll(1:CtrlPoly(1))        = Sx(1);
        SyAll(1:CtrlPoly(1))        = Sy(1);
    end
    if CtrlPoly(end) ~= numPoly
        SxAll(CtrlPoly(end):end)    = Sx(end);
        SyAll(CtrlPoly(end):end)    = Sy(end);
    end

    S_All   = [SxAll SyAll];
end

% takes min/max in x and y
function outline = getOutline2(Polygons,orient)
    numPoly = length(Polygons);
    outline = zeros(numPoly*2+1,2);
    switch orient
        case 'EW'
            col = 1;
        case 'NS'
            col = 2;
        otherwise
            error('Wrong orientaion in getOutline!')
    end
    
    for iPoly = 1 : numPoly
        outline(iPoly,1)                = max(Polygons{iPoly}(:,col));
        outline(iPoly,2)                = Polygons{iPoly}(1,3);
        
        outline(numPoly*2+1-iPoly,1)    = min(Polygons{iPoly}(:,col));
        outline(numPoly*2+1-iPoly,2)    = Polygons{iPoly}(1,3);
    end
    outline(end,:)                      = outline(1,:);  
end

% plot one of the orientations but doesn't do subplots
function plotOutlines2(Outlines,orient)
    switch orient
        case 'EW'
            outlines    = Outlines.EW;
            xl          = 'Easting';
            ti          = 'EW';
        case 'NS'
            outlines    = Outlines.NS;
            xl          = 'Northing';
            ti          = 'NS';
        otherwise
            error('Unknown orientation in plotOutlines')
    end
    
    n           = size(outlines,3) - 1;
    
    figure()
    hold on
    for i = 1 : n
        plot(outlines(:,1,i),outlines(:,2,i),'k','LineWidth',1)
    end
    plot(outlines(:,1,end),outlines(:,2,end),'r','LineWidth',2)
    title(ti,'FontSize', 28)
    xlabel(xl, 'FontSize', 24)
    ylabel('Depth','FontSize', 24)
end

% does not care about marker layers, just shifts polygons 
% (possibly useful for gravity because faster)
function NewVolume = stretchVolumeZ2(Volume,Sz)
    % copy Volume
    NewVolume = Volume;
    
    % check Sz
    if Sz < 0.1
        fprintf('Warning: Reset Sz to 0.1 \n')
        Sz = 0.1;
    end
    
    % find z-coordinate of center of mass of volume
    center          = findMassCenter(Volume);
    z_center        = center(3); 
    
    % loop over polygons
    for i = 1 : length(Volume.Polygons)
        % get polygon
        Poly        = Volume.Polygons{i};
        % convert z-coordiantes to relative coords
        Poly(:,3)   = Poly(:,3) - z_center;
        % stretch z-coordinates
        Poly(:,3)   = Poly(:,3) * Sz;
        % convert back to absolute coordiantes
        Poly(:,3)   = Poly(:,3) + z_center;
        % insert to NewVolume
        NewVolume.Polygons{i} = Poly;
    end
end


%% pvd file writing (not functional)
%{
in front of the version loop:
% write pvd file for paraview
if opt.varyGeom.writeParaview
    pvdFileName = [outputDir 'Versions'];
    if exist([pvdFileName,'.pvd'],'file')
        delete([pvdFileName,'.pvd']);
    end
end



in the main loop in the write paraview part
% append to pvd file
if opt.varyGeom.writeParaview
    is = strfind(opt.inputFileName,'/');
    fileName = [opt.inputFileName(is(end)+1:end-4) '_' volLabel];
    UpdatePVD_File(pvdFileName, [opt.outputDir, fileName, '.vtk'], iVersion)
end
%}

%% no longer needed

% function simplePolygon = simplifyPolygon(Polygon, theta)
%     num_Coord       = size(Polygon,1);
%     ind             = (1:2:num_Coord);
%     num_Coord       = length(ind);
%     Coords_old      = [Polygon(ind,1)' Polygon(1,1); Polygon(ind,2)' Polygon(1,2)];
%     Coords_new      = zeros(2,num_Coord);
%     Coords_new(:,1) = Coords_old(:,1);
%     iter_new        = 1;
%     for j = 2 : num_Coord
%         Coords          = Coords_old(:,j);
%         vecTo           = Coords - Coords_new(:,iter_new);
%         
%         vecFrom         = Coords_old(:,j + 1) - Coords;
%         dotproduct      = vecTo(1) * vecFrom(1) + vecTo(2) * vecFrom(2);
%         angle           = acosd(dotproduct/(norm(vecTo) * norm(vecFrom)));
%         
%         if angle > theta
%             iter_new                    = iter_new + 1;
%             Coords_new(:,iter_new)      = Coords;
%         end
%     end
%     Coords_new      = Coords_new(:,1:iter_new);
%     Coords_new      = [Coords_new; Polygon(1,3)*ones(1,iter_new)];
%     simplePolygon   = Coords_new';   
% end