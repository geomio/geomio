function [setup, opt] = readLaMEMinput(opt)
%
% call it as 
% opt.debug = true; % or false
% opt.LaMEMinputFileName = 'Tibet3d.dat';
% setup = ReadLamemInput(opt)
% setup = ReadLamemInput(opt,NewLaMEM), where NewLaMEM specifies the format 
% of the LaMEM file and is True or False
%
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   readLaMEMinput.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================filename = opt.LaMEMinputFileName;

%filename = opt.LaMEMinputFileName;
fstr = readFile(opt.LaMEMinputFileName);

% read important parameters
setup.nel_x     = read_param(fstr,'nel_x');
setup.nel_y     = read_param(fstr,'nel_y');
setup.nel_z     = read_param(fstr,'nel_z');
    
setup.seg_x     = read_param(fstr,'nseg_x');
setup.seg_y     = read_param(fstr,'nseg_y');
setup.seg_z     = read_param(fstr,'nseg_z');

if opt.NewLaMEM
      
    setup.coord_x    = read_param(fstr,'coord_x');
    setup.coord_y    = read_param(fstr,'coord_y');
    setup.coord_z    = read_param(fstr,'coord_z');
    
    setup.bias_x     = read_param(fstr,'bias_x');
    setup.bias_y     = read_param(fstr,'bias_y');
    setup.bias_z     = read_param(fstr,'bias_z');
    
    if isempty(setup.seg_x) == 0        
        setup = Create_seg_new_LaMEM(setup,'x');
    end
    
    if isempty(setup.seg_y) == 0        
        setup = Create_seg_new_LaMEM(setup,'y');        
    end
    
    if isempty(setup.seg_z) == 0        
        setup = Create_seg_new_LaMEM(setup,'z');
    end
    
    setup.x_left     = setup.coord_x(1);
    setup.y_front    = setup.coord_y(1);
    setup.z_bot      = setup.coord_z(1);
    
    setup.W         =  setup.coord_x(end)- setup.coord_x(1);
    setup.L         =  setup.coord_y(end)- setup.coord_y(1);
    setup.H         =  setup.coord_z(end)- setup.coord_z(1);
    
    setup.x(1)      =  setup.coord_x(1);
    setup.y(1)      =  setup.coord_y(1);
    setup.z(1)      =  setup.coord_z(1);
    
    setup.numx      = read_param(fstr,'nmark_x');
    setup.numy      = read_param(fstr,'nmark_y');
    setup.numz      = read_param(fstr,'nmark_z');
    
    
else
    
    setup.W         = read_param(fstr,'W');
    setup.L         = read_param(fstr,'L');
    setup.H         = read_param(fstr,'H');
    
    setup.x_left    = read_param(fstr,'x_left');
    setup.y_front   = read_param(fstr,'y_front');
    setup.z_bot     = read_param(fstr,'z_bot');
    
    setup.x(1) = read_param(fstr,'x_left');
    setup.y(1) = read_param(fstr,'y_front');
    setup.z(1) = read_param(fstr,'z_bot');

    setup.numx = read_param(fstr,'NumPartX');
    setup.numy = read_param(fstr,'NumPartY');
    setup.numz = read_param(fstr,'NumPartZ');
    
end



setup.x(2) = setup.x(1)+setup.W;
setup.y(2) = setup.y(1)+setup.L;
setup.z(2) = setup.z(1)+setup.H;



% create rectilinear coordinates for markers 
setup = CreateMarkerSpacing1D(setup,'x',opt);
setup = CreateMarkerSpacing1D(setup,'y',opt);
setup = CreateMarkerSpacing1D(setup,'z',opt);
 

% check aspect ratios
aspect_ratio(setup.x,setup.y,'xy',opt.debug);
aspect_ratio(setup.x,setup.z,'xz',opt.debug);
aspect_ratio(setup.y,setup.z,'yz',opt.debug);
fprintf('\n');

% Save in opt
opt.setup.W = setup.W;
opt.setup.H = setup.H;
opt.setup.L = setup.L;

% opt.setup.x = setup.x;
% opt.setup.y = setup.y;
% opt.setup.z = setup.z;

opt.setup.coord_x   = setup.coord_x;
opt.setup.coord_y   = setup.coord_y;
opt.setup.coord_z   = setup.coord_z;

opt.setup.marker_x  = setup.marker_x;
opt.setup.marker_y  = setup.marker_y;
opt.setup.marker_z  = setup.marker_z;

opt.setup.nel_x     = setup.nel_x;
opt.setup.nel_y     = setup.nel_y;
opt.setup.nel_z     = setup.nel_z;

opt.setup.numMark_x = setup.numx;
opt.setup.numMark_y = setup.numy;
opt.setup.numMark_z = setup.numz;

opt.setup.x_left    = setup.x_left;
opt.setup.y_front   = setup.y_front;
opt.setup.z_bot     = setup.z_bot;

end

% -------------------------------------------------------------------------
function fstr = readFile(filename)

% read file split in lines and store as string
%str_lines=splitlines(string(fileread(filename)));
str      = string(fileread(filename));
str_lines= strsplit(str,'\n');


% filter commented lines
noncomments  = (regexp(str_lines, '^\s*[^\s*#].*' ));
%ind = find(~cellfun(@isempty,noncomments));
%fstr = str_lines(ind);
fstr = str_lines(~cellfun(@isempty,noncomments));

end
% -------------------------------------------------------------------------
function ar = aspect_ratio(x,y,name,plotit)

    % cell cordinates and sizes
    dx = diff(x);
    dy = diff(y);
    d1 = min(dx)/max(dy); if (d1 < 1) d1 = 1/d1; end
    d2 = min(dy)/max(dx); if (d2 < 1) d2 = 1/d2; end
    ar = max([d1,d2]);
    xc = x(1:end-1) + dx./2;
    yc = y(1:end-1) + dy./2;

    % compute aspect ratio
    [DX,DY] = meshgrid(dx,dy);
    DXY = DX ./DY;
    DYX = DY ./DX;
    idx = find(DXY<1);
    AR = DXY;
    AR(idx) = DYX(idx);

    if ar >= 30
        fprintf('> LaMEM grid %s-aspectratio is %g\n',name,ar);

        figure('name',name)
        pcolor(xc,yc,AR); colorbar; axis equal
        title('aspect ratio')
        error(sprintf('LaMEM grid %s-aspectratio is larger than 30\n',name));
    elseif ar >= 3
        warning(sprintf('LaMEM grid %s-aspectratio is larger than 3: %g\n',name,ar));
        if plotit
            figure('name',name)
            pcolor(xc,yc,AR); colorbar; axis equal
            title('aspect ratio')
        end
    else
        fprintf('> LaMEM grid %s-aspectratio is %g\n',name,ar);
    end
end

% -------------------------------------------------------------------------
function param = read_param(fstr,paramname)
    t=regexp(fstr, ['\<' paramname '\>']);
    ind = find(~cellfun(@isempty,t));
    if ~isempty(ind)
        args=regexp(fstr{ind(end)}, '=|#','split');
        param = str2num(args{2});
    else
        param = [];
    end
end

% -------------------------------------------------------------------------
function setup = CreateMarkerSpacing1D(setup,i,opt)

% define variables
nel_i = ['nel_' i];
seg_i = ['seg_' i];
marker_i = ['marker_' i];
num_i = ['num' i];

% set spacing
if setup.(nel_i) < 0 
    
    % non-uniform spaceing

    n = abs(setup.(nel_i));
    inode = [setup.(i)(1) setup.(seg_i)(1:n-1) setup.(i)(2)];
    dseg  = diff(inode);
    ncell = setup.(seg_i)(n:2*n-1);
    rcell = setup.(seg_i)(2*n:3*n-1);
    avgdx  = dseg ./ ncell;

    startdx   =  2 .* avgdx ./ (1+rcell);
    enddx   = rcell .* startdx;
	deltadx = (enddx - startdx) ./(ncell-1);
    
    % nodes
    m=1;
    dx = zeros(1,sum(ncell));
    for k= 1:n
        dx(m:m+ncell(k)-1) = [startdx(k) cumsum(ones(1,ncell(k)-1) .* deltadx(k))+ startdx(k) ];
        m = m+ncell(k);
    end
    setup.(i) = [setup.(i)(1) cumsum(dx)+ setup.(i)(1)];

    % markers
    partdx = diff(setup.(i)) / setup.(num_i);
    setup.(marker_i) = zeros(1,sum(ncell)*setup.(num_i));
    m = 1;
    for k= 1:sum(ncell)
        setup.(marker_i)(m:m+setup.(num_i)-1) = setup.(i)(k) + cumsum([0.5*partdx(k) ones(1,setup.(num_i)-1)*partdx(k)]);
        m = m+setup.(num_i);
    end

else
    % uniform spaceing
    dx     = (setup.(i)(end) - setup.(i)(1)) / setup.(nel_i) ;
    partdx = dx / setup.(num_i);

    setup.(i) = setup.(i)(1) : dx : setup.(i)(end);
    setup.(marker_i) = (setup.(i)(1)+0.5*partdx) : partdx : (setup.(i)(end)-0.5*partdx); 


end

end

function setup = Create_seg_new_LaMEM(setup,i)

    % define variables
    nel_i = ['nel_' i];
    seg_i = ['seg_' i];
    bias_i = ['bias_' i];    
    coord_i = ['coord_' i];
    
    nel_temp = setup.(seg_i)*-1;
    setup.(seg_i) = [setup.(coord_i)(2:length(setup.(coord_i))-1) setup.(nel_i) setup.(bias_i)];
    setup.(nel_i) = nel_temp;
end