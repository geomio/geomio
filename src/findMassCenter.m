function center = findMassCenter(Volume)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville, Tobias S. Baumann &
%    Arne Spang
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   findMassCenter.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
%        Arne Spang       [arspang@uni-mainz.de]
% =========================================================================

    % number of polygons in volume
    numPoly = length(Volume.Polygons);
    % will later hold xcenter,ycenter, depth and area(weight) of the polygon
    Polygons = zeros(numPoly,4);
    for i = 1 : length(Volume.Polygons)
        [geom,~,~]  = polygeom(Volume.Polygons{i}(:,1),Volume.Polygons{i}(:,2));
        Polygons(i,:)   = [geom(2),geom(3),Volume.Polygons{i}(1,3),geom(1)];
    end
    center      = [0 0 0];
    center(1)   = sum(Polygons(:,1).*Polygons(:,4))/sum(Polygons(:,4));
    center(2)   = sum(Polygons(:,2).*Polygons(:,4))/sum(Polygons(:,4));
    center(3)   = sum(Polygons(:,3).*Polygons(:,4))/sum(Polygons(:,4));
%     fprintf('find Mass center took %.5f s\n',toc);
end