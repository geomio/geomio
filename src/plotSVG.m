function plotSVG(PathCoord, opt, varargin)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   plotSVG.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
FillIn = true;
if nargin == 3
    if varargin{1} == 0
        FillIn = false; 
    end
end

% do we use octave or not?
isOctave = exist('OCTAVE_VERSION', 'builtin') ~= 0;


% Res = 300/4; % ppi
% set(figure(1),'units','normalized','PaperUnits','inches','PaperSize',[opt.svg.width opt.svg.height]./Res,'PaperPosition',[0 0 opt.svg.width opt.svg.height]./Res,'visible','off','Position',[0 0 1 1])
% % AX1 = axes('units','normalized','position',[0 0 1 1]);
% AX1 = axes('units','normalized','position',[0.01 0.01 .9 .9]);
hold on
Layers = fieldnames(PathCoord);
noLayers = length(Layers);
for iL = 1:noLayers
    Paths = fieldnames(PathCoord.(Layers{iL}));
    noPaths = length(Paths);
    for iP = 1:noPaths
    
        % if the path is found in opt.pathNames
        if sum (strcmp((Paths{iP}),opt.pathNames))>0 
            SSP = PathCoord.(Layers{iL}).(Paths{iP}).StartSubPath;
            for iS = 1:length(SSP)-1
                IS = SSP(iS);
                IE = SSP(iS+1)-1;
                Coord = PathCoord.(Layers{iL}).(Paths{iP}).DrawCoord(:,IS:IE);
                Style = PathCoord.(Layers{iL}).(Paths{iP}).PathStyle;
                Comm  = PathCoord.(Layers{iL}).(Paths{iP}).Commands.String;
                if ~FillIn 
                    Style.fill = 'none';
                end
                if Style.stroke_width == 0 || isnan(Style.stroke_width)
                   Style.stroke_width = 1; 
                end
         
                if (strcmpi(Comm(end),'z')) || ~strcmp(Style.fill,'none')
                    if strcmp(Style.stroke,'none') || strcmp(Style.stroke,'empty')
                      if isOctave
                         fill(Coord(1,:),Coord(2,:),'r','FaceColor',Style.fill,'LineStyle','none')
                      else  
%                         fill(Coord(1,:),Coord(2,:),'r','FaceColor',Style.fill,'LineStyle','none','FaceAlpha',Style.fill_opacity)
                        fill(Coord(1,:),Coord(2,:),'r','FaceColor',Style.fill,'LineStyle','-','FaceAlpha',Style.fill_opacity)
                      end
                        
                    else
                      if isOctave
                        fill(Coord(1,:),Coord(2,:),'r','FaceColor',Style.fill,'EdgeColor',Style.stroke,'LineWidth',Style.stroke_width/2)
                      else
                        fill(Coord(1,:),Coord(2,:),'r','FaceColor',Style.fill,'EdgeColor',Style.stroke,'LineWidth',Style.stroke_width/2,'FaceAlpha',Style.fill_opacity)
                      end
                    end
                else
                    plot(Coord(1,:),Coord(2,:),'Color',Style.stroke,'LineWidth',Style.stroke_width/2)
                end

            end
        end
    end
end
axis equal
%axis([0 opt.svg.width 0 opt.svg.height])
