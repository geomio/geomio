function opt = checkOpts(opt)
%
% Check all input options
% 
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   checkOpts.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
fprintf('-----------------------------------------------------------------------------------\n');
fprintf('                                   geomIO\n\n');

% properly linked to installation ?
% 
% PWD = pwd;
% [Repo] = getGeomIORepoAdress()
% cd(Repo);
% system('git log -1 --abbrev-commit > ShortGitLog.txt');
% fid = fopen('ShortGitLog.txt');
% A = fscanf(fid,'%c');
% CommitInfo = regexp(A,'.+\n\n','match');
% fprintf('git status:\n')
% fprintf(CommitInfo{1})
% fclose(fid);
% 
% cd(PWD);

fprintf(['--- Job type: ' opt.runJob '\n\n']);

% debug mode ?
if opt.debug
    fprintf('--- Debug mode \n\n');
end
fprintf('-----------------------------------------------------------------------------------\n\n');

disp('=== I/O paths:');
% make sure the path strings have correct format
opt.inputDir  = cleanString(opt.inputDir);
opt.outputDir = cleanString(opt.outputDir);
opt.imgDir    = cleanString(opt.imgDir);



% input directory and files
if isdir(opt.inputDir)

%     if isfield(opt,'multiSVGs')
%         
%         % if this field doesn't exist we create it
%         if ~isfield(opt,'readMultiSVG')
%             opt.readMultiSVGs = true;
%         end
%     
%         for k = 1:length(opt.multiSVGs)
%             opt.multiSVGs{k} = [opt.inputDir opt.multiSVGs{k}];
% 
%             if exist(opt.multiSVGs{k},'file') == 2
%                 disp(['> Multiple SVG input files: ' opt.multiSVGs{k}])
%             else
%                 error(['Your input file does not exist:' opt.multiSVGs{k}])
%             end
%         end
% 
%         opt.inputFileName = opt.multiSVGs{1};
% 
%     else
        opt.inputFileName      = [opt.inputDir opt.inputFileName];
        if opt.useSVG
            if exist(opt.inputFileName,'file') == 2
                disp(['> Single input file: ' opt.inputFileName])
            else
                error(['Your input file does not exist:' opt.inputFileName])
            end
        end
%         opt.multiSVGs{1}= 0;
%         opt.readMultiSVGs = false;
%     end  

%     if isfield(opt,'orientVert')
%         opt.inputFileNameVert = [opt.inputDir opt.inputFileNameVert];
%         opt.Vert.outFileName = [opt.outputDir opt.Vert.outFileName];
%         if exist(opt.inputFileNameVert,'file') == 2
%             disp(['> Input file for vertical cross section : ' opt.inputFileNameVert])
%         else
%             error(['Your input file does not exist:' opt.inputFileNameVert])
%         end
%     end

    if opt.readLaMEM
        opt.LaMEMinputFileName = [opt.inputDir opt.LaMEMinputFileName];
        if exist(opt.LaMEMinputFileName,'file') == 2
            disp(['> LaMEM input file: ' opt.LaMEMinputFileName]);
        else
            error(['Your LaMEM input file does not exist:' opt.LaMEMinputFileName]);
        end
    end
else
	error(['Your input directory does not exist:' opt.inputDir])
end

if ~isdir(opt.outputDir)
    mkdir(opt.outputDir);
    disp(['> Output directory does not exist. Created the following directory: ']);
    disp(['> ' opt.outputDir]);
end
    

% get orientation
if strcmp(opt.orient,'ReadFromFileName')
    if opt.Mode == '3D'
        opt.orient = getOrientation(opt.inputFileName);
    else
        opt.orient = 'HZ';
    end
end
    
if strcmp(opt.orient,'NS')
    opt.normalDir = [1 0 0];
elseif strcmp(opt.orient,'EW') 
    opt.normalDir = [0 1 0];
elseif strcmp(opt.orient,'HZ')
    opt.normalDir = [0 0 1];
else
    error('Unknown orientation')
end


if opt.useSVG
    if ~isempty(opt.pathNames)
        if ~isempty(opt.phase)
            if length(opt.pathNames)~=length(opt.phase)
                error('The lengths of opt.pathNames and opt.phase should be identical')
            end
        end
    else
        if ~isempty(opt.phase)
            warning('opt.pathNames is not specified, but opt.phase is. The user defined opt.phase will be ignored. opt.phase will be read from the SVG file.')
        end
    end
end

% % get root of the inputfilename 
% opt.inputFileNameRoot = getRootString([opt.inputDir opt.inputFileName]);
% opt.outputFileNameRoot = getRootString([opt.outputDir opt.outputFileName]);


%opt.outputFileNameRootVert = getRootString([opt.outputDir opt.Vert.outFileName]);


% %order entries according to the orientation
% if isfield(opt,'xi')
%    opt.(opt.orient).xi = opt.xi;
%    opt                 = rmfield(opt,'xi');
% end
% 
% if isfield(opt,'yi')
%    opt.(opt.orient).yi = opt.yi;
%    opt                 = rmfield(opt,'yi');
% end

% /!\/!\/!\/!\  Should be fixed  /!\/!\/!\/!\
% if isfield(opt,'zi')
%     opt.(opt.orient).zi = opt.zi;
%     opt                 = rmfield(opt,'zi');
% end

% if isfield(opt,'z')
%     opt.(opt.orient).z  = opt.z;
%     opt                 = rmfield(opt,'z');
% end



% output directory
if ~isdir(opt.outputDir)
	disp('> Your output directory does not exist yet');
    opt.outputDir = [pwd '/Output/'];
    mkdir(opt.outputDir);
    disp(['> Created output directory: ' opt.outputDir]);
end

% output file name
if ~isempty(opt.outputFileName)
    opt.outputFileName     = [opt.outputDir opt.outputFileName];
    disp(['> Output file: ' opt.outputFileName])
else
    opt.outputFileName     = [opt.outputDir 'output_interp.' opt.orient '.svg'];
    disp(['> Your output filename does not exist; The name was defined as follows:']);
    disp(['> ' opt.outputFileName])
end


if opt.writeSVG
    if ~strcmp(opt.orient,getOrientation(opt.outputFileName))
        error('Filenames of in and output files are not consistent')
    end
end

% vertical slices
if opt.Vert.write
    if ~strcmp(opt.Vert.orient,getOrientation(opt.Vert.outFileName))
        error('Filenames of in and output files are not consistent')
    end
    if ~strcmp(opt.Vert.orient,'NS') && ~strcmp(opt.Vert.orient,'EW') && ~strcmp(opt.Vert.orient,'HZ')
        error('opt.Vert.orient must be NS, EW or HZ!')
    end
    if isempty(opt.Vert.xi) || isempty(opt.Vert.yi) || isempty(opt.Vert.zi)
        error('Define opt.Vert.xi, opt.Vert.yi and opt.Vert.zi!')
    end
    if ~isempty(opt.Vert.X_to_Y) && (length(opt.Vert.X_to_Y) > 1 || opt.Vert.X_to_Y < 0)
        error('opt.Vert.X_to_Y has to be a single positive value!')
    end
    if ~isempty(opt.Vert.ref) 
        if ~(length(opt.Vert.ref) == 4)
            error('ref has to be a 4 element vector!')
        elseif ~(opt.Vert.ref(1) < opt.Vert.ref(2)) || ~(opt.Vert.ref(3) < opt.Vert.ref(4))
            error('ref should be: [x_min x_max y_min Y_max]')
        end
    end
end


% geometry variation
if opt.varyGeom.do

    opt.varyGeom.CtrlPoly = sort(opt.varyGeom.CtrlPoly);
    
    if strcmp(opt.varyGeom.stretchType,'factor')
        % nothing to be done
    elseif strcmp(opt.varyGeom.stretchType,'absolute')
        % nothing to be done
    else
        error('Unknown polygon stretch type (factor or absolute)')
    end
    
    if opt.varyGeom.loadStretch
        % nothing to be done
    elseif strcmp(opt.varyGeom.genType,'grid')
        if (isfield(opt.varyGeom,'Grid') && isfield(opt.varyGeom.Grid,'lb') && isfield(opt.varyGeom.Grid,'ub') && isfield(opt.varyGeom.Grid,'num')) 
            numPoly = length(opt.varyGeom.CtrlPoly);
            if size(opt.varyGeom.Grid.lb,1) == numPoly
                if strcmp(opt.varyGeom.dim,'3D')
                    if size(opt.varyGeom.Grid.lb,2) == 2
                        if ~all(size(opt.varyGeom.Grid.lb) == size(opt.varyGeom.Grid.ub) & size(opt.varyGeom.Grid.lb) == size(opt.varyGeom.Grid.num))
                            error('opt.varyGeom.Grid.lb, opt.varyGeom.Grid.ub and opt.varyGeom.Grid.num all have to be the same size.')
                        end
                    else
                        error('opt.varyGeom.Grid.lb has to be a two-column vector [X,Y] for dim = 3D')
                    end
                    
                elseif strcmp(opt.varyGeom.dim,'3D_homo') || strcmp(opt.varyGeom.dim,'2D_X') || strcmp(opt.varyGeom.dim,'2D_Y')
                    if size(opt.varyGeom.Grid.lb,2) == 1
                        if ~all(size(opt.varyGeom.Grid.lb) == size(opt.varyGeom.Grid.ub) & size(opt.varyGeom.Grid.lb) == size(opt.varyGeom.Grid.num))
                            error('opt.varyGeom.Grid.lb, opt.varyGeom.Grid.ub and opt.varyGeom.Grid.num all have to be the same size.')
                        end
                    else
                        error('opt.varyGeom.Grid.lb has to be a one-column vector.')
                    end
                    
                else
                    error('opt.varyGeom.dim not understood: Options: 3D, 3D_homo, 2D_X, 2D_Y')
                end
                
            else
                error('opt.varyGeom.Grid.lb needs as many lines as opt.varyGeom.CtrlPoly.')
            end
            
        else
            error('If using gen type grid, also define Grid.lb, Grid.ub and Grid.num')
        end
        
        if ~all(opt.varyGeom.Grid.lb > opt.varyGeom.Smin)
            error('opt.varyGeom.Grid.lb needs to be > Smin (%.2f).',opt.varyGeom.Smin)
        end
        
        if ~all(opt.varyGeom.Grid.ub > opt.varyGeom.Grid.lb)
            error('opt.varyGeom.Grid.lb > opt.varyGeom.Grid.ub.')
        end

        if isfield(opt.varyGeom.Grid,'Sz')
            if ~(length(opt.varyGeom.Grid.Sz) == 3)
                error('opt.varyGeom.Grid.Sz needs to be a 3-element vector [lb, ub, num]')
            end
        end
        
        if isfield(opt.varyGeom.Grid,'noise')
            if opt.varyGeom.Grid.noise > 1
                error('Noise > 1 will lead to parameter overlap.')
            elseif opt.varyGeom.Grid.noise > 0.5
                warning('Noise > 0.5 might lead to some parameter overlap in teh grid.\n')
            end
        end
        
    else
        if strcmp(opt.varyGeom.genType,'acceptance')
            if isempty(opt.varyGeom.acceptRule)
                opt.varyGeom.acceptRule = opt.varyGeom.sigma;
            end
        elseif strcmp(opt.varyGeom.genType,'hist')
            % nothing to be done
        elseif strcmp(opt.varyGeom.genType,'chain')
            % nothing to be done
        elseif strcmp(opt.varyGeom.genType,'guidedChain')
            if isempty(opt.varyGeom.sigma2)
                opt.varyGeom.sigma2 = opt.varyGeom.sigma;
            end
        elseif strcmp(opt.varyGeom.genType,'normal')
            % nothing to be done
        else
            error('Unknown stretch generation type (acceptance, hist, chain, guidedChain, normal or grid)')
        end

        if strcmp(opt.varyGeom.dim,'3D')
            if ~(length(opt.varyGeom.mu) == 2 && length(opt.varyGeom.sigma) == 2)
                error('mu and sigma need to be 2 element vectors (x,y)')
            end
        elseif strcmp(opt.varyGeom.dim,'3D_homo') || strcmp(opt.varyGeom.dim,'2D_X') || strcmp(opt.varyGeom.dim,'2D_Y')
            if ~(length(opt.varyGeom.mu) == 1 && length(opt.varyGeom.sigma) == 1)
                error('mu and sigma need to be a single value each')
            end
        else
            error('opt.varyGeom.dim not understood: Options: 3D, 3D_homo, 2D_X, 2D_Y')
        end

        if opt.varyGeom.shiftZ
            if ~(length(opt.varyGeom.shiftZ) == 2)
                error('shiftZ needs to be a 2 element vector (mu & sigma)')
            end
        end

        if opt.varyGeom.Sz
            if ~(length(opt.varyGeom.Sz) == 2)
                error('Sz needs to be a 2 element vector (mu & sigma)')
            end
        end
    end

end

if opt.writePolygons
    if opt.getPolygons == 0
        % if opt.getPolygons is active, then switch opt.getPolygons on as
        % well
        opt.getPolygons = 1;
    end
    if ~isempty(opt.PolygonsFileName)
%         opt.PolygonsFileName     = [opt.outputDir opt.PolygonsFileName];
        disp(['> LaMEM inputfile will be written to: ' opt.PolygonsFileName])
    else
        opt.PolygonsFileName     = [opt.outputDir 'LaMEMInputPolygons.bin'];
        disp(['> Your filename for the LaMEM input does not exist; write to: ' opt.PolygonsFileName])
    end
end
fprintf('\n');

end

function str = cleanString(str)

    if strcmp(str(1),'/') % absolute path

        if ~strcmp(str(end),'/')
            str = [str '/'];
        end

    else % relative path

        if ~strcmp(str(end),'/')
            str = [str '/'];
        end

        if strcmp(str(1:2),'./')
           str = str(3:end);
        end

        str = [pwd '/' str];

    end
end


function str = getRootString(str)
    Extension = str(end-6:end);
    if    ~(strcmp(Extension,'.NS.svg') | strcmp(Extension,'.EW.svg') | strcmp(Extension,'.HZ.svg'))
        error('filename must have an identifier of this type *.NS.svg or *.EW.svg or *.HZ.svg')
    end
    is = strfind(str,'/');
    ie = strfind(str,'.');
    if length(ie)<2
        error('filename must have an identifier of this type *.NS.svg or *.EW.svg or *.HZ.svg')
    end
    str = str(is(end)+1:ie(end-1)-1);
end

function orient = getOrientation(str)
    i = strfind(str,'.');
    if length(i)==1
        error(['The filename ' str ' has no orientation suffix. Please add either NS, EW, or HZ']);
    end
    orient = str(i(end-1)+1:i(end)-1);

    if ~strcmpi(orient,{'NS','EW','HZ'})
        error(['The filename ' str ' orientation suffix does not fit to database. Can be either NS, EW, or HZ']);
    end

end
