function Phase = assignPhase2Markers(PathCoord, opt, Xp, Yp, Phase)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   assignPhase2Markers.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

% AX2 = axes('units','inches','position',[0.1 0.15 .8 .85].*[opt.svg.width opt.svg.height opt.svg.width opt.svg.height]./Res);
%
% % Get drawing coordinate of the rabbit
% myPaths     = fieldnames(PathCoord.myLayer);
% noPaths     = length(myPaths);
Layers = fieldnames(PathCoord);
noLayers = length(Layers);
for iL = 1:noLayers
    Paths = fieldnames(PathCoord.(Layers{iL}));
    
    % remove Paths "trench" or "mor" as they are not closed polygons
    ind = find(strncmp(Paths,'trench',6) | strncmp(Paths,'mor',3) | strncmp(Paths,'fault',5));
    if ~isempty(ind)
        Paths(ind) = [];
    end
    
    noPaths = length(Paths);
    for iP = 1:noPaths
        SSP = PathCoord.(Layers{iL}).(Paths{iP}).StartSubPath;
        for iS = 1:length(SSP)-1
            IS = SSP(iS);
            IE = SSP(iS+1)-1;
            % if the path is found in opt.pathNames
            if sum (strcmp((Paths{iP}),opt.pathNames))>0 
                Coord = PathCoord.(Layers{iL}).(Paths{iP}).DrawCoord(:,IS:IE);
                PathPhase = opt.phase(strcmp((Paths{iP}),opt.pathNames));
    %             PathPhase = PathCoord.(Layers{iL}).(Paths{iP}).Phase;
                % Assign a different phase to the particles inside the rabbit
                [IN]         = inpolygon(Xp(:),Yp(:),Coord(1,:),Coord(2,:));
                Phase(IN)    = PathPhase;
            end
        end
    end
end


