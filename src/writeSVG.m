function [docNode, docNode_str] = writeSVG(docNode,PathCoord,opt,orient,ax,outputFileName,makeRef,RefCoords)
%
% Call it as 
% [docNode_str,docNode] = writeSVG(docNode,PathCoord)
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   writeSVG.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================

svgList     = docNode.getElementsByTagName('svg');
currentSVG  = svgList.item(0);
currentSVG.setIdAttribute('id', true) % must be defined to use .getElementById, later on

num = strsplit(char(currentSVG.getAttribute('height')),{'m','p'});
svg.height  = str2double(num(1));
num = strsplit(char(currentSVG.getAttribute('width')),{'m','p'});
svg.width   = str2double(num(1));


layerFields    = fields(PathCoord);
pathFields     = fields(PathCoord.(layerFields{end}));

% number of new profiles (including the old ones)
noProf = length(layerFields);

% make a copy of first layer
cpylayer = docNode.getElementsByTagName('g').item(0).cloneNode(true);

% make copy of first path of first layer
cpypath = cpylayer.getElementsByTagName('path').item(0).cloneNode(true);

% remove all paths from cpylayer
noPaths = cpylayer.getElementsByTagName('path').getLength;
for k= 1:noPaths
    child = cpylayer.getElementsByTagName('path').item(0);
    cpylayer.removeChild(child);
end

% remove images from cpylayer
child = cpylayer.getElementsByTagName('image').item(0);
if ~isempty(child)
    cpylayer.removeChild(child);
end

% remove all layers from document
root     = docNode.getDocumentElement;    
noLayers = root.getElementsByTagName('g').getLength;
for k= 1:noLayers
    child = root.getElementsByTagName('g').item(0);
    root.removeChild(child);
end


% Create reference layer
thislayer = cpylayer.cloneNode(true);

% layer attributes
thislayer.setAttribute('id','RefLayer00');
thislayer.setAttribute('inkscape:label','Reference');

% create reference path
thispath = cpypath.cloneNode(true);
yrefPaper = svg.height - opt.yrefPaper;
%yrefPaper = svg.height - opt.(orient).yrefPaper;
InputCoord      = [];
%InputCoord(1,1) = opt.(orient).xrefPaper(1);
InputCoord(1,1) = opt.xrefPaper(1);
InputCoord(2,1) = yrefPaper(1);
%InputCoord(1,2) = opt.(orient).xrefPaper(2);
InputCoord(1,2) = opt.xrefPaper(2);
InputCoord(2,2) = yrefPaper(2);

PathStyle    = 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1';
Commands.String = 'M';
Commands.SectionLength = [2 0 0];
[ PathData ] = writePathData( Commands,InputCoord);

% change path attributes
%thispath.setAttribute('CoordRef', sprintf('%g,%g,%g,%g',opt.(orient).xref(1),opt.(orient).yref(1),opt.(orient).xref(2),opt.(orient).yref(2)));
thispath.setAttribute('CoordRef', sprintf('%g,%g,%g,%g',opt.xref(1),opt.yref(1),opt.xref(2),opt.yref(2)));
thispath.setAttribute('d', PathData);
thispath.setAttribute('style',PathStyle);
        
% append path to layer
thislayer.appendChild(thispath);

% append layer to root
root.appendChild(thislayer);

for il = 1:noProf
%     if ax(il) < 0
%         layerLabel = sprintf('%s_m%.3d',orient,abs(ax(il)));
%         layerLabelOut = sprintf('%s_m%.3d',orient,abs(ax(il)));
%     else
%         layerLabel = sprintf('%s_p%.3d',orient,abs(ax(il)));
%         layerLabelOut = sprintf('%s_p%.3d',orient,abs(ax(il)));
%     end
    
    if ax(il) < 0
        layerLabel = sprintf('m%.3d',abs(ax(il)));
        layerLabelOut = sprintf('m%.3d',abs(ax(il)));
    else
        layerLabel = sprintf('p%.3d',abs(ax(il)));
        layerLabelOut = sprintf('p%.3d',abs(ax(il)));
    end

    % create clone for current layer 
    thislayer = cpylayer.cloneNode(true);
    
    % change layer attributes
    thislayer.setAttribute('id',sprintf('ilayer%.3d',il));
    thislayer.setAttribute('inkscape:label',layerLabel);
    
    if opt.imgAdd
        try
        if il==1
            % define xlink    
            thissvg = docNode.getElementsByTagName('svg').item(0);
            thissvg.setAttribute('xmlns:xlink','http://www.w3.org/1999/xlink');
        else
            % shows only the first image
            thislayer.setAttribute('style','display:none')
        end

        % add image
        ImageInfo = imfinfo([opt.imgDir,layerLabelOut,'.', opt.imgType]);
        thisimage = docNode.createElement('image');
        thisimage.setAttribute('id',sprintf('ilayer_im_%.3d',il));

        if (~isnan(opt.imgWidth) && opt.imgWidth ~= ImageInfo.Width)
            warning('Widths of images is different. We use the width of the input SVG images');
        end

        if (~isnan(opt.imgHeight) && opt.imgHeight ~= ImageInfo.Height)
            warning('Heights of images is different. We use the width of the input SVG images');
        end

        thisimage.setAttribute('width',sprintf('%.5gpx',ImageInfo.Width));
        thisimage.setAttribute('height',sprintf('%.5gpx',ImageInfo.Height));
%         thisimage.setAttribute('width',sprintf('%.5gpx',opt.imgWidth));
%         thisimage.setAttribute('height',sprintf('%.5gpx',opt.imgHeight));
        thisimage.setAttribute('x',sprintf('%.5g',opt.imgX));
        thisimage.setAttribute('y',sprintf('%.5g',opt.imgY));
        thisimage.setAttribute('xlink:href',sprintf('%s%s.%s',opt.imgDir,layerLabelOut,opt.imgType));
        thisimage.setAttribute('sodipodi:absref',sprintf('%s%s.%s',opt.imgDir,layerLabelOut,opt.imgType));
        if opt.imgHide
            thisimage.setAttribute('style','image-rendering:optimizeSpeed;display:none');
        else
            thisimage.setAttribute('style','image-rendering:optimizeSpeed');
        end
        thisimage.setAttribute('preserveAspectRatio','none');
        thislayer.appendChild(thisimage);
        catch
           warning(['Image' [opt.imgDir,layerLabel,'.', opt.imgType] ' might not exist.']) 
        end
        
    end

    pathFields = fieldnames(PathCoord.(layerLabel));
    noPaths = length(pathFields);
    
    for ip = 1:noPaths        
        pathLabel = pathFields{ip};
        % create clone for current path
        thispath = cpypath.cloneNode(true);
        % remove CoordRef attribute
        thispath.removeAttribute('CoordRef')
        % remove id attribute
        thispath.removeAttribute('id')

        % extract path data
        InputCoord          = PathCoord.(layerLabel).(pathLabel).InputCoord;
        InputCoord(2,:)     = svg.height - InputCoord(2,:);
        PathStyle           = PathCoord.(layerLabel).(pathLabel).PathStyle;
        Stroke              = rgb2hex(PathCoord.(layerLabel).(pathLabel).PathStyle.stroke .* 255);
        StyleString         = ['fill:' PathStyle.fill ';' 'stroke:' Stroke ';' 'stroke-width:' num2str(PathStyle.stroke_width) ';' 'stroke-opacity:1'];
        Commands            = PathCoord.(layerLabel).(pathLabel).Commands;
        [ PathData ]        = writePathData( Commands,InputCoord);
        
        % change path attributes
        thispath.setAttribute('label', pathLabel);
        thispath.setAttribute('d', PathData);
        thispath.setAttribute('id', ['path' num2str(ip)]);
        thispath.setAttribute('style',StyleString);
        
%         if isfield(opt,'orientVert')
%             if strcmp(orient,opt.Vert.orient)
%                 thispath.setAttribute('style',[PathStyle ';stroke-dasharray:7.0,3.5;display:inline;stroke-dashoffset:0']);
% 
%                 if opt.(opt.Vert.orient).lock
%                     thispath.setAttribute('sodipodi:insensitive','true');
%                 end
%             else
%                 thispath.setAttribute('style',PathStyle);
%             end
%         else
%                 thispath.setAttribute('style',PathStyle);
%         end
        % append path to layer
        thislayer.appendChild(thispath);
        
    end
    % append layer to root
    root.appendChild(thislayer);

end

% Create new reference layer
if makeRef
    % remove old reference frame
    frame = root.getElementsByTagName('g').item(0);
    root.removeChild(frame);
       
    % Create reference layer
    thislayer = cpylayer.cloneNode(true);

    % layer attributes
    thislayer.setAttribute('id','RefLayer00');
    thislayer.setAttribute('inkscape:label','Reference');
    
    % create clone for current path
    thispath = cpypath.cloneNode(true);
    % remove CoordRef attribute
    thispath.removeAttribute('CoordRef')
    % remove id attribute
    thispath.removeAttribute('id')
        
    PathStyle    = 'fill:none;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1';
    Commands.String = 'M';
    Commands.SectionLength = [2 0 0];
    % Because Inkscape is weird----------------------%
    RefCoords(1,3) = opt.svg.height - RefCoords(1,3);
    RefCoords(1,4) = opt.svg.height - RefCoords(1,4);
    %------------------------------------------------%
    [ PathData ] = writePathData( Commands,[RefCoords(1,1) RefCoords(1,2); RefCoords(1,4) RefCoords(1,3)]);
    
    % change path attributes
    %thispath.setAttribute('CoordRef', sprintf('%g,%g,%g,%g',opt.(orient).xref(1),opt.(orient).yref(1),opt.(orient).xref(2),opt.(orient).yref(2)));
    thispath.setAttribute('CoordRef', sprintf('%g,%g,%g,%g',RefCoords(2,1),RefCoords(2,4),RefCoords(2,2),RefCoords(2,3)));
    thispath.setAttribute('d', PathData);
    thispath.setAttribute('style',PathStyle);
    
    % append path to layer
    thislayer.appendChild(thispath);
    
    % append layer to root
    root.appendChild(thislayer);
end


% write output
if opt.debug
    docNode_str = xmlwrite(docNode);
    disp(' > Modified svg file:');
    disp(docNode_str);
else
    xmlwrite(outputFileName,docNode);
    docNode_str = [];
    disp([' > Saved NEW svg file: ' outputFileName]);
end

fprintf('\n');
end
