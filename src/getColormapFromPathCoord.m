function [CMAP, PhaseMax] = getColormapFromPathCoord(PathCoord, opt, varargin)
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   getColormapFromPathCoord.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
if nargin == 2
    CMAP = [1 1 1];
elseif nargin == 3
    if length(varargin{1}) == 3 && max(varargin{1}) <= 1 && min(varargin{1}) >= 0
        CMAP = varargin{1};
    else
       error('the extra argument should be a 1*3 matrix, with values between 0 and 1') 
    end
end


PhaseMax = 0;

Layers = fieldnames(PathCoord);
noLayers = length(Layers);
for iL = 1:noLayers
    Paths = fieldnames(PathCoord.(Layers{iL}));
    noPaths = length(Paths);
    for iP = 1:noPaths
        % if the path is found in opt.pathNames
        if sum (strcmp((Paths{iP}),opt.pathNames))>0 
        
            Style = PathCoord.(Layers{iL}).(Paths{iP}).PathStyle;
            PathPhase = opt.phase(strcmp((Paths{iP}),opt.pathNames));
            if strcmp(Style.fill,'none')
              Style.fill = [1 1 1];
            end
            CMAP(PathPhase+1,:) = Style.fill;

            if PathPhase>PhaseMax
                PhaseMax = PathPhase;
            end
        end
    end
end
colormap(CMAP)