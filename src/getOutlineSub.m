function outline = getOutlineSub(vol,orient,coord,label)
% get faces and vertices
fv.faces = vol.TRI;

X = vol.CartCoord{1}(:);
Y = vol.CartCoord{2}(:);
Z = vol.CartCoord{3}(:);
fv.vertices = [X Y Z];

% make profile plane
x_min = min(X);     x_max = max(X);
y_min = min(Y);     y_max = max(Y);
z_min = min(Z);
coveredFlag = true;
switch orient
    case 'EW'
        p0  = [x_min, coord, z_min];
        n   = [0 1 0];
        ind = [1, 3];
        if coord < y_min || coord > y_max
            coveredFlag = false;
        end
    case 'NS'
        p0  = [coord, y_min, z_min];
        n   = [1 0 0];
        ind = [2, 3];
        if coord < x_min || coord > x_max
            coveredFlag = false;
        end
end

if coveredFlag
    % intersect
    prof = intersectPlaneSurf(fv,p0,n);

    if length(prof) > 1
        fprintf(['Warning: ',label,' might be split on ',orient,'-profile. Only returning 1 part.\n'])
    end

    outline = [prof{1}(ind(1),:)', prof{1}(ind(2),:)'];
else
    fprintf([label,' does not show up on the ',orient,'-profile!\n'])
    outline = [NaN, NaN];
end
end