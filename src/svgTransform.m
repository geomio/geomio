function [Coord] = svgTransform(TransformationString,Coord)
    % transformation of Coord in 2D
    % String contains the content of the transform attribute
    % Coord is a n x 2 matrix of coordinates
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   svgTransform.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================    

    AllStrings = regexp(TransformationString,['(matrix\([\d\s,-\.]+\))|' ...
                                 '(translate\([\d\s,-\.]+\))|' ...
                                 '(scale\([\d\s,-\.]+\))|' ...
                                 '(rotate\([\d\s,-\.]+\))|' ...
                                 '(skewX\([\d\s,-\.]+\))|' ...
                                 '(skewY\([\d\s,-\.]+\))|'],'match');
    
    noTransformations = length(AllStrings);
    
    % Loop through successive transformations
    for iT = 1: noTransformations
        
        OldCoord = [Coord ; ones(1,size(Coord,2))];
        
        String =  AllStrings{iT};
        Type = regexp(String,'[a-z]+','match');
        Type = Type{1};
        ValueString = regexp(String,'\(.+\)','match');
        ValueString = ValueString{iT}(2:end-1);
        ValueString = regexprep(ValueString,'-',' -');

        val = str2num(ValueString);
%         noargs = length(find(val));
        noargs = length(val);
        switch Type
            case 'matrix'
                A = [val(1:2:5) ; val(2:2:6) ; 0 0 1];
                NewCoord = A*OldCoord;

            case 'translate'
                if noargs == 2
                    A = [1 0 val(1) ; 0 1 val(2) ; 0 0 1];
                else
                    A = [1 0 val(1) ; 0 1 0 ; 0 0 1];
                end
                NewCoord = A*OldCoord;

            case 'scale'
                if noargs == 2
                    A = [val(1) 0 0 ; 0 val(2) 0; 0 0 1];
                else
                    A = [val(1) 0 0 ; 0 val(1) 0; 0 0 1];
                end 
                NewCoord = A*OldCoord;

            case 'rotate'
                a = val(1) * pi/180; % angle of rotation
                A = [cos(a) -sin(a) 0 ; sin(a) cos(a) 0 ; 0 0 1];

                if noargs == 1

                elseif noargs == 3
                    OldCoord(:,1) = OldCoord(1,:) - val(2);
                    OldCoord(:,2) = OldCoord(2,:) - val(3);
                end

                NewCoord = A*OldCoord;

                NewCoord(1,:) = NewCoord(1,:) + val(2);
                NewCoord(2,:) = NewCoord(2,:) + val(3);

            case 'skewX'
                a = val(1) * pi/180; % angle of rotation
                A = [1 tan(a) 0 ; 0 1 0 ; 0 0 1];
                NewCoord = A*OldCoord;
            case 'skewY'
                a = val(1) * pi/180; % angle of rotation
                A = [1 0 0 ; tan(a) 1 0 ; 0 0 1];
                NewCoord = A*OldCoord;
        end

        Coord = NewCoord(1:2,:);
    end

end