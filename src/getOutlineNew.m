function outline = getOutlineNew(Polygons,orient,Coord)
    numPoly  = length(Polygons);
    
    switch orient
        case 'EW'
            dimX = 1;
            dimY = 2;
            
        case 'NS'
            dimX = 2;
            dimY = 1;
        otherwise
            error('Wrong orientaion in getOutline!')
    end
    
    % preallocate and prepare
    outline = zeros(numPoly*2+1,2);
    ind     = true(numPoly*2+1,1);
    
    % make profile line
    profEnds = [min(Polygons{1}(:,dimX)), max(Polygons{1}(:,dimX))];
    for iPoly = 2 : numPoly
        if min(Polygons{iPoly}(:,dimX)) < profEnds(1)
            profEnds(1) = min(Polygons{iPoly}(:,dimX));
        end
        if max(Polygons{iPoly}(:,dimX)) > profEnds(2)
            profEnds(2) = max(Polygons{iPoly}(:,dimX));
        end
    end
    profLen  = abs(diff(profEnds));
    profEnds = [profEnds(1)-profLen/10, profEnds(2)+profLen/10];
    prof     = [profEnds(1), Coord; profEnds(2), Coord];
    
    % warning flags
    warn  = 0;
    warn2 = 0;
    
    for iPoly = 1 : numPoly
        % access Polygon x and y coords
        Pol = [Polygons{iPoly}(:,dimX),Polygons{iPoly}(:,dimY)];
        
        % find interesections
        [xi,~] = polyxpoly(prof(:,1),prof(:,2),Pol(:,1),Pol(:,2));
        
        if length(xi) < 2
            warn = 1;
            ind(iPoly) = false;
            ind(numPoly*2+1-iPoly) = false;
            continue
        end
        
        if length(xi) > 2
            warn2 = 1;
            xi = [max(xi) min(xi)];
        end
        
        % enter into outline matrix
        outline(iPoly,1)             = xi(1);
        outline(iPoly,2)             = Polygons{iPoly}(1,3);
        outline(numPoly*2+1-iPoly,1) = xi(2);
        outline(numPoly*2+1-iPoly,2) = Polygons{iPoly}(1,3);
    end
    
    % issue warning
    if warn
        fprintf('Warning: Some slices are not be present on %s-profile. \n',orient);
        % remove faulty ones
        outline = outline(ind,:);
    end
    if warn2
        fprintf('Warning: Some slices intersect %s-profile twice. Only the outer edges will be plotted \n', orient)
    end
    
    % close outline polygon
    outline(end,:)                      = outline(1,:);
end