function writeParaview(Volumes,pathLabel,opt)
%
% writeParaview(Volumes,pathLabel,opt)
%
% write volume as vtk fiel to read with paraview
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   writeParaview.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================


if strcmp(opt.CoordinateSystem,'Spherical') % i.e. if input in Lat Lon Depth
    [X,Y,Z,~] = lonLatR2xyz(Volumes.(pathLabel).Lon, ...
                            Volumes.(pathLabel).Lat, ...
                            6371 - Volumes.(pathLabel).Depth);
else
    X = Volumes.(pathLabel).CartCoord{1};
    Y = Volumes.(pathLabel).CartCoord{2};
    Z = Volumes.(pathLabel).CartCoord{3};
end


% Slightly shifts the object  so that they can be plot on top of another
I = find(strcmp(opt.pathNames,pathLabel));
if length(I) >1
    error(sprintf('the path %s was found more than once in opt.pathNames',pathLabel));
end

X = X + opt.shiftPVobj(1)*I;
Y = Y + opt.shiftPVobj(2)*I;
Z = Z + opt.shiftPVobj(3)*I;


is = findstr(opt.inputFileName,'/');
fileName = [opt.inputFileName(is(end)+1:end-4) '_' pathLabel];
CellScalars = Volumes.(pathLabel).phase*ones(size(Volumes.(pathLabel).TRI,1),1);
exportTriangulation2VTK(fileName,[X(:),Y(:),Z(:)],Volumes.(pathLabel).TRI,opt.outputDir,CellScalars);
fprintf([' > Saved paraview file: ' opt.outputDir fileName '.vtk \n']);


end