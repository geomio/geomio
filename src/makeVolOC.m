function Volumes = makeVolOC(Volumes, Lon, Lat, Depth, drawDir, sliceDir, opt)
% Build weak zone on top of subducting slabs inside change Slab dip function 
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville, Tobias S. Baumann &
%    Arne Spang
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   makeVolOC.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
%        Arne Spang       [arspang@uni-mainz.de]
% =========================================================================
if opt.transform2Cart
    % Get reference projection
    mstruct         = getReferenceProjection(opt.referenceProjection);
    
    [CartCoord{1}, CartCoord{2}] = mfwdtran(mstruct,Lat,Lon);
else
    CartCoord{1} = Lon;
    CartCoord{2} = Lat;
end
CartCoord{3} = Depth;

% check if there is another volume with the same name
for i = 1 : length(opt.pathNames)
    assert(~strcmp(opt.pathNames{i},'OceanicCrust'),'There must not be a volume named OceanicCrust when opt.varySub.addCrust flag is activated')
end

% make up path style
PathStyle.fill         = 'none';
PathStyle.fill_opacity = 1;
PathStyle.stroke       = [0 0 0];
PathStyle.stroke_width = 1;

Volumes.OceanicCrust.CartCoord    = CartCoord;
Volumes.OceanicCrust.ax           = [drawDir,sliceDir];
Volumes.OceanicCrust.type         = opt.varySub.type_OC;
Volumes.OceanicCrust.phase        = opt.varySub.ID_OC;
Volumes.OceanicCrust.PathStyle    = PathStyle;
Volumes.OceanicCrust.TRI          = triangulateVolumes( CartCoord{drawDir(1)}, CartCoord{drawDir(2)}, opt );

opt.pathNames                 = [opt.pathNames, 'OceanicCrust'];

% Create polygon slices
%=====================================
fprintf('\n');
fprintf('Adding Oceanic Crust in subducting plate \n');
if opt.getPolygons
    [Volumes.OceanicCrust.Polygons, Volumes.OceanicCrust.PolyPos] = makePolygonSlices(...
        Volumes.OceanicCrust.CartCoord,Volumes.OceanicCrust.ax,Volumes.OceanicCrust.TRI,...
        opt.normalDir, {opt.setup.marker_x , opt.setup.marker_y, opt.setup.marker_z}, opt);
    
    % Check for overlapping polygons on the same slice
    checkOverlap(Volumes.OceanicCrust,'OceanicCrust',opt);
end


% Save paraview
%=====================================
if opt.writeParaview
    writeParaview(Volumes,'OceanicCrust',opt)
end
fprintf('\n');
end