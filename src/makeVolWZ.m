function Volumes = makeVolWZ(Volumes, Lon, Lat, Depth, drawDir, sliceDir, opt)
% Build weak zone on top of subducting slabs inside change Slab dip function 
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville, Tobias S. Baumann &
%    Arne Spang
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   makeVolWZ.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
%        Arne Spang       [arspang@uni-mainz.de]
% =========================================================================

if opt.transform2Cart
    % Get reference projection
    mstruct         = getReferenceProjection(opt.referenceProjection);
    
    [CartCoord{1}, CartCoord{2}] = mfwdtran(mstruct,Lat,Lon);
else
    CartCoord{1} = Lon;
    CartCoord{2} = Lat;
end
CartCoord{3} = Depth;

% check if there is another volume with the same name
for i = 1 : length(opt.pathNames)
    assert(~strcmp(opt.pathNames{i},'WeakZone'),'There must not be a volume named WeakZone when opt.varySub.addWZ flag is activated')
end

% make up path style
PathStyle.fill         = 'none';
PathStyle.fill_opacity = 1;
PathStyle.stroke       = [0 0 0];
PathStyle.stroke_width = 1;

Volumes.WeakZone.CartCoord    = CartCoord;
Volumes.WeakZone.ax           = [drawDir,sliceDir];
Volumes.WeakZone.type         = opt.varySub.type_WZ;
Volumes.WeakZone.phase        = opt.varySub.ID_WZ;
Volumes.WeakZone.PathStyle    = PathStyle;
Volumes.WeakZone.TRI          = triangulateVolumes( CartCoord{drawDir(1)}, CartCoord{drawDir(2)}, opt );

opt.pathNames                 = [opt.pathNames, 'WeakZone'];

% Create polygon slices
%=====================================
fprintf('\n');
fprintf('Adding Weak Zone \n');
if opt.getPolygons
    [Volumes.WeakZone.Polygons, Volumes.WeakZone.PolyPos] = makePolygonSlices(...
        Volumes.WeakZone.CartCoord,Volumes.WeakZone.ax,Volumes.WeakZone.TRI,...
        opt.normalDir, {opt.setup.marker_x , opt.setup.marker_y, opt.setup.marker_z}, opt);
    
    % Check for overlapping polygons on the same slice
    checkOverlap(Volumes.WeakZone,'WeakZone',opt);
end


% Save paraview
%=====================================
if opt.writeParaview
    writeParaview(Volumes,'WeakZone',opt)
end
fprintf('\n');
end
