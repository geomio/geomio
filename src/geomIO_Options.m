classdef geomIO_Options < handle %& dynamicprops
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville, Tobias S. Baumann &
%    Arne Spang
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   geomIO_Options.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
%        Arne Spang       [arspang@uni-mainz.de]
% =========================================================================
    properties
        inputDir        = './';
        outputDir       = './Output';
        
        inputFileName   = ['geomIO_IN.HZ.svg'];
        outputFileName  = [];
        
        PolygonsFileName = ['Polygons.bin'];
        LaMEMinputFileName = 'none';
        
        % Related to input file
        orient              = 'ReadFromFileName';
        
        z                  =  'ReadZFromFile';
        zi                 =  'NotSpecified';
        
        xrefPaper          = [ 0 744 ];
        yrefPaper          = [ 0 1052 ];
        
        xref               = [ 0 744 ];
        yref               = [ 0 1052 ];
        
        
        shiftPVobj        = [0 0 0];
        closedContours      = true;
        
        % Output options
        imgAdd              = true;
        interp              = true;
        debug               = false;
        getPolygons         = false;
        
        writeParaview       = false;
        writeSVG            = false;
        
        writeVertSVG        = false;
        writeVolumes         = true;
        
        % Geometry options
%         dirNormal           = [1 0 0];
        CoordinateSystem    = 'Cartesian'; % Cartesion or Spherical
        InterpType          ='PCHIP';
        transformCoord      = true; % Paper2LatLon or to whatever units CoordRef is in (in the svg file)

        % Spherical to cartesian coordinate system, using a map projection
        % note that CoordinateSystem should then be cartesian
        referenceProjection = 'NotSpecified';
        transform2Cart      = false; % Lat Lon 2 LaMEM, Cartesian / requires the Mapping Toolbox
        
        % Paths
        pathNames           = {}; % in drawing order from bottom to top
        phase               = [];
        type                = [];

%         readPhaseFromSVG = true;
        
        DrawCoordRes        = 21; % no points per segment in DrawCoord
        
        
        % Images
        imgDir          = ['Images/']; % path from the svg file
        imgType         = 'jpg';
        imgX            = 0;
        imgY            = 0;
        
        %setup = struct('units','geo','W',0,'L',0,'originAtLLC','false','marker_x',[],'marker_y',[],'marker_z',[]);
        setup = struct('units','geo','W',0,'L',0,'H',0,'x_left',0,'y_front',0,'z_bot',0,'originAtLLC','false','marker_x',[],'marker_y',[],'marker_z',[]);
        
        Box = struct('xmin',[],'xmax',[],'ymin',[],'ymax',[]); % Coordinate of the box draw on the "Box" layer, for convenience only. No computation is done with those
        
        svg = struct('width',[],'height',[]);
        
        useSVG = true;
        
%         % Multi SVGs
%         multiSVGs= {};
%         readMultiSVGs = false;
%         inputFileNameRoot
%         outputFileNameRoot
        
        % /!\ /!\ /!\ /!\ /!\ /!\
        % Kind of dirty
        % Multi svg requires opt.(opt.orient).z etc...
        % Therefore I initialize them here

        % Vertical slices
        Vert = struct('write',false,'orient',[],'xi',[],'yi',[],'zi',[],'outFileName','outVert','imgDir','VertImg','center',false,'X_to_Y',[],'ref',[],'simplify',[]);        
        % write:        make vertical slices or not
        % orient:       orientation of slices
        % xi:           raster in x-direction
        % yi:           raster in y-direction
        % zi:           raster in z-direction
        % outFileName:  name of output file
        % imgDir:       directory for Images
        % center:       start drawing at 0,0 or not
        % X_to_Y:       scaling between x and y axis (=1: equal axis, =2: exaggerated in x-direction, =0.5 exaggerated in y-direction)
        % ref:          choose the extends of the new reference frame ([x_min x_max y_min y_max])
        % simplify:     reduces number of nodes in the output file. only takes a new node after x degrees of curvature 
        %               2 - 5 is probably a reasonable value (0.01 will still eliminate all unnecessary nodes on non curved polygons)

        % Geometry variation
        varyGeom = struct('do',false,'volume','','loadStretch',[],'n',1,'stretchType','factor','dim','3D','mu',[],'sigma',[],'Smin',0.01,'Sz',[],'SzType','com','shift',[],'shiftZ',[],'theta',0,'genType','normal','sigma2',[],'acceptRule',[],'secondChance',[0.0 0.0],'histNumCand',100000,'CtrlPoly',[],'CP_ref','global','CP_track',false,'outName','varyGeom','outNameOffset',0,'writeParaview',false,'writePolygons',false,'saveVols',[0],'drawOutlines',false,'outlineProf',[0 0],'loadPolygons',{{false,'',''}},'gravOnly',false);
        % do:               make geometry variations or not
        % volume:           name of volume to vary
        % loadStretch:      name of mat file to load
        % n:                number of variations per volume
        % stretchType:      stretch by 'factor (default)' or 'absolute' value
        % dim:              which dimensions to vary ['3D','3D_homo','2D_X','2D_Y']
        % mu:               [mean Sx, mean Sy] or [mean Dx, mean Dy] (Sx & Sy are stretch factors, Dx & Dy are values for absolute stretch)
        % sigma:            [sigma Sx, Sigma Sy] or [sigma Dx, sigma Dy]
        % Smin:             minimum stretch factor to avoid polygons getting too small (default is 0.01)
        % Sz:               stretch in z-direction [mean Sz, sigma Sz]
        % SzType:           which part of the body stays fixed? ['com','top','bottom','middle']
        % shift:            shift in x & y [mean shiftX, mean shiftY, sigma shiftX, sigma shiftY]
        % shiftZ:           shift entire Volume in z-direction
        % theta:            rotates x-y-coordinate system clockwise
        % genType:          how should parameters be generated. normal (default), hist, chain, guidedChain, acceptance or grid
        % sigma2:           sigma2 for guided chain method controls how steep the probability peak is (default will be sigma)
        % acceptRule:       how far can a new value be from the last one for both dimensions (only needed if genType = acceptance, default is sigma)
        % secondChance:     how likely does a new value get accepted even if it does not satisfy the acceptance rule for both dimensions (only needed if genType = acceptance, default is [0.1 0.1])
        % histNumCand:      number of candidates generated when using genType = hist (default = 100000)
        % Grid.lb:          for genType=grid, lower bound of values (column vector, one row for each control polygon, 2 columns for dim=3D)
        % Grid.ub:          for genType=grid, upper bound of values (column vector, one row for each control polygon, 2 columns for dim=3D)
        % Grid.num:         for genType=grid, number of grid points between lb and ub (column vector, one row for each control polygon, 2 columns for dim=3D)
        % Grid.Sz:          for genType=grid, [lb, ub, num] for Sz
        % Grid.noise:       for genType=grid, add random noise to parameters, value will be multiplied with half of grid spacing and serve as standard deviation, should be < 0.5
        % CtrlPoly:         e.g. [3,7,17] (indices of polygons to find stretch parameters for)
        % CP_ref:           control polygons will be in reference to the 'global (default)' levels or the 'volume' slices
        % CP_track:         true or false(defalt) (should control polygons shift with the rest of the body when it is stretched in z)
        % outName:          name of output files
        % outNameOffset:    offset the numbering of output files in case of producing in mulitple batches
        % writeParaview:    output a vtk file for paraview for each variation (a few MB for each variation and volume)
        % writePolygons:    output a polygon file to be read by LaMEM for each variation (a few MB each variation)
        % saveVols:         save the produced volumes (0=no, 1=necessary, 2=full)
        % drawOutlines:     output a file containing the outlines of each variation in EW and NS view
        % outlineProf:      [Y-coord of EW-profile, X-coord of NS-profile]
        % loadPolygons:     [path of Vol.mat, name of Volume to be replaced] (Vol in Vol.mat should contain PolyPos and Polygons)
        % gravOnly:         true or false (uses a simpler algorithm to stretch in z-direction)

        % Subduction angle
        varySub = struct('do',false,'vols',{''},'ref','coord','theta',0,'tolZ',2,'drawOutlines',false,'outlineProf',[0,0],'addWZ',false,'d_WZ',0,'ID_WZ',[],'type_WZ',0,'d_Lith',0,'addCrust',false,'d_OC',0,'ID_OC',[],'type_OC',0);
        % do:               change the subduction angle or not
        % vols:             name of slab volumes, list as cell array (only one so far)
        % ref:              reference system for coordinates ('trench','coord'(default: 'coord'))
        % xRot:             distance between reference and rotation center (may give multiple)
        % theta:            change in angle for everything beyond xRot (>0: steepen slab, <0: flatten slab) (as many entries as xRot)
        % tolZ:             tolerance in z-direction when defining top and bottom of the plate (default: 2)
        % drawOutlines:     draw outlines of units [true or false]
        % outlineProf:      [Y-coord of EW-profile, X-coord of NS-profile]
        
        % addWZ:            add weak zone on top of the slab [true or false]
        % d_WZ:             thickness of weak zone (same units as coordinates)
        % ID_WZ:            phase ID of weak zone
        % type_WZ:          marker type of weak zone [0=overwriting, 1=additive]
        % d_Lith:           thickness of lithosphere of overriding plate in km

        % addCrust:         add crust under the top of the slabe [true or false]
        % d_OC:             thickness of crust in km (same units as coordinates)
        % ID_OC:            phase ID of oceanic crust
        % type_OC:          marker type of oceanic crust [0=overwriting, 1=additive]


        % Mode: 2D or 3D, orientation checks will be jumped for 2D
        Mode = '3D';
        
        % Other stuff
        normalDir = [1 0 0];
        
        
        % LaMEM stuff
        NewLaMEM            = true;
        readLaMEM           = false;
        writePolygons       = false;
        originAtLLC         = false;
        
        svgEditor = 'Inkscape'; % 'Inkscape' or 'Illustrator'
        
        runJob = 'default';
        
        % gravity computation
        gravity = struct('drho',[],'survey_x',[],'survey_y',[],'survey_z',[],'lenUnit','km');
        
    end
    methods

    end
end