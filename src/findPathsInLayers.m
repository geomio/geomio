function [ LayersCell , varargout ] = findPathsInLayers( PathCoord, pathLabel )
% [ LayersCell ] = findPathsInLayers( PathCoord, pathLabel )
% [ LayersCell , IL ] = findPathsInLayers( PathCoord, pathLabel )
% LayersCell contains the name of the layers containing the path designated
% by pathLabel
% I stores the index corresponding the layers in the PathCoord structure
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   findPathsInLayers.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
Layers = fieldnames(PathCoord);
LayersCell = {};
C = 0;
IL = [];
for iL = 1:length(Layers)
    Paths = fieldnames(PathCoord.(Layers{iL}));
    if max(strcmp(Paths,pathLabel)) == 1
        C = C+1;
        LayersCell{C} = Layers{iL};
        IL = [IL iL];
    else
        % This error is unjustified since a given path doesn't need to be
        % on all layers
        %error('Probably, there are unlabeled path in your SVG')
        
    end
end

varargout{1} = IL;