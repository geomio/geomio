function Volumes = makeVolume( PathCoord, Volumes, pathLabel, opt )
% UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
% Transform coordinates from paper to lat/lon/depth to cartesian
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   makeVolume.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================fprintf(' > Create Volume \n');


% input: lat, lon, output: x,y !!
[ Lat, Lon, Depth, drawDir, sliceDir ] = paper2LatLonDepth( PathCoord, pathLabel, opt.orient, opt );

if opt.varySub.do
    if strcmp(pathLabel,opt.varySub.vols)
        [Volumes, Lat, Lon, Depth] = changeSlabDip(Volumes, Lat, Lon, Depth, drawDir, sliceDir, opt);
    end
end

if opt.transform2Cart
    % Get reference projection
    mstruct         = getReferenceProjection(opt.referenceProjection);
    
    [CartCoord{1}, CartCoord{2}] = mfwdtran(mstruct,Lat,Lon);
    
    % origin of LaMEM domain at the Lower Left Corner
    %     if opt.setup.originAtLLC
    %         CartCoord{1} = CartCoord{1} - opt.setup.x(1);
    %         CartCoord{2} = CartCoord{2} - opt.setup.y(1);
    %     end
else
    CartCoord{1} = Lon;
    CartCoord{2} = Lat;
end
CartCoord{3} = Depth;



% if opt.transform2Cart
% %     CartCoord{1} = CartCoord{1}*1e3;
% %     CartCoord{2} = CartCoord{2}*1e3;
% %     CartCoord{3} = CartCoord{3}*1e3;
% end

I = find(strcmp(pathLabel,opt.pathNames));

% Make volumes
if strcmp(opt.CoordinateSystem,'Spherical')
    Volumes.(pathLabel).Lat       = Lat;
    Volumes.(pathLabel).Lon       = Lon;
    Volumes.(pathLabel).Depth     = Depth;
end
% Volumes.(pathLabel).normalDir = opt.normalDir;
% Volumes.(pathLabel).orient    = opt.orient;

Volumes.(pathLabel).CartCoord = CartCoord;
Volumes.(pathLabel).ax        = [drawDir,sliceDir];
Volumes.(pathLabel).phase     = opt.phase(I);
[ Layers , ~ ]                = findPathsInLayers( PathCoord, pathLabel );
Volumes.(pathLabel).PathStyle = PathCoord.(Layers{1}).(pathLabel).PathStyle;
Volumes.(pathLabel).TRI       = triangulateVolumes( CartCoord{drawDir(1)}, CartCoord{drawDir(2)}, opt );
if isempty(opt.type)
    Volumes.(pathLabel).type  = 0;
else
    Volumes.(pathLabel).type  = opt.type(I);
end

end
