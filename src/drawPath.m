function Ax = drawPath( PathStyle, InputCoord, DrawCoord, Commands,  varargin )
%drawPath( InputCoord, DrawCoord, Commands, svg )
%   InputCoord is a N*2 array that contains the raw coordinates of the path from the SVG file
% DrawCoord is a N*2 array that is finely resolved that can be drawn with plot an
% that include the interpretation of bezier curves
% Commands is a structure containing information about the path
% svgCell is a Cell array containing the SVG tag block
% the option 'debug' can be added to draw the control points
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   drawPath.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================




%% Get syle info
% if opt.svg
% style.fill      = extractStyle(StyleString, 'fill');
% style.stroke    = extractStyle(StyleString, 'stroke');
% style.stroke    = hex2rgb(style.stroke(2:end));
% style.stroke_width    = extractStyle(StyleString, 'stroke-width');
% style.stroke_width = regexp(style.stroke_width,'[^px]','match');
% style.stroke_width = str2double(style.stroke_width{1});

% PathStyle.fill          = 'none';
% PathStyle.stroke        = [0 0 0];
% PathStyle.stroke_width  = [.5];




% style.linecap   = extractStyle(StyleString, 'stroke-linecap')
% style.stroke_opacity  = extractStyle(StyleString, 'stroke-opacity')



% Create the DrawCoord array
DrawCoord = [];
Cend = 0;
StartSubPath = 1;
for iC = 1:length(Commands.String)
    Comm = Commands.String(iC)
    Cstart = Cend +1;
    Cend   = Cstart + Commands.SectionLength(iC) - 1;
    
    
    Coord = InputCoord(Cstart:Cend,:);
    switch Comm
        case {'M','m','L','l'}
            DrawCoord = [DrawCoord ; Coord];
        case {'C','c','S','s'}
            %             if strcmp(Comm,'C')
            Coord = [InputCoord(Cstart-1,:) ; Coord];
            %             end
            
            %             no_triplets = size(Coord,1)/3;
            no_triplets = (size(Coord,1)-1)/3;
            for iT = 1:no_triplets
                Istart = 2+(iT-1)*3;
                [~,b] = bezier(Coord(Istart+[-1:2],:),50 );
                b(1,:) = []; % Avoids doubling points at end/beginning of segments
                DrawCoord = [DrawCoord;b];
            end
            
        case {'Z','z'}
            StartSubPath = [StartSubPath length(DrawCoord)+1];
        otherwise
            
    end
end
if StartSubPath(end)~=length(DrawCoord)+1
    StartSubPath = [StartSubPath length(DrawCoord)+1];
end



%% Draw control points
if ~isempty(varargin)
    Debug = varargin{1};
else
    Debug = 'NoDebug';
end


    fill(DrawCoord(:,1),DrawCoord(:,2),PathStyle.fill,'EdgeColor','none')



% style.stroke
% DrawCoord(:,1)
% style.stroke_width
plot(DrawCoord(:,1),DrawCoord(:,2),'-k','Color',PathStyle.stroke,'Linewidth',PathStyle.stroke_width+0.0001)


if strcmp(Debug,'debug')
    Cend = 0;
    for iC = 1:length(Commands.String)
        
        Cstart = Cend +1;
        Comm = Commands.String(iC)
%         switch Comm
%             case {'S','s'}
%             	Cend   = Cstart + Commands.SectionLength(iC) + 1 - 1;
%             otherwise
                Cend   = Cstart + Commands.SectionLength(iC) - 1;
%         end
        
        
        Coord = InputCoord(Cstart:Cend,:);
        switch Comm
            case {'C','c','S','s'}
                Coord = [InputCoord(Cstart-1,:) ; Coord];
                
                no_triplets = (size(Coord,1)-1)/3
                DrawCoord = [];
                for iT = 1:no_triplets
                    Istart = 2+(iT-1)*3;
                    
                    Iori = Istart -1;
                    Icp1 = Istart;
                    Icp2 = Istart+1;
                    Ilast= Istart +2;
                    plot(Coord([Iori Icp1],1),Coord([Iori Icp1],2),'-b','Color',[.5 .5 1])
                    plot(Coord([Icp2 Ilast],1),Coord([Icp2 Ilast],2),'-b','Color',[.5 .5 1])
                    %
                    plot(Coord([Iori Ilast],1),Coord([Iori Ilast],2),'sk','MarkerFaceColor','b')
                    plot(Coord([Icp1 Icp2 ],1),Coord([Icp1 Icp2 ],2),'ok')
                    
                end
                
        end
        
    end
end
Ax = gca;

end



function OptionValue = extractStyle(String, optName)
OptionValue = regexp(String,[optName ':[^;]+'],'match');
%     Option = regexp(StyleString,'fill:[^;]+','match')
OptionValue = OptionValue{1}(length(optName)+2:end);
end

