function [Polygons, pos] = makePolygonSlices(Xds,ax, TRI, dirNormal, Xpart,opt)
%
% [Polygons, pos] = makePolygonSlices(Xds,ax, TRI, dirNormal, Xpart,opt)
%
%
% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville & Tobias S. Baumann
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   makePolygonSlices.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
% =========================================================================
fprintf(' > Create polygons: ');


% [ TRI ] = TriangulateVolumes( XdDir1, XdDir2 );

% get coordinates
X = Xds{1}(:);
Y = Xds{2}(:);
Z = Xds{3}(:);

% xpart = Xpart{ax(1)}(:);
% ypart = Xpart{ax(2)}(:);
% zpart = Xpart{ax(3)}(:);

xpart = Xpart{1}(:);
ypart = Xpart{2}(:);
zpart = Xpart{3}(:);

% CoordGrid contains x,y,z values of points on the surface, such that x and
% y and regularly gridded
x_min = min(X);     x_max = max(X);
y_min = min(Y);     y_max = max(Y);
z_min = min(Z);     z_max = max(Z);

xp = xpart(xpart >= x_min & xpart <= x_max); % xp Test, i.e. used for tor the interpolation
yp = ypart(ypart >= y_min & ypart <= y_max);
zp = zpart(zpart >= z_min & zpart <= z_max);

xShift = find(xpart >= x_min & xpart <= x_max,1,'first');
yShift = find(ypart >= y_min & ypart <= y_max,1,'first');
zShift = find(zpart >= z_min & zpart <= z_max,1,'first');


% triangulation
fv.faces    = TRI;
fv.vertices = [X Y Z];

% check extensions & dimensions
if opt.debug
    fprintf('x-vol  [%d %d]  xp [%d %d] \n',  [x_min x_max],[min(xpart) max(xpart)]);
    fprintf('y-vol  [%d %d]  yp [%d %d] \n',  [y_min y_max],[min(ypart) max(ypart)]);
    fprintf('z-vol  [%d %d]  zp [%d %d] \n\n',[z_min z_max],[min(zpart) max(zpart)]); 
end


tic

% orientation of slices

cstDir = find(dirNormal);
if cstDir == 1
    no_part     = length(xp);
    min_cDir    = min(X(TRI),[],2);
    max_cDir    = max(X(TRI),[],2);
    Shift       = xShift-1;
elseif cstDir == 2
    no_part     = length(yp);
    min_cDir    = min(Y(TRI),[],2);
    max_cDir    = max(Y(TRI),[],2);
    Shift       = yShift-1;
elseif cstDir == 3
    no_part     = length(zp);
    min_cDir    = min(Z(TRI),[],2);
    max_cDir    = max(Z(TRI),[],2);
    Shift       = zShift-1;
else
    error('dirNormal is not well defined')
end    

if no_part == 0
    error('The volume defined by Xcart, Ycart, Zcart is not inside the box defined by xpart, ypart, zpart')
end

% get polygons
Polygons = cell(size(no_part));
p0       = [x_min y_min z_min];

n=1;
for iS = 1:no_part

    if cstDir ==1
        p0(cstDir) = xp(iS);
    elseif cstDir == 2
        p0(cstDir) = yp(iS);
    elseif cstDir == 3
        p0(cstDir) = zp(iS);
    else
        error('dirNormal is not well defined')
    end
    
    Itri         = (min_cDir<=p0(cstDir) & max_cDir>=p0(cstDir));
    fv.faces     = TRI(Itri,:);
    lin          = intersectPlaneSurf(fv,p0,dirNormal);
%     lin          = intersectPlaneSurf_geomIO_version(fv,p0,dirNormal);
    
    if length(lin)>1
        if opt.debug
            warning('there is more than one shape');
        end
        % check if it is the issue of one polygon cut in two
        if length(lin) == 2
            c = [lin{1}, lin{2}];
            close = max([abs(max(c(1,:)-min(c(1,:)))), abs(max(c(2,:)-min(c(2,:))))])/1e3;
            if pdist([lin{1}(:,1)';lin{2}(:,1)'],'euclidean') < close && pdist([lin{1}(:,end)';lin{2}(:,end)'],'euclidean')
                newlin = {};
                newlin{1} = [lin{1}, fliplr(lin{2})];
                lin = newlin;
            elseif pdist([lin{1}(:,1)';lin{2}(:,end)'],'euclidean') < close && pdist([lin{1}(:,end)';lin{2}(:,1)'],'euclidean')
                newlin = {};
                newlin{1} = [lin{1}, lin{2}];
                lin = newlin;
            end
        end
    end
    for kk= 1:length(lin)
        Polygons{n} = lin{kk}';
        % make sure all polygons are clockwise
        Polygons{n} = makeClockWise(Polygons{n}, cstDir);
        pos(n)      = iS+Shift;
        n= n+1;
    end
%     Polygons{iS} = lin{1}';

end
fprintf(' %.2f s\n',toc);

end


% 
% %% Plot
% patch(X(TRI'),Y(TRI'),Z(TRI'),[.2 .2 .8],'linestyle','none')
% light
% axis equal
% view(60,10)
% % 
% hold on
% 
% [XP,YP,ZP] = meshgrid(xp,yp,zp);
% plot3(XP(:),YP(:),ZP(:),'o')
% 
% for iS = 1:no_part
%     gridCoord = lin{iS};
%     plot3(gridCoord(1,:),gridCoord(2,:),gridCoord(3,:),'or','Markersize',4,'MarkerFaceColor','r')
% end




