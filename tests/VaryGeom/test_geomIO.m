% -------------------------------------------------------------------------
%    Copyright (c) 2015-, JGU Mainz, Arthur Bauville, Tobias S. Baumann &
%    Arne Spang
%    All rights reserved.
%
%    This software was developed at:
%
%         Institute of Geosciences
%         Johannes-Gutenberg University, Mainz
%         Johann-Joachim-Becherweg 21
%         55128 Mainz, Germany
%
%    project:    geomIO
%    filename:   test_geomIO.m
%
%    geomIO is free software: you can redistribute it and/or modify
%    it under the terms of the GNU General Public License as published
%    by the Free Software Foundation, version 3 of the License.
%
%    geomIO is distributed in the hope that it will be useful,
%    but WITHOUT ANY WARRANTY; without even the implied warranty of
%    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
%    See the GNU General Public License for more details.
%
%    You should have received a copy of the GNU General Public License
%    along with geomIO. If not, see <http://www.gnu.org/licenses/>.
%
%
%    Contact:
%        Tobias Baumann   [baumann@uni-mainz.de]
%        Arthur Bauville  [abauville@jamstec.go.jp]
%        Arne Spang       [arspang@uni-mainz.de]
% =========================================================================
%% Set things up
clear
outDirPuna = './Out_Puna';
outDirGrid = './Out_Grid';
outDirSub  = './Out_Sub';

%% check path
filepath = fileparts(mfilename('fullpath'));
curpath  = pwd;

if ~strcmp(filepath,curpath)
    fprintf('Please navigate to %s to run the tests.\n',filepath)
    return
end

%% Clean up

if exist(outDirPuna,'dir')
    deleteDir(outDirPuna)
end
if exist(outDirGrid,'dir')
    deleteDir(outDirGrid)
end
if exist(outDirGrid,'dir')
    deleteDir(outDirSub)
end
if isfile('PetscVec.mat')
    delete PetscVec.mat
end

%% Create Puna Geom

% settings
inputFile                   = ['RoundTopShortAPMB_OR.HZ.svg'];
opt                         = geomIO_Options();
opt.outputDir               = [outDirPuna];
opt.inputFileName           = inputFile;
opt.LaMEMinputFileName      ='testPuna.dat';
opt.readLaMEM               = true;
opt.writeParaview           = true;
opt.writePolygons           = true;
opt.interp                  = true;
opt.zi                      = [-80:1:15];
opt.getPolygons             = true;

opt.writeSVG                = false;
opt.imgAdd                  = false;

opt.Vert.write              = false;

opt.varyGeom.do             = true;
opt.varyGeom.volume         = 'APMB_HZ';
opt.varyGeom.n              = 3;
opt.varyGeom.stretchType    = 'factor';
opt.varyGeom.dim            = '3D';
opt.varyGeom.mu             = [1.5 0.5];
opt.varyGeom.sigma          = [0.1 0.1];
opt.varyGeom.genType        = 'guidedChain';
opt.varyGeom.CtrlPoly       = [45 50];
opt.varyGeom.CP_ref         = 'volume';
opt.varyGeom.outName        = 'test';
opt.varyGeom.writeParaview  = false;
opt.varyGeom.writePolygons  = true;
opt.varyGeom.drawOutlines   = true;
opt.varyGeom.saveVols       = true;
opt.varyGeom.outlineProf    = [7536 -12.5];

% Density assignment
paths = {
    'Air', 0, 0, 0
    'Mantle', 3300, 5, 0
    'MiddleCrust', 2810, 3, 0
    'UpperCrust', 2700, 2 , 0
    'Sediments', 2550, 1, 0
    'APMB_HZ', 2650, 4, 1
    'CrustalRoot_HZ', 2910, 4, 0
     };
opt.pathNames       = {paths{:,1}}; 
opt.gravity.drho    = [paths{:,2}]; 
opt.phase           = [paths{:,3}];
opt.type            = [paths{:,4}];

% Run geomIO
[~,Volumes,~] = run_geomIO(opt,'default');

%% Test some things
assert(length(fields(Volumes)) == 7,'Puna: Number of Volumes is wrong.\n')
assert(length(Volumes.APMB_HZ.Polygons) == 73,'Puna: Volume has wrong number of polygons.\n')
assert(isfile([outDirPuna, '/Polygons.256x256x128.3x3x3.bin']),'Puna: Polygon file does not exist.\n')
assert(isfile([outDirPuna, '/RoundTopShortAPMB_OR.HZ_CrustalRoot_HZ.vtk']),'Puna: VTK file does not exist.\n')

% check versions
cd(outDirPuna)
for i = 1 : 3
    fname = ['test',sprintf('%06d',i)];
    assert(exist(fname,'dir') == 7,['Puna: Failed to create ', fname, '.\n'])
    cd(fname)
    assert(isfile('StretchInfo.csv'),'Failed to create Stretchinfo file.\n')
    assert(isfile('Polygons.256x256x128.3x3x3.bin'),'Failed to create Polygon file.\n')
    cd('../')
end

% check saveVols
maxX = max(Volumes.APMB_HZ.Polygons{29}(:,1));
maxY = max(Volumes.APMB_HZ.Polygons{29}(:,2));
dep  = Volumes.APMB_HZ.Polygons{29}(1,3);
load saveVols.mat;
assert(length(saveVols) == 3, 'Puna: saveVols has wrong number of versions.\n')
for i = 1 : 3
    vol = saveVols{i};
    vDep = vol.Polygons{29}(1,3);
    assert(length(vol.Polygons) == 73,'Puna: Version %d has wrong number of polygons.\n', i)
    assert(max(vol.Polygons{29}(:,1)) > maxX,'Puna: Version %d seems to have shrunk in x-direction.\n', i)
    assert(max(vol.Polygons{29}(:,2)) < maxY,'Puna: Version %d seems to have grown in y-direction.\n', i)
    assert(vDep < (dep + 1e-6) && vDep > (dep - 1e-6),'Depth of polygons has changed.\n')
end
cd('../')

% clean up
deleteDir(outDirPuna)
delete PetscVec.mat
clear opt Volumes

%% Create reduced version of Puna Geom to check generate stetch grid

% settings
inputFile                   = ['RoundTopShortAPMB_OR.HZ.svg'];
opt                         = geomIO_Options();
opt.outputDir               = [outDirGrid];
opt.inputFileName           = inputFile;
opt.LaMEMinputFileName      ='testPuna.dat';
opt.readLaMEM               = true;
opt.writeParaview           = false;
opt.writePolygons           = false;
opt.interp                  = true;
opt.zi                      = [-80:1:15];
opt.getPolygons             = true;

opt.writeSVG                = false;
opt.imgAdd                  = false;

opt.Vert.write              = false;

opt.varyGeom.do             = true;
opt.varyGeom.volume         = 'MiddleCrust';
opt.varyGeom.stretchType    = 'factor';
opt.varyGeom.dim            = '2D_Y';
opt.varyGeom.genType        = 'grid';
opt.varyGeom.CtrlPoly       = [30, 45, 50];
opt.varyGeom.Grid.lb        = [1;  10;  7.5];
opt.varyGeom.Grid.ub        = [4;  15; 27.5];
opt.varyGeom.Grid.num       = [3;  2;    4];
opt.varyGeom.Grid.noise     = 0.2;
opt.varyGeom.Grid.Sz        = [0, 2, 2];
opt.varyGeom.CP_ref         = 'volume';
opt.varyGeom.outName        = 'testGrid';
opt.varyGeom.writeParaview  = false;
opt.varyGeom.writePolygons  = false;
opt.varyGeom.drawOutlines   = false;
opt.varyGeom.saveVols       = true;
opt.varyGeom.outlineProf    = [7536 -12.5];

% Density assignment
paths = {
    'Air', 0, 0, 0
    'Mantle', 3300, 5, 0
    'MiddleCrust', 2810, 3, 0
    'UpperCrust', 2700, 2 , 0
    'Sediments', 2550, 1, 0
     };
opt.pathNames       = {paths{:,1}}; 
opt.gravity.drho    = [paths{:,2}]; 
opt.phase           = [paths{:,3}];
opt.type            = [paths{:,4}];

% Run geomIO
[~,Volumes,~] = run_geomIO(opt,'default');

%% Test some things
assert(length(fields(Volumes)) == 5,'Grid: Number of Volumes is wrong.\n')
assert(length(Volumes.MiddleCrust.Polygons) == 81,'Grid: Volume has wrong number of polygons.\n')
assert(~isfile([outDirGrid, '/Polygons.256x256x128.3x3x3.bin']),'Grid: Polygon file was accidentally created.\n')
assert(~isfile([outDirGrid, '/RoundTopShortAPMB_OR.HZ_MiddleCrust.vtk']),'Grid: VTK file was accidentally created.\n')

% check saveVols
cd(outDirGrid)
load saveVols.mat;
assert(length(saveVols) == 48, 'Grid: saveVols has wrong number of versions.\n')
for i = 1 : 48
    vol = saveVols{i};
    if i <= 24
        assert(length(vol.Polygons) < 80,'Grid: Number of Polygons in version %d probably too high.\n',i)
    else
        assert(length(vol.Polygons) > 80,'Grid: Number of Polygons in version %d probably too low.\n',i)
    end
end

% check Stretch Info
load StretchInfo.mat
assert(all(size(StrInfo.Stretch) == [3,2,48]),'Grid: StrInfo.Stretch has wrong dimensions.\n')
assert(isfield(StrInfo,'Sz'),'Grid: No Sz field.\m')
assert(all(all(StrInfo.Stretch(:,1,:) == 1)),'Grid: Not all Sx are 1 like they should be.\n')
assert(all(all(StrInfo.Stretch(1,2,:) < 5)),'Grid: Some Sy1 are too high.\n')
assert(all(all(StrInfo.Stretch(2,2,:) > 9)),'Grid: Some Sy2 are too low.\n')
assert(all(all(StrInfo.Stretch(3,2,:) < 30)),'Grid: Some Sy3 are too high.\n')
cd('../')

% clean up
deleteDir(outDirGrid)
clear opt Volumes

%% Create Subduction Zone

% settings
inputFile           = ['Subduct.EW.svg'];
opt                 = geomIO_Options();
opt.outputDir       = [outDirSub];
opt.inputFileName   = inputFile;
opt.LaMEMinputFileName ='testSub.dat';
opt.readLaMEM       = true;
opt.writeParaview   = true;
opt.writePolygons   = true;
opt.interp          = true;
opt.zi              = [-9:1:9];
opt.getPolygons     = true;

opt.varySub.do      = true;
opt.varySub.vols    = {'SimpleSlab'};
opt.varySub.ref     = 'trench';
opt.varySub.xRot    = [0];
opt.varySub.theta   = [5];
opt.varySub.tolZ    = [1];

opt.varySub.addCrust= true;
opt.varySub.d_OC    = 8;
opt.varySub.ID_OC   = 4;
opt.varySub.type_OC = 0;

opt.varySub.addWZ   = true;
opt.varySub.d_WZ    = 15;
opt.varySub.ID_WZ   = 5;
opt.varySub.d_Lith  = 100;


% Density assignment
paths = {
    'Air',         100, 0, 0
    'Mantle',     3300, 6, 0
    'ContCrust',  2800, 1, 0
    'ContMantle', 3250, 2, 0
    'SimpleSlab', 3300, 3, 0
    };
opt.pathNames       = {paths{:,1}}; 
opt.gravity.drho    = [paths{:,2}]; 
opt.phase           = [paths{:,3}];
opt.type            = [paths{:,4}];

% Run geomIO
[~,Volumes,~] = run_geomIO(opt,'default');

%% Test some things
assert(length(fields(Volumes)) == 7,'Sub: Number of Volumes is wrong.\n')
assert(length(Volumes.SimpleSlab.Polygons) == 324,'Sub: Slab has wrong number of polygons.\n')
assert(isfile([outDirSub, '/Polygons.512x1x256.3x3x3.bin']),'Sub: Polygon file does not exist.\n')
assert(isfile([outDirSub, '/Subduct.EW_SimpleSlab.vtk']),'Sub: Slab VTK file does not exist.\n')
assert(isfile([outDirSub, '/Subduct.EW_OceanicCrust.vtk']),'Sub: Oceanic Crust VTK file does not exist.\n')
assert(isfile([outDirSub, '/Subduct.EW_WeakZone.vtk']),'Sub: Weakzone VTK file does not exist.\n')
assert(min(Volumes.SimpleSlab.Polygons{323}(:,1)) < -631,'Sub: Left margin has shifted.\n')
assert(Volumes.SimpleSlab.Polygons{1}(1,3) < -310,'Sub: Slab was not steepened enough.\n')
assert(Volumes.SimpleSlab.Polygons{1}(1,3) > -311,'Sub: Slab was steepened too much.\n')
assert(min(Volumes.OceanicCrust.Polygons{end-8}(:,1)) < -631,'Sub: Oceanic Crust is not thick enough.\n')
assert(min(Volumes.OceanicCrust.Polygons{end-9}(:,1)) > -34,'Sub: Oceanic Crust is too thick.\n')
assert(Volumes.OceanicCrust.Polygons{1}(1,3) < - 281.5,'Sub: Oceanic Crust does not go all the way down.\n')
assert(length(Volumes.WeakZone.Polygons) == 105,'Sub: WeakZone has wrong number of polygons.\n')
assert(Volumes.WeakZone.Polygons{1}(1,3) > -100,'Sub: WeakZone goes too deep.\n')
assert(max(Volumes.WeakZone.Polygons{end}(:,1)) > 9,'Sub: WeakZone is not wide enough.\n')
assert(Volumes.WeakZone.type == 0,'Sub: WeakZone type is not set correctly.\n')

% clean up
deleteDir(outDirSub)
delete PetscVec.mat
clear opt Volumes

%% Message
fprintf('################# \n')
fprintf('All Tests passed! \n')
fprintf('################# \n')

%% supporting functions
function deleteDir(dirName)
list = dir(dirName);
for i = 1 :length(list)
    if ~list(i).isdir
        delete([dirName, '/', list(i).name]);
    elseif list(i).isdir & regexp(list(i).name, regexptranslate('wildcard','test*'))
        subList = dir([dirName, '/', list(i).name]);
        for j = 1 : length(subList)
            if ~subList(j).isdir
                delete([dirName, '/', list(i).name, '/', subList(j).name])
            end
        end
        rmdir([dirName, '/', list(i).name])
    end
end
rmdir(dirName)
end